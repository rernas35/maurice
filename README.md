
# README #

Maurice's aim is to enable data management(dispatch, encapsulate and query) and visualization easily on lambda architecture. 

![enter image description here](https://s3-us-west-2.amazonaws.com/maurice-static/maurice.jpg)

Basically with maurice you can ;   

* Define your event types easily without importing your event model library (for complex types use json-schema and for flat event type use delimited event type)
```
#!yaml
${transactionId:integer}|${customerId:integer}|${cardNumber:string}|${transactionAmount:float}|${transactionDate:integer}|${payload:json}
```

* Seggragate data by Predicate Javascript
```
#!javascipt

	var date = event.getTransactionDate();
	return date.getDay() == 0 || date.getDate() == 6;
```

* Encapsulate data  
  
```
#!yaml
  Skip
  	cardNumber --> This cardNumber field won't be included while serializing back.
	payload.transaction.cardNumber --> in payload field, transaction's cardNumber field won't be included while serializing back

  TransformValue      
	customerId -> value.substr(0,2) + "xxx" + value.substr(5,8)
	
```
          
* Create Ponds easily from streams. Pond is summarization of event stream in a time window.
  
* Process data by scala code and execute it on cluster  
  
For Future ;   

* Anomaly detection on ponds


### Modules ###

The following modules of Maurice with their definitions ; 

* **streaming (akka-streaming)** : first spark streaming is used then switched to akka streaming.All incoming stream is processed by akka-streaming engine.
* **caching** : configuration is kept in caching module also some pub-sub events are managed here.
* **mgmt-core and mgmt-ws** : Management APIs exist in mgmt-ws module and core fuctionality exist in mgmt-core module.
* **mapreduce-core and mapreduce-spark** : Scala code that is developed on portal , is executed by these 2 modules.
* **spark-implicits** : scala implicits that helps to save, import and export data.
* **query** : executes the queries and mark them whether it should be executed on NoSQL or RDBMS


### How to run it ? ###

Maurice runs on **Mesosphere DCOS** (datacenter operating system). You can [create a instance of DCOS cluster on AWS](https://docs.mesosphere.com/1.10/installing/cloud/) and select Maurice from the repository.  
You will be able to define components of maurice like cassandra, mysql, spark.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact