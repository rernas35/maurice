import {PanelCtrl} from 'app/plugins/sdk';
import moment from 'moment';
import _ from 'lodash';
import DataTable from './lib/jquery.dataTables.min.js'
import './lib/require.js'
import './css/event-def-panel.css!';
import './css/jquery.dataTables.css!';

angular.module("myApp", ["ngTable"]);

const eventDefinitionList = {
  username:"bisi",
  eventDefinitions:[],
  dataTable:null,
  tableParams:null,
  selectedEventDef:null,
  selectedTopic:null,
  selectedTransform:null,
  isNew:false,
  isNewTopic:false,
  isNewTransform:false,
  evalScriptEditor:null,
  mode:"event-list"
};

export class EventDefCtrl extends PanelCtrl {
  constructor($scope, $injector,$timeout) {
    super($scope, $injector);
    // getUser.bind(this);
    // listEventDefs.bind(this);
    this.request4User();
    this.request4EventList();

    _.defaultsDeep(this.panel, eventDefinitionList);


    this.events.on('init-edit-mode', this.onInitEditMode.bind(this));
    this.events.on('panel-teardown', this.onPanelTeardown.bind(this));
    this.events.on('panel-initialized', this.render.bind(this));
    this.listEventDefinitions();

    requirejs.config({
      paths: {
        ace: 'public/plugins/grafana-maurice-event-definitions/lib/ace/'
      }
    });    
  }

  request4User(){
    this.sendAjaxRequest("GET","/api/user","",false,this.callback4User);
  }

  callback4User(res) {
        this.panel.username = res.login;
      };

  request4EventList(){
    this.sendAjaxRequest("GET","http://localhost:9290/analytics/api/services/management/event",
                              "",
                              true,
                              function(res){this.$scope.$apply(this.callback4EventList(res));});
  }

  callback4EventList(res) {
        this.panel.eventDefinitions = res.t;
        this.panel.mode='event-list';
        this.$timeout( function(){
             $('#event-def-list-table').DataTable({
                                                   "lengthMenu": [
                                                                  [ 5, 10, -1 ],
                                                                  [ '5 rows', '10 rows', 'Show all' ]
                                                              ]
                                                  });
         });
      };

  back2EventList(){
    this.panel.isNew=false;
    this.request4EventList();
  }

   back2EventDetail(){
    this.panel.isNewTopic=false;
    this.panel.mode='event-detail';
    this.initializeTopicRouteTable();
  }

  back2TopicDetail(){
    this.panel.isNewTransform=false;
    this.panel.mode='topic-detail';
    this.initializeTransformTable();
  }

  request4UpdateEventDefinition(){
    var data = angular.toJson(this.panel.selectedEventDef);
    this.sendAjaxRequest("PUT","http://localhost:9290/analytics/api/services/management/event",
                                data,
                                true,
                                function(){
                                    this.back2EventList();
                                  });
  }

  request4CreateEventDefinition(){
    this.sendAjaxRequest("POST","http://localhost:9290/analytics/api/services/management/event",
                                angular.toJson(this.panel.selectedEventDef),
                                true,
                                function(){
                                    this.back2EventList();
                                  });
  }

  request4DeleteEventDefinition(){
    this.sendAjaxRequest("DELETE","http://localhost:9290/analytics/api/services/management/event/" + this.panel.selectedEventDef.name ,
                                  "",
                                  true,
                                  function(){
                                    this.back2EventList();
                                  });
  }


  /* not implemented yet*/
  request4UpdateTopicRoute(){
    var data = angular.toJson(this.panel.selectedEventDef,function(key, val) {
                                                                        if (key !== "$$hashKey")return val;
                                                                      });
    this.sendAjaxRequest("PUT","http://localhost:9290/analytics/api/services/management/event",data,true,this.callback4TopicAddDelete);
  }

  request4CreateTopicRoute(){
    this.sendAjaxRequest("POST","http://localhost:9290/analytics/api/services/management/event/"  + this.panel.selectedEventDef.name + "/topic", 
                                angular.toJson(this.panel.selectedTopic),
                                //JSON.stringify(this.panel.selectedTopic),
                                true,
                                this.callback4TopicAddDelete);
  }

  request4DeleteTopicRoute(){
    this.sendAjaxRequest("DELETE","http://localhost:9290/analytics/api/services/management/event/" 
                                    + this.panel.selectedEventDef.name
                                    + "/topic/"
                                    + this.panel.selectedTopic.topicName,
                                  "",
                                  true,
                                  this.callback4TopicAddDelete);
  }

  callback4TopicAddDelete(){
    this.sendAjaxRequest("GET","http://localhost:9290/analytics/api/services/management/event/" 
                                    + this.panel.selectedEventDef.name,
                          "",true,
                          function(res){
                              this.panel.selectedEventDef = res.t;
                              this.$scope.$apply(this.back2EventDetail()); 
                          });
  }

  removeTransformScript(removal){
    this.panel.selectedTopic.transformScriptList = this.panel.selectedTopic.transformScriptList.filter(item => item.fieldPath !== removal.fieldPath);
    this.back2TopicDetail();
  }

  addTransformScript(){
    this.panel.selectedTopic.transformScriptList.push(this.panel.selectedTransform);
    this.back2TopicDetail();
  }

  callback4General(res){
    alert(res.description);
  }

  initializeTopicRouteTable(){
    this.$timeout( function(){
             $('#topic-list-table').DataTable({
                                                   "lengthMenu": [
                                                                  [ 4, 8, -1 ],
                                                                  [ '4 rows', '8 rows', 'Show all' ]
                                                              ]
                                                  });
         });
  }

  initializeEvalScriptEditor(){
      var onChange = function() {
        this.panel.selectedTopic.evaluationScript = this.panel.evalScriptEditor.getValue();
      }
  
      requirejs(['ace/ace'],function(){
        ace.config.set('basePath', '/public/plugins/grafana-maurice-event-definitions/lib/ace');
        var editor = ace.edit("evalScriptEditor");
        editor.session.setMode("ace/mode/javascript");
        editor.setTheme("ace/theme/monokai");
        editor.on("change", onChange.bind(this));
        this.panel.evalScriptEditor = editor;
     }.bind(this));
  }

  initializeTransformTable(){
    this.$timeout( function(){
             $('#transform-list-table').DataTable({
                                                   "lengthMenu": [
                                                                  [ 4, 8, -1 ],
                                                                  [ '4 rows', '8 rows', 'Show all' ]
                                                              ]
                                                  });
         });
    this.initializeEvalScriptEditor();
  }

  // initializeEvalScript(){
  //   setTimeout( function(){
  //            var editor = ace.edit("editor");
  //             editor.setTheme("ace/theme/monokai");
  //             editor.session.setMode("ace/mode/javascript");
  //        }, 1000 );
  // }

  initiateNewEventDefinition(){
    this.panel.isNew = true;
    this.panel.selectedEventDef = {"name": "","externalId" : "","description": "","ownerName" : this.panel.username,"format" : ""};
    this.panel.mode="event-detail";
  }

  initiateNewTopicRoute(){
    this.panel.isNewTopic = true;
    this.panel.selectedTopic = {"topicName": "","evaluationScript" : "","throughput": "LOW","transformScriptList" : []};
    this.panel.mode="topic-detail";
  }

  initiateNewTransform(){
    this.panel.isNewTransform = true;
    this.panel.selectedTransform = {"action": "skip","fieldPath" : "","javascript": "","phase":"PRE"};
    this.panel.mode="transform-detail";
  }

  onInitEditMode() {
    this.addEditorTab('Options', 'public/plugins/grafana-maurice-event-definitions/editor.html', 2);
  }

  onPanelTeardown() {
    this.$timeout.cancel(this.nextTickPromise);
  }

  sendAjaxRequest(_type,_url,_data,_crossDomain,_callback4Success) {
      var request = $.ajax({
        type: _type,
        url: _url,
        context:this,
        data: _data,
        crossDomain : _crossDomain,
        contentType: 'json'
      });
      request.done(_callback4Success);
      request.fail(function(jqXHR, textStatus) {
      });
  }



  listEventDefinitions() {
//    this.nextTickPromise = this.$timeout(this.updateClock.bind(this), 1000);
  }




  

}

EventDefCtrl.templateUrl = 'module.html';
