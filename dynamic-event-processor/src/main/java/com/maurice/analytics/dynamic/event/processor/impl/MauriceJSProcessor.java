package com.maurice.analytics.dynamic.event.processor.impl;

import com.maurice.analytics.dynamic.event.processor.IMauriceProcessor;
import com.maurice.analytics.dynamic.event.processor.error.MauriceProcessingException;
import com.maurice.analytics.dynamic.event.processor.error.MauriceScriptCompileException;

import javax.script.ScriptException;

public class MauriceJSProcessor implements IMauriceProcessor {

    JSEngine nashornEngine;
    String script;


    public MauriceJSProcessor(String script) throws MauriceScriptCompileException{
        this.nashornEngine = new JSEngine();
        this.script = script;
        try {
            nashornEngine.initializaInvocable(script);
        } catch (ScriptException e) {
            throw new MauriceScriptCompileException("Error while copiling script", e);
        }
    }



    @Override
    public Object process(Object... params) throws MauriceProcessingException {
        nashornEngine.evaluate(script,"process", params);

        return null;
    }
}
