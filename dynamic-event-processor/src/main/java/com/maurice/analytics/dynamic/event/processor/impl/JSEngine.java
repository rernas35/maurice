package com.maurice.analytics.dynamic.event.processor.impl;


import com.maurice.analytics.dynamic.event.processor.error.MauriceProcessingException;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class JSEngine<R> {

    private static ScriptEngine engine;
    private static ConcurrentHashMap<Integer,Invocable> preCompiledScriptMap = new ConcurrentHashMap<Integer,Invocable>();

    public JSEngine(){
        engine = new ScriptEngineManager().getEngineByName("nashorn");
    }


    protected Optional<Invocable> getPreCompiledScript(String finalScript){
        if (preCompiledScriptMap.contains(finalScript.hashCode()))
            return Optional.of(preCompiledScriptMap.get(finalScript.hashCode()));
        return Optional.empty();
    }


    protected Invocable initializaInvocable(String finalScript) throws ScriptException {
        engine.eval(finalScript);
        Invocable invocable =  (Invocable) engine;
        preCompiledScriptMap.put(finalScript.hashCode(),invocable);
        return invocable;
    }

    public <R> R evaluate(String finalScript, String methodName, Object... params) throws MauriceProcessingException {
        try {
            Optional<Invocable> preCompiledScript = getPreCompiledScript(finalScript);
            return (R) preCompiledScript.orElse(initializaInvocable(finalScript)).invokeFunction(methodName, params);
        } catch (ScriptException | NoSuchMethodException e) {
            throw new MauriceProcessingException(String.format("Error while processing script : %s" , finalScript),e);
        }
    }

}
