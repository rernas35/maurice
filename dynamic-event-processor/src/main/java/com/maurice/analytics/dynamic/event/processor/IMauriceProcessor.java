package com.maurice.analytics.dynamic.event.processor;

import com.maurice.analytics.dynamic.event.processor.error.MauriceProcessingException;

public interface IMauriceProcessor {

      Object process(Object... params) throws MauriceProcessingException;



}
