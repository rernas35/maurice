package com.maurice.analytics.cache;

import com.maurice.analytics.cache.mock.MockHazelcastInstance;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MauriceAnalyticsCacheTest {


    @Before
    public void setUp() throws Exception {
        MauriceAnalyticsCache.initialize(() -> new MockHazelcastInstance());
    }

    @Test
    public void shouldNotHaveNullInstanceAfterInitialization() {
        assertNotNull(MauriceAnalyticsCache.getInstance());
    }

    @Test
    public void shouldExistInTheMapWhenPutSimpleEventDefinitionThen() {
        MauriceCacheHelper.putEventDefinitionWithName("1000");
        assertTrue(MauriceAnalyticsCache.getEventDefinitionMap().get("1000") != null);
    }

    @Test
    public void shouldNotContainEventDefinitionInTheMapWhenAnEventDefinitionIsRemoved() {
        MauriceCacheHelper.putEventDefinitionWithName("1000");
        MauriceAnalyticsCache.removeEventDefinition("1000");
        assertTrue(MauriceAnalyticsCache.getEventDefinition("1000") == null);
    }

    @Test
    public void shouldReturnCorrectSizeOfMapWhen3EventDefinitionAreAdded() {
        MauriceCacheHelper.putEventDefinitionWithName("1000");
        MauriceCacheHelper.putEventDefinitionWithName("1001");
        MauriceCacheHelper.putEventDefinitionWithName("1002");
        assertEquals(MauriceAnalyticsCache.getEventDefinitions().size(), 3);
    }

    @Test
    public void shouldContainUserAfterUserIsPutToTheMap() {
        MauriceCacheHelper.putUserWithName("1000");
        assertTrue(MauriceAnalyticsCache.getUser("1000") != null);
    }

    @Test
    public void shouldNotContainUserInTheMapWhenAnUserIsRemoved() {
        MauriceCacheHelper.putUserWithName("1000");
        MauriceAnalyticsCache.removeUser("1000");
        assertTrue(MauriceAnalyticsCache.getUser("1000") == null);
    }

    @Test
    public void shouldReturnCorrectSizeOfMapWhen3UsersAreAdded() {
        MauriceCacheHelper.putUserWithName("1000");
        MauriceCacheHelper.putUserWithName("1001");
        MauriceCacheHelper.putUserWithName("1002");
        assertTrue(MauriceAnalyticsCache.getUsers().size() == 3);
    }

    @Test
    public void shouldContainEventDefinitionWithTopicRouteAfterEventDefinitionWithTopicRouteIsPutToTheMap() {
        MauriceCacheHelper.putEventDefinitionWithTopicRoute("1000", 2000L);
        assertTrue(MauriceAnalyticsCache.getTopicRoutesByEventDefinitionId("1000").get(0).getTopicRouteId() == 2000L);
    }


}
