package com.maurice.analytics.cache.mock;

import com.hazelcast.aggregation.Aggregator;
import com.hazelcast.core.*;
import com.hazelcast.map.EntryProcessor;
import com.hazelcast.map.MapInterceptor;
import com.hazelcast.map.QueryCache;
import com.hazelcast.map.listener.MapListener;
import com.hazelcast.map.listener.MapPartitionLostListener;
import com.hazelcast.mapreduce.JobTracker;
import com.hazelcast.mapreduce.aggregation.Aggregation;
import com.hazelcast.mapreduce.aggregation.Supplier;
import com.hazelcast.monitor.LocalMapStats;
import com.hazelcast.projection.Projection;
import com.hazelcast.query.Predicate;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class MockMap<K, V> extends ConcurrentHashMap<K, V> implements IMap<K, V> {

    /**
     *
     */
    private static final long serialVersionUID = -2656489782037325471L;

    @Override
    public String getPartitionKey() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getServiceName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub

    }

    @Override
    public void removeAll(Predicate<K, V> predicate) {

    }

    @Override
    public void delete(Object key) {
        // TODO Auto-generated method stub

    }

    @Override
    public void flush() {
        // TODO Auto-generated method stub

    }

    @Override
    public Map<K, V> getAll(Set<K> keys) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void loadAll(boolean replaceExistingValues) {
        // TODO Auto-generated method stub

    }

    @Override
    public void loadAll(Set<K> keys, boolean replaceExistingValues) {
        // TODO Auto-generated method stub

    }

    @Override
    public ICompletableFuture<V> getAsync(K key) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ICompletableFuture<V> putAsync(K key, V value) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ICompletableFuture<V> putAsync(K key, V value, long ttl, TimeUnit timeunit) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ICompletableFuture<Void> setAsync(K key, V value) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ICompletableFuture<Void> setAsync(K key, V value, long ttl, TimeUnit timeunit) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ICompletableFuture<V> removeAsync(K key) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean tryRemove(K key, long timeout, TimeUnit timeunit) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean tryPut(K key, V value, long timeout, TimeUnit timeunit) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public V put(K key, V value, long ttl, TimeUnit timeunit) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void putTransient(K key, V value, long ttl, TimeUnit timeunit) {
        // TODO Auto-generated method stub

    }

    @Override
    public V putIfAbsent(K key, V value, long ttl, TimeUnit timeunit) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void set(K key, V value) {
        // TODO Auto-generated method stub

    }

    @Override
    public void set(K key, V value, long ttl, TimeUnit timeunit) {
        // TODO Auto-generated method stub

    }

    @Override
    public void lock(K key) {
        // TODO Auto-generated method stub

    }

    @Override
    public void lock(K key, long leaseTime, TimeUnit timeUnit) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isLocked(K key) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean tryLock(K key) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean tryLock(K key, long time, TimeUnit timeunit) throws InterruptedException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean tryLock(K key, long time, TimeUnit timeunit, long leaseTime, TimeUnit leaseTimeunit)
            throws InterruptedException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void unlock(K key) {
        // TODO Auto-generated method stub

    }

    @Override
    public void forceUnlock(K key) {
        // TODO Auto-generated method stub

    }

    @Override
    public String addLocalEntryListener(MapListener listener) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String addLocalEntryListener(EntryListener listener) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String addLocalEntryListener(MapListener listener, Predicate<K, V> predicate, boolean includeValue) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String addLocalEntryListener(EntryListener listener, Predicate<K, V> predicate, boolean includeValue) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String addLocalEntryListener(MapListener listener, Predicate<K, V> predicate, K key, boolean includeValue) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String addLocalEntryListener(EntryListener listener, Predicate<K, V> predicate, K key,
                                        boolean includeValue) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String addInterceptor(MapInterceptor interceptor) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void removeInterceptor(String id) {
        // TODO Auto-generated method stub

    }

    @Override
    public String addEntryListener(MapListener listener, boolean includeValue) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String addEntryListener(EntryListener listener, boolean includeValue) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean removeEntryListener(String id) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String addPartitionLostListener(MapPartitionLostListener listener) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean removePartitionLostListener(String id) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String addEntryListener(MapListener listener, K key, boolean includeValue) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String addEntryListener(EntryListener listener, K key, boolean includeValue) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String addEntryListener(MapListener listener, Predicate<K, V> predicate, boolean includeValue) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String addEntryListener(EntryListener listener, Predicate<K, V> predicate, boolean includeValue) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String addEntryListener(MapListener listener, Predicate<K, V> predicate, K key, boolean includeValue) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String addEntryListener(EntryListener listener, Predicate<K, V> predicate, K key, boolean includeValue) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public EntryView<K, V> getEntryView(K key) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean evict(K key) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void evictAll() {
        // TODO Auto-generated method stub

    }

    @Override
    public Set<K> keySet(Predicate predicate) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Set<java.util.Map.Entry<K, V>> entrySet(Predicate predicate) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<V> values(Predicate predicate) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Set<K> localKeySet() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Set<K> localKeySet(Predicate predicate) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void addIndex(String attribute, boolean ordered) {
        // TODO Auto-generated method stub

    }

    @Override
    public LocalMapStats getLocalMapStats() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object executeOnKey(K key, EntryProcessor entryProcessor) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<K, Object> executeOnKeys(Set<K> keys, EntryProcessor entryProcessor) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void submitToKey(K key, EntryProcessor entryProcessor, ExecutionCallback callback) {
        // TODO Auto-generated method stub

    }

    @Override
    public ICompletableFuture submitToKey(K key, EntryProcessor entryProcessor) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<K, Object> executeOnEntries(EntryProcessor entryProcessor) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<K, Object> executeOnEntries(EntryProcessor entryProcessor, Predicate predicate) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <R> R aggregate(Aggregator<Entry<K, V>, R> aggregator) {
        return null;
    }

    @Override
    public <R> R aggregate(Aggregator<Entry<K, V>, R> aggregator, Predicate<K, V> predicate) {
        return null;
    }

    @Override
    public <R> Collection<R> project(Projection<Entry<K, V>, R> projection) {
        return null;
    }

    @Override
    public <R> Collection<R> project(Projection<Entry<K, V>, R> projection, Predicate<K, V> predicate) {
        return null;
    }

    @Override
    public <SuppliedValue, Result> Result aggregate(Supplier<K, V, SuppliedValue> supplier,
                                                    Aggregation<K, SuppliedValue, Result> aggregation) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <SuppliedValue, Result> Result aggregate(Supplier<K, V, SuppliedValue> supplier,
                                                    Aggregation<K, SuppliedValue, Result> aggregation, JobTracker jobTracker) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public QueryCache<K, V> getQueryCache(String s) {
        return null;
    }

    @Override
    public QueryCache<K, V> getQueryCache(String s, Predicate<K, V> predicate, boolean b) {
        return null;
    }

    @Override
    public QueryCache<K, V> getQueryCache(String s, MapListener mapListener, Predicate<K, V> predicate, boolean b) {
        return null;
    }

}
