package com.maurice.analytics.cache.mock;

import com.hazelcast.cardinality.CardinalityEstimator;
import com.hazelcast.config.Config;
import com.hazelcast.core.*;
import com.hazelcast.durableexecutor.DurableExecutorService;
import com.hazelcast.logging.LoggingService;
import com.hazelcast.mapreduce.JobTracker;
import com.hazelcast.quorum.QuorumService;
import com.hazelcast.ringbuffer.Ringbuffer;
import com.hazelcast.scheduledexecutor.IScheduledExecutorService;
import com.hazelcast.transaction.*;
import com.maurice.analytics.cache.constants.MauriceCacheConstants;
import com.maurice.analytics.cache.model.EventDefinition;
import com.maurice.analytics.cache.model.User;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

public class MockHazelcastInstance implements HazelcastInstance {

    Map<String, EventDefinition> eventDefinitionMap = new MockMap<String, EventDefinition>();
    Map<String, User> userMap = new MockMap<String, User>();

    Map<String, ITopic> topicMap = new HashMap<String, ITopic>();

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <E> IQueue<E> getQueue(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <E> ITopic<E> getTopic(String name) {
        ITopic topic = topicMap.get(name);
        if (topic == null) {
            topic = new MockTopic<>(name);
            topicMap.put(name, topic);
        }
        return topic;
    }

    @Override
    public <E> ISet<E> getSet(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <E> IList<E> getList(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <K, V> IMap<K, V> getMap(String name) {
        if (MauriceCacheConstants.EVENT_DEFINITION_CACHE.equals(name))
            return (IMap) eventDefinitionMap;
        else if (MauriceCacheConstants.USER_CACHE.equals(name))
            return (IMap) userMap;
        throw new IllegalArgumentException(String.format("%s is not an applicable name for cache", name));
    }

    @Override
    public <K, V> ReplicatedMap<K, V> getReplicatedMap(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public JobTracker getJobTracker(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <K, V> MultiMap<K, V> getMultiMap(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ILock getLock(String key) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <E> Ringbuffer<E> getRingbuffer(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <E> ITopic<E> getReliableTopic(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Cluster getCluster() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Endpoint getLocalEndpoint() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IExecutorService getExecutorService(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public DurableExecutorService getDurableExecutorService(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T executeTransaction(TransactionalTask<T> task) throws TransactionException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T executeTransaction(TransactionOptions options, TransactionalTask<T> task) throws TransactionException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TransactionContext newTransactionContext() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TransactionContext newTransactionContext(TransactionOptions options) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IdGenerator getIdGenerator(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAtomicLong getAtomicLong(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <E> IAtomicReference<E> getAtomicReference(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ICountDownLatch getCountDownLatch(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ISemaphore getSemaphore(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<DistributedObject> getDistributedObjects() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String addDistributedObjectListener(DistributedObjectListener distributedObjectListener) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean removeDistributedObjectListener(String registrationId) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Config getConfig() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PartitionService getPartitionService() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public QuorumService getQuorumService() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ClientService getClientService() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public LoggingService getLoggingService() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public LifecycleService getLifecycleService() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T extends DistributedObject> T getDistributedObject(String serviceName, String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ConcurrentMap<String, Object> getUserContext() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public HazelcastXAResource getXAResource() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ICacheManager getCacheManager() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CardinalityEstimator getCardinalityEstimator(String s) {
        return null;
    }

    @Override
    public IScheduledExecutorService getScheduledExecutorService(String s) {
        return null;
    }

    @Override
    public void shutdown() {
        // TODO Auto-generated method stub

    }


}
