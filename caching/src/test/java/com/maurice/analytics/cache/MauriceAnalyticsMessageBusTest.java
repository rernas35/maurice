package com.maurice.analytics.cache;

import com.maurice.analytics.cache.mock.MockHazelcastInstance;
import com.maurice.analytics.cache.model.MapReduceEvent;
import com.maurice.analytics.cache.model.MapReduceStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(MauriceAnalyticsCache.class)
public class MauriceAnalyticsMessageBusTest {

    @Before
    public void setup() {
        PowerMockito.mockStatic(MauriceAnalyticsCache.class);
        when(MauriceAnalyticsCache.getInstance()).thenReturn(new MockHazelcastInstance());
    }

    @Test
    public void shouldContain1EventWhenMapReduceStatusEventisAdded() {
        MauriceAnalyticsMessageBus.publishMapReduceEventStatus(new MapReduceEvent(MapReduceStatus.COMPLETED, "completed for test"));
        assertEquals(1, MauriceAnalyticsMessageBus.getMapReduceEventStatusTopic().getLocalTopicStats().getPublishOperationCount());
    }

    @Test
    public void shouldContain1ExecutorCommandWhenMapReduceStatusExecutorCommandisAdded() {
        MauriceAnalyticsMessageBus.publishMapReduceExecutorCommand(new MapReduceEvent(MapReduceStatus.SHUTDOWN_SENT, "shutdown sent for test purpose"));
        assertEquals(1, MauriceAnalyticsMessageBus.getMapReduceExecutorCommandTopic().getLocalTopicStats().getPublishOperationCount());
    }


}
