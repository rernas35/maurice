package com.maurice.analytics.cache;

import com.maurice.analytics.cache.model.EventDefinition;
import com.maurice.analytics.cache.model.TopicRoute;
import com.maurice.analytics.cache.model.User;

public class MauriceCacheHelper {

    public static EventDefinition putEventDefinitionWithName(String name) {
        EventDefinition eventDefinition = new EventDefinition();
        eventDefinition.setName(name);
        MauriceAnalyticsCache.putEventDefinition(eventDefinition);
        return eventDefinition;
    }


    public static User putUserWithName(String name) {
        User user = new User();
        user.setName(name);
        MauriceAnalyticsCache.putUser(user);
        return user;
    }

    public static EventDefinition putEventDefinitionWithTopicRoute(String eventDefinitionName, long topicRouteId) {
        EventDefinition eventDefinition = putEventDefinitionWithName(eventDefinitionName);
        TopicRoute topicRoute = new TopicRoute();
        topicRoute.setTopicRouteId(topicRouteId);
        eventDefinition.getTopicRouteList().add(topicRoute);
        MauriceAnalyticsCache.putEventDefinition(eventDefinition);
        return eventDefinition;
    }


}
