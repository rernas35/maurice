package com.maurice.analytics.cache.mock;

import com.hazelcast.core.ITopic;
import com.hazelcast.core.MessageListener;
import com.hazelcast.monitor.LocalTopicStats;
import com.hazelcast.monitor.impl.LocalTopicStatsImpl;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class MockTopic<E> implements ITopic<E> {

    Queue<E> queue = new ConcurrentLinkedQueue<E>();
    Map<String, MessageListener> listenerMap = new HashMap<String, MessageListener>();
    LocalTopicStats stats = new LocalTopicStatsImpl();
    String name;

    public MockTopic(String name) {
        this.name = name;
    }

    @Override
    public String getPartitionKey() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getServiceName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub

    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return name;
    }

    @Override
    public void publish(E message) {
        queue.add(message);
        ((LocalTopicStatsImpl) stats).incrementPublishes();
    }

    @Override
    public String addMessageListener(MessageListener<E> listener) {
        listenerMap.put("test", listener);
        return "test";
    }

    @Override
    public boolean removeMessageListener(String registrationId) {
        if (listenerMap.remove(registrationId) != null)
            return true;
        else
            return false;
    }

    @Override
    public LocalTopicStats getLocalTopicStats() {
        return stats;
    }

}
