package com.maurice.analytics.cache.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class TopicRelatedModel implements Serializable {

    private static final long serialVersionUID = -9166067955449981999L;

    private List<TopicRoute> topicRouteList = new ArrayList<TopicRoute>();

    public List<TopicRoute> getTopicRouteList() {
        return topicRouteList;
    }


    public void setTopicRouteList(List<TopicRoute> topicRouteList) {
        this.topicRouteList = topicRouteList;
    }
}
