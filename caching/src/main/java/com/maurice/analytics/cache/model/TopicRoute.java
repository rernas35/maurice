package com.maurice.analytics.cache.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TopicRoute implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1318401341839866135L;
    Long topicRouteId;
    String topicName;
    String evaluationScript = "";
    List<TransformScript> transformScriptList = new ArrayList<TransformScript>();
    TopicThroughput throughput = TopicThroughput.LOW;


    public TopicRoute() {
        // TODO Auto-generated constructor stub
    }

    public TopicRoute(Long topicRouteId, String topicName, String evaluationJS,List<TransformScript> transformScriptList) {
        this.topicRouteId = topicRouteId;
        this.topicName = topicName;
        this.evaluationScript = evaluationJS;
        this.transformScriptList = transformScriptList;
    }

    public Long getTopicRouteId() {
        return topicRouteId;
    }

    public void setTopicRouteId(Long topicRouteId) {
        this.topicRouteId = topicRouteId;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public void setEvaluationScript(String evaluationScript) {
        this.evaluationScript = evaluationScript;
    }

    public String getEvaluationScript() {
        return evaluationScript;
    }

    public void setTransformScriptList(List<TransformScript> transformScriptList) {
        this.transformScriptList = transformScriptList;
    }

    public List<TransformScript> getTransformScriptList() {
        return transformScriptList;
    }

    public TopicThroughput getThroughput() {
        return throughput;
    }

    public void setThroughput(TopicThroughput throughput) {
        this.throughput = throughput;
    }


    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TopicRoute)) return false;
        return topicRouteId.equals(((TopicRoute) obj).topicRouteId);
    }

}
