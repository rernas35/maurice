package com.maurice.analytics.cache;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.ITopic;
import com.maurice.analytics.cache.constants.MauriceCacheConstants;
import com.maurice.analytics.cache.model.MapReduceEvent;

public class MauriceAnalyticsMessageBus {

    static {
        MauriceAnalyticsCache.initialize(() -> Hazelcast.newHazelcastInstance());
    }

    public static ITopic<MapReduceEvent> getMapReduceEventStatusTopic() {
        return MauriceAnalyticsCache.getInstance().getTopic(MauriceCacheConstants.EVENT_STATUS_TOPIC_NAME);
    }

    public static void publishMapReduceEventStatus(MapReduceEvent message) {
        getMapReduceEventStatusTopic().publish(message);
    }

    public static ITopic<MapReduceEvent> getMapReduceExecutorCommandTopic() {
        return MauriceAnalyticsCache.getInstance().getTopic(MauriceCacheConstants.EXECUTOR_COMMAND_TOPIC_NAME);
    }

    public static void publishMapReduceExecutorCommand(MapReduceEvent message) {
        getMapReduceExecutorCommandTopic().publish(message);
    }

    public static void main(String[] args) {

    }

}
