package com.maurice.analytics.cache.model;


public enum MapReduceStatus {

    NOT_STARTED, RUNNING, COMPLETED, ERROR, SHUTDOWN_SENT;


}
