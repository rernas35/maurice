package com.maurice.analytics.cache.model;

import java.io.Serializable;

public class TransformScript implements Serializable{

    TransformPhase phase;
    String action;
    String fieldPath;
    String javascript;

    public TransformPhase getPhase() {
        return phase;
    }

    public void setPhase(TransformPhase phase) {
        this.phase = phase;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getFieldPath() {
        return fieldPath;
    }

    public void setFieldPath(String fieldPath) {
        this.fieldPath = fieldPath;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

    public String getJavascript() {
        return javascript;
    }
}
