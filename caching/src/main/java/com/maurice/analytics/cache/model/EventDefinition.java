package com.maurice.analytics.cache.model;


import org.codehaus.jackson.annotate.JsonIgnore;

public class EventDefinition extends TopicRelatedModel implements ICachable<String> {

    private static final long serialVersionUID = 8964848627744607006L;

    private Long id;
    private String name;
    private String description;
    private String ownerName;
    private String externalId;
    private String format;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDesription(String description) {
        this.description = description;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    @JsonIgnore
    public String getKey() {
        return getName();
    }

}
