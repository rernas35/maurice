package com.maurice.analytics.cache.constants;


public class MauriceCacheConstants {

    public static final String EVENT_DEFINITION_CACHE = "event_definition";
    public static final String USER_CACHE = "user";

    public final static String EVENT_STATUS_TOPIC_NAME = "EVENT_STATUS";
    public final static String EXECUTOR_COMMAND_TOPIC_NAME = "EXECUTOR_COMMAND";
}
