package com.maurice.analytics.cache.model;

import java.io.Serializable;
import java.util.Date;

public class MapReduceEvent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8869176422215167344L;

    MapReduceStatus status;
    String executionId;
    Date date;
    String description;

    public MapReduceEvent(MapReduceStatus status, String description) {
        this.status = status;
        this.date = new Date();
        this.description = description;
        // TODO Auto-generated constructor stub
    }

    public String getExecutionId() {
        return executionId;
    }

    public void setExecutionId(String executionId) {
        this.executionId = executionId;
    }

    public MapReduceStatus getStatus() {
        return status;
    }

    public void setStatus(MapReduceStatus status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
