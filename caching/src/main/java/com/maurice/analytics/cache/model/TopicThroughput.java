package com.maurice.analytics.cache.model;


public enum TopicThroughput {

    LOW(10), MEDIUM(40), HIGH(160);

    int partitionCount;


    public int getPartitionCount() {
        return partitionCount;
    }


    private TopicThroughput(int partitionCount) {
        this.partitionCount = partitionCount;
    }

}
