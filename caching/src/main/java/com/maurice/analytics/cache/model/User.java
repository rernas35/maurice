package com.maurice.analytics.cache.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class User implements Serializable,ICachable<String> {

    /**
     *
     */
    private static final long serialVersionUID = -6691481450088373827L;

    private Long id;
    private String name;
    private String externalId;

    private List<String> eventDefinitionNameList = new ArrayList<String>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getEventDefinitionNameList() {
        return eventDefinitionNameList;
    }

    public void addEventDefinitionName(String eventDefinitionName) {
        this.eventDefinitionNameList.add(eventDefinitionName);
    }

    public void removeEventDefinitionName(String eventDefinitionName) {
        this.eventDefinitionNameList.remove(eventDefinitionName);
    }

    @Override
    public String getKey() {
        return getName();
    }

}
