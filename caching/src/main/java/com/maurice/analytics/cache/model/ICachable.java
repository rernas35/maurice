package com.maurice.analytics.cache.model;

import java.io.Serializable;

public interface ICachable<KEY extends Serializable> {

    KEY getKey();

    Long getId();

}
