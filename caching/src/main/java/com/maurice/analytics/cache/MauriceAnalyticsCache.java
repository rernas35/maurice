package com.maurice.analytics.cache;

import com.hazelcast.core.HazelcastInstance;
import com.maurice.analytics.cache.model.EventDefinition;
import com.maurice.analytics.cache.model.TopicRoute;
import com.maurice.analytics.cache.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.maurice.analytics.cache.constants.MauriceCacheConstants.EVENT_DEFINITION_CACHE;
import static com.maurice.analytics.cache.constants.MauriceCacheConstants.USER_CACHE;

;

public class MauriceAnalyticsCache {

    static Logger LOGGER = LoggerFactory.getLogger(MauriceAnalyticsCache.class);

    static HazelcastInstance instance = null;

    public static void initialize(Supplier<HazelcastInstance> hazelcastSupplier) {
        if (instance == null) {
            instance = hazelcastSupplier.get();
        }
    }

    static HazelcastInstance getInstance() {
        return instance;
    }

    public static void putEventDefinition(EventDefinition eventDefinition) {
        getEventDefinitionMap().put(eventDefinition.getName(), eventDefinition);
    }

    public static void removeEventDefinition(String name) {
        getEventDefinitionMap().remove(name);
    }

    static Map<String, EventDefinition> getEventDefinitionMap() {
        return instance.getMap(EVENT_DEFINITION_CACHE);
    }

    public static EventDefinition getEventDefinition(String name) {
        return getEventDefinitionMap().get(name);
    }

    public static List<EventDefinition> getEventDefinitions() {
        Map<String, EventDefinition> map = getEventDefinitionMap();
        return map.entrySet().stream().map(e -> e.getValue()).collect(Collectors.toList());
    }

    public static void putUser(User user) {
        getUserMap().put(user.getName(), user);
    }

    static Map<String, User> getUserMap() {
        return instance.getMap(USER_CACHE);
    }

    public static User getUser(String name) {
        return getUserMap().get(name);
    }

    public static void removeUser(String name) {
        getUserMap().remove(name);
    }

    public static List<User> getUsers() {
        Map<String, User> map = getUserMap();
        Iterator<Entry<String, User>> iterator = map.entrySet().iterator();
        List<User> users = new ArrayList<User>();
        while (iterator.hasNext()) {
            User user = iterator.next().getValue();
            users.add(user);
        }
        return users;
    }

    public static List<TopicRoute> getTopicRoutesByEventDefinitionId(String name) {
        EventDefinition api = (EventDefinition) getEventDefinitionMap().get(name);
        return api.getTopicRouteList();
    }


}
