package com.maurice.analytics.dynamic.event.model.eval.model;

import java.util.ArrayList;
import java.util.List;

public class Value {

    int v;
    List<String> list = new ArrayList<String>();

    public Value(int v,List<String> list){
        this.v = v;
        this.list = list;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    public List<String> getList() {
        return list;
    }


}
