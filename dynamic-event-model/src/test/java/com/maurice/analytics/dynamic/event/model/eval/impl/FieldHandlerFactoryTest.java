package com.maurice.analytics.dynamic.event.model.eval.impl;

import com.maurice.analytics.dynamic.event.model.error.MauriceFormatException;
import com.maurice.analytics.dynamic.event.model.eval.FieldHandler;
import com.maurice.analytics.dynamic.event.model.eval.impl.field.FloatFieldHandler;
import com.maurice.analytics.dynamic.event.model.eval.impl.field.IntegerFieldHandler;
import com.maurice.analytics.dynamic.event.model.eval.impl.field.JSONFieldHandler;
import com.maurice.analytics.dynamic.event.model.eval.impl.field.StringFieldHandler;
import com.maurice.analytics.dynamic.event.model.eval.model.FieldDefinition;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class FieldHandlerFactoryTest {

    @Test
    public void shouldReturnStringFieldHandlerForStringInput(){
        FieldHandler fieldHandler = FieldHandlerFactory.getFieldHandler(new FieldDefinition("name", "string"));
        assertTrue(fieldHandler instanceof StringFieldHandler);
    }

    @Test
    public void shouldReturnIntegerFieldHandlerForIntegerInput(){
        FieldHandler fieldHandler = FieldHandlerFactory.getFieldHandler(new FieldDefinition("name", "integer"));
        assertTrue(fieldHandler instanceof IntegerFieldHandler);
    }

    @Test
    public void shouldReturnFloatFieldHandlerForFloatInput(){
        FieldHandler fieldHandler = FieldHandlerFactory.getFieldHandler(new FieldDefinition("name", "float"));
        assertTrue(fieldHandler instanceof FloatFieldHandler);
    }

    @Test
    public void shouldReturnJSONFieldHandlerForJSONInput(){
        FieldHandler fieldHandler = FieldHandlerFactory.getFieldHandler(new FieldDefinition("name", "json"));
        assertTrue(fieldHandler instanceof JSONFieldHandler);
    }

    @Test(expected = MauriceFormatException.class)
    public void shouldThrowExceptionForUndefinedType(){
        FieldHandler fieldHandler = FieldHandlerFactory.getFieldHandler(new FieldDefinition("name", "xml"));
    }

    @Test
    public void shouldReturnAllFieldHandlers(){
        FieldHandler[] allHandlers = FieldHandlerFactory.getAllHandlers();
        assertTrue(allHandlers[0] instanceof StringFieldHandler);
        assertTrue(allHandlers[1] instanceof IntegerFieldHandler);
        assertTrue(allHandlers[2] instanceof FloatFieldHandler);
        assertTrue(allHandlers[3] instanceof JSONFieldHandler);
    }

}
