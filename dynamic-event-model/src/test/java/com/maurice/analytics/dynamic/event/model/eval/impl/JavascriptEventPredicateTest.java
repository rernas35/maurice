package com.maurice.analytics.dynamic.event.model.eval.impl;


import com.maurice.analytics.dynamic.event.model.error.MauriceEvaluationException;
import com.maurice.analytics.dynamic.event.model.eval.IEventPredicate;
import com.maurice.analytics.dynamic.event.model.eval.model.Value;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.stream.IntStream;


public class JavascriptEventPredicateTest {

    Long startTime;

    @Before
    public void setStartTime(){
        startTime = System.currentTimeMillis();
    }

    @After
    public void printElapsedTime(){
        System.out.println(String.format("Elapsed Time : %d" , System.currentTimeMillis() - startTime));
    }

    @Test
    public void shouldEvaluateJSONObject(){

        IEventPredicate predicate = new JavascriptEventPredicate();
        IntStream.rangeClosed(1 , 10).forEach(x -> {
                    try {
                        predicate.evaluate("{\"v\":"+ x +",\"list\":[\"1\",\"2\",\"3\"]}","var obj = JSON.parse(value);" +
                                                                            "return obj.list.length == 3");
                    } catch (MauriceEvaluationException e) {
                    }
                }
        );

    }

    @Test
    public void shouldEvaluateJavaObject() throws MauriceEvaluationException{
        IEventPredicate predicate = new JavascriptEventPredicate();
        IntStream.rangeClosed(1 , 10).forEach(x -> {
                    try {
                        predicate.evaluate(new Value(x, Arrays.asList(new String[]{"1", "2", "3"})), "return value.list.length == 3");
                    } catch (MauriceEvaluationException e) {
                    }
                }
        );
    }




}
