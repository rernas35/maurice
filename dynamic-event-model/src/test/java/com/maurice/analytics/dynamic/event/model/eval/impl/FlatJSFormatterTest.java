package com.maurice.analytics.dynamic.event.model.eval.impl;

import com.maurice.analytics.dynamic.event.model.error.MauriceEvaluationException;
import com.maurice.analytics.dynamic.event.model.error.MauriceFormatException;
import com.maurice.analytics.dynamic.event.model.eval.TransformContext;
import com.maurice.analytics.dynamic.event.model.eval.model.FieldDefinition;
import com.maurice.analytics.dynamic.event.model.util.FormatUtil;
import jdk.nashorn.api.scripting.JSObject;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;


public class FlatJSFormatterTest {

    FlatJSFormatter formatter = new FlatJSFormatter();

    @Test
    public void shouldSkipStringFieldForSingleLevelTransformation() throws MauriceEvaluationException {
        String format = "${id:integer}|${name:string}|${payload:json}";
        String value = "12|rr|{\"transactionType\":\"refund\"}";
        String valueWithoutName = "12|{\"transactionType\":\"refund\"}";

        JSObject object = formatter.parse(format,value);
        TransformContext transformationContext = new TransformContext();
        transformationContext.addPreTransformationPolicy(new SkipPolicy("name"));
        String serializedValue = formatter.format(format, object, transformationContext);

        assertEquals(valueWithoutName,serializedValue);
    }

    @Test
    public void shouldSkipJsonElementForMultiLevelTransformation() throws MauriceEvaluationException{
        String format = "${id:integer}|${name:string}|${payload:json}";
        String value = "12|rr|{\"transactionType\":\"refund\",\"amount\":1000}";
        String valueWithoutName = "12|rr|{\"transactionType\":\"refund\"}";

        JSObject object = formatter.parse(format,value);
        TransformContext transformationContext = new TransformContext();
        transformationContext.addPreTransformationPolicy(new SkipPolicy("payload.amount"));
        String serializedValue = formatter.format(format, object, transformationContext);

        assertEquals(valueWithoutName,serializedValue);
    }

    @Test
    public void shouldSkipJsonArrayElementForMultiLevelTransformation() throws MauriceEvaluationException{
        String format = "${id:integer}|${name:string}|${payload:json}";
        String value = "12|rr|{\"transaction\":" +
                                                 "{" +
                                                    "\"types\":[" +
                                                                "{\"purchase\":1000}," +
                                                                "{\"refund\":2000}" +
                                                               "]" +
                                                 "}" +
                                "}";
        String valueWithoutName = "rr|{\"transaction\":{}}";

        JSObject object = formatter.parse(format,value);
        TransformContext transformationContext = new TransformContext();
        transformationContext.addPreTransformationPolicy(new SkipPolicy("payload.transaction.types"));
        transformationContext.addPreTransformationPolicy(new SkipPolicy("id"));
        String serializedValue = formatter.format(format, object, transformationContext);

        assertEquals(valueWithoutName,serializedValue);
    }

    @Test
    public void shouldSkipJsonArrayElementSubFieldForMultiLevelTransformation() throws MauriceEvaluationException{
        String format = "${id:integer}|${name:string}|${payload:json}";
        String value = "12|rr|{\"transaction\":" +
                                                "{" +
                                                    "\"types\":[" +
                                                                "{\"purchase\":1000,\"enabled\":\"true\"}," +
                                                                "{\"refund\":2000,\"enabled\":\"true\"}" +
                                                              "]" +
                                                "}" +
                                            "}";
        String valueWithoutName = "rr|{\"transaction\":" +
                                                        "{" +
                                                            "\"types\":[" +
                                                                        "{\"purchase\":1000}," +
                                                                        "{\"refund\":2000}" +
                                                                      "]" +
                                                        "}" +
                                        "}";

        JSObject object = formatter.parse(format,value);
        TransformContext transformationContext = new TransformContext();
        transformationContext.addPreTransformationPolicy(new SkipPolicy("payload.transaction.types.enabled"));
        transformationContext.addPreTransformationPolicy(new SkipPolicy("id"));
        String serializedValue = formatter.format(format, object, transformationContext);

        assertEquals(valueWithoutName,serializedValue);
    }

    @Test
    public void shouldSkipJsonFieldForSingleLevelTransformation() throws MauriceEvaluationException{
        String format = "${id:integer}|${name:string}|${payload:json}";
        String value = "12|rr|{\"transactionType\":\"refund\"}";
        String valueWithoutName = "12|rr";

        JSObject object = formatter.parse(format,value);
        TransformContext transformationContext = new TransformContext();
        transformationContext.addPreTransformationPolicy(new SkipPolicy("payload"));
        String serializedValue = formatter.format(format, object, transformationContext);

        assertEquals(valueWithoutName,serializedValue);
    }

    @Test
    public void shouldSkipIntegerFieldForSingleLevelTransformation() throws MauriceEvaluationException{
        String format = "${id:integer}|${name:string}|${payload:json}";
        String value = "12|rr|{\"transactionType\":\"refund\"}";
        String valueWithoutName = "rr|{\"transactionType\":\"refund\"}";

        JSObject object = formatter.parse(format,value);
        TransformContext transformationContext = new TransformContext();
        transformationContext.addPreTransformationPolicy(new SkipPolicy("id"));
        String serializedValue = formatter.format(format, object, transformationContext);

        assertEquals(valueWithoutName,serializedValue);
    }

    @Test
    public void shouldReturnEntityFlatFieldsAndJSONFields() throws MauriceEvaluationException{
        String format = "${id:float}|${name:string}|${payload:json}";
        String value = "12|rr|{\"transactionType\":\"refund\"}";
        JSObject object = formatter.parse(format,value);

        Object entityId = formatter.evaluate(object, "function evaluate(value){return value.id;}","evaluate");
        assertEquals( 12.0, entityId);

        Object entityName = formatter.evaluate(object, "function evaluate(value){return value.name;}","evaluate");
        assertEquals( "rr",entityName);

        Object entityPayloadBody = formatter.evaluate(object, "function evaluate(value){return value.payload.transactionType;}","evaluate");
        assertEquals( "refund",entityPayloadBody);

        formatter.format(format,object,new TransformContext());
    }


    @Test
    public void shouldParseStringAndSerializeWithTheSameValues  () throws MauriceEvaluationException{
        String format = "${id:integer}|${name:string}|${payload:json}";
        String value = "12|rr|{\"transaction\":{\"type\":\"refund\",\"amount\":2000}}";
        JSObject object = formatter.parse(format,value);
        String serialized = formatter.format(format, object,new TransformContext());
        assertEquals(value,serialized);
    }

    @Test
    public void shouldParseStringAndSerializeWithSingleJSONValue  () throws MauriceEvaluationException{
        String format = "${payload:json}";
        String value = "{\"transaction\":{\"type\":\"refund\",\"amount\":2000}}";
        JSObject object = formatter.parse(format,value);
        String serialized = formatter.format(format, object,new TransformContext());
        assertEquals(value,serialized);
    }

    @Test
    public void shouldParseStringAndSerializeWithTheSameValuesForArrays  () throws MauriceEvaluationException{
        String format = "${id:integer}|${name:string}|${payload:json}";
        String value = "12|rr|{\"transaction\":{\"types\":[\"refund\",\"purchase\"]}}";
        JSObject object = formatter.parse(format,value);
        String serialized = formatter.format(format, object,new TransformContext());
        assertEquals(value,serialized);
    }

    @Test
    public void shouldParseStringAndSerializeWithTheSameValuesForArrayInArray  () throws MauriceEvaluationException{
        String format = "${id:integer}|${name:string}|${payload:json}";
        String value = "12|rr|{\"transaction\":" +
                                    "{\"types\":[[\"refund\",\"purchase\"]," +
                                                "[\"refund\"]" +
                                               "]" +
                                    "}" +
                              "}";
        JSObject object = formatter.parse(format,value);
        String serialized = formatter.format(format, object,new TransformContext());
        assertEquals(value,serialized);
    }

    @Test
    public void shouldParseStringAndSerializeWithTheSameValuesForJsObjectInArray  () throws MauriceEvaluationException{
        String format = "${id:integer}|${name:string}|${payload:json}";
        String value = "12|rr|{\"transaction\":" +
                                                "{" +
                                                        "\"types\":[" +
                                                                        "{\"purchase\":1000}," +
                                                                        "{\"refund\":2000}" +
                                                                    "]" +
                                                "}" +
                                "}";
        JSObject object = formatter.parse(format,value);
        String serialized = formatter.format(format, object,new TransformContext());
        assertEquals(value,serialized);
    }

    @Test
    public void shouldRegexExtractVariableNameAndTypeForSingleFieldDefinitionNoNumericInName() throws MauriceFormatException{
        FieldDefinition fieldDefinition = FormatUtil.getFieldDefinition("${fieldName:string}");
        assertEquals(fieldDefinition.getName(),"fieldName");
        assertEquals(fieldDefinition.getType(),"string");
    }

    @Test
    public void shouldRegexExtractVariableNameAndTypeForSingleFieldDefinitionNumierInName() throws MauriceFormatException{
        FieldDefinition fieldDefinition = FormatUtil.getFieldDefinition("${fieldName01:string}");
        assertEquals(fieldDefinition.getName(),"fieldName01");
        assertEquals(fieldDefinition.getType(),"string");
    }

    @Test(expected = MauriceFormatException.class)
    public void shouldRegexExtractVariableNameAndTypeForSingleFieldDefinitionNonExistingType() throws MauriceFormatException{
        FieldDefinition fieldDefinition = FormatUtil.getFieldDefinition("${fieldName:double}");
    }

    @Test
    public void shouldExtractDelimiterDynamicallyAndSuccessWithPipe() throws MauriceFormatException{
        Optional<String> delimiter = FormatUtil.getDelimiter("${id:integer}|${name:string}|${payload:json}");
        assertEquals("|",delimiter.get());
    }

    @Test
    public void shouldExtractDelimiterDynamicallyAndSuccessWithSlash() throws MauriceFormatException{
        Optional<String> delimiter = FormatUtil.getDelimiter("${id:integer}/${name:string}/${payload:json}");
        assertEquals("/",delimiter.get());
    }

    @Test(expected = MauriceFormatException.class)
    public void shouldFailWhileExtractingDelimitersForMultipleDelimiters() throws MauriceFormatException{
        FormatUtil.getDelimiter("${id:integer}|${name:string}&${payload:json}");
    }


}
