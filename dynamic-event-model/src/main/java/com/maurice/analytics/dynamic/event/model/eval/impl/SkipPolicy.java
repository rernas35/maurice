package com.maurice.analytics.dynamic.event.model.eval.impl;

import com.maurice.analytics.dynamic.event.model.eval.PreTransformPolicy;

public class SkipPolicy extends PreTransformPolicy{
    public SkipPolicy(String path) {
        super(path);
    }
}
