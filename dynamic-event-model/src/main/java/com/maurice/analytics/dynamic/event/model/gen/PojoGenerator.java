package com.maurice.analytics.dynamic.event.model.gen;

import com.maurice.analytics.dynamic.event.model.eval.impl.FlatJSFormatter;
import com.maurice.analytics.dynamic.event.model.eval.model.FieldDefinition;
import com.maurice.analytics.dynamic.event.model.util.FormatUtil;
import javassist.*;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ConstPool;
import javassist.bytecode.annotation.Annotation;
import jdk.nashorn.api.scripting.JSObject;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;


public class PojoGenerator {

    public static String PACKAGE_NAME = "maurice.gen.";
    public static FlatJSFormatter formatter = new FlatJSFormatter();

    public static class GenerationContext{
        final boolean igniteAnnotationRequired;

         public GenerationContext(boolean igniteAnnotationRequired){
             this.igniteAnnotationRequired = igniteAnnotationRequired;
         }

    }

    public static Class<Serializable> generateClass(String name, String format) throws CannotCompileException,NotFoundException {
            Optional<String> delimiter = FormatUtil.getDelimiter(format);
            Map<String, FieldDefinition> fieldMap = FormatUtil.getFieldDefinitionMap(FormatUtil.getStringArray(format,delimiter));

            ClassPool pool = ClassPool.getDefault();
            CtClass clazz = pool.makeClass(PACKAGE_NAME + makeFirstLetterUpperCase(name));
            clazz.setInterfaces(new CtClass[]{pool.get("java.io.Serializable")});
            organizeImport(pool,clazz);

            fieldMap.entrySet().forEach(e -> {
                try {
                    FieldDefinition value = e.getValue();
                    if (value.getType().equals("json"))
                        return;
                    String fieldDeclaration = String.format("public %s %s;" ,makeFirstLetterUpperCase( value.getType()),value.getName());
                    CtField f = CtField.make(fieldDeclaration,clazz);
                    organizeAnnotation(clazz,f);
                    clazz.addField(f);
                }catch (CannotCompileException ex){
                    ex.printStackTrace();
                }
            });

            return clazz.toClass(PojoGenerator.class.getClassLoader(), PojoGenerator.class.getProtectionDomain());


    }

    public static Object initiateObject(Class<Serializable> clazz,String format,String valueStr){
        try{
            Object retObject = clazz.newInstance();
            JSObject jsObject = formatter.parse(format,valueStr);
            Arrays.stream(clazz.getFields()).forEach(f -> {
                try {
                    f.set(retObject,convert(f,jsObject.getMember(f.getName())));
                }catch (IllegalAccessException e){
                    e.printStackTrace();
                }

            });
            return retObject;
        }catch (Exception rex){
            return null;
        }
    }

    private static Object convert(Field field,Object value){
        if (field.getType().equals(Integer.class)){
            return ((Double)value).intValue();
        }else if (field.getType().equals(Float.class)){
            return ((Double)value).floatValue();
        }
        return value;
    }


    private static void organizeImport(ClassPool pool,CtClass ctClazz){
        pool.importPackage("org.apache.ignite.cache.query.annotations");
    }

    private static void organizeAnnotation(CtClass ctClazz, CtField field){
        ConstPool constPool = ctClazz.getClassFile().getConstPool();
        AnnotationsAttribute attr = new AnnotationsAttribute(constPool, AnnotationsAttribute.visibleTag);
        Annotation annot = new Annotation("org.apache.ignite.cache.query.annotations.QuerySqlField", constPool);

        attr.addAnnotation(annot);
        field.getFieldInfo().addAttribute(attr);
    }

    public static String makeFirstLetterUpperCase(String value){
        return value.substring(0,1).toUpperCase() + value.substring(1);
    }

    public static void main(String... args) throws Exception{
        String format = "${createDate:string}|${id:integer}|${payload:json}";
        Class c = generateClass("adminEvent", format);
        Object obj = initiateObject(c,format,"falan|1|{\"bisi\":1}");
        System.out.println(c.getFields().length);
        System.out.println(Class.forName(PACKAGE_NAME + "AdminEvent"));
    }


}
