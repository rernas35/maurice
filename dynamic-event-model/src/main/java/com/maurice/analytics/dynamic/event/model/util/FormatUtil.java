package com.maurice.analytics.dynamic.event.model.util;

import com.maurice.analytics.dynamic.event.model.error.MauriceFormatException;
import com.maurice.analytics.dynamic.event.model.eval.model.FieldDefinition;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class FormatUtil {

    public final static String REGEX_FORMAT_ITEM_PATTERN = "^\\$\\{([a-zA-Z0-9]*)\\:(integer|string|float|json)\\}";
    public final static String REGEX_MAIN_DELIMITER_PATTERN = "\\}([^\\$\\{)])\\$";
    public final static String LINE_SEPERATOR = System.lineSeparator();

    public static Map<String, FieldDefinition> getFieldDefinitionMap(String... fieldArray) {
        return Arrays.stream(fieldArray).map(s -> getFieldDefinition(s)).collect(Collectors.toMap(FieldDefinition::getName, Function.identity()));
    }

    public static FieldDefinition getFieldDefinition(String fieldFormat) throws MauriceFormatException {
        Pattern pattern = Pattern.compile(REGEX_FORMAT_ITEM_PATTERN);
        Matcher matcher = pattern.matcher(fieldFormat);
        if (matcher.find())
            return new FieldDefinition(matcher.group(1),matcher.group(2));
        else
            throw new MauriceFormatException(String.format("Field definition is wrong : %s" , fieldFormat));
    }


    public static Optional<String> getDelimiter(String format) throws MauriceFormatException {
        Pattern pattern = Pattern.compile(REGEX_MAIN_DELIMITER_PATTERN);
        Matcher matcher = pattern.matcher(format);
        Optional<String> candidateDelimiter = Optional.empty();
        while (matcher.find()){
            String tempDelimiter = matcher.group(1);
            if (candidateDelimiter.isPresent()){
                if (!candidateDelimiter.get().equals(tempDelimiter) ) {
                    throw new MauriceFormatException(String.format("Delimiter is not set properly.Multiple delimiters are caught --> %s , %s", candidateDelimiter, tempDelimiter));
                }
            } else {
                candidateDelimiter = Optional.of(tempDelimiter);
            }
        }
        return candidateDelimiter;
    }

    public static String[] getStringArray(String format, Optional<String> delimiter){
        if (delimiter.isPresent())
            return format.split(Pattern.quote(delimiter.get()));
        else {
            String[] retArray = {format};
            return retArray;
        }

    }
}
