package com.maurice.analytics.dynamic.event.model.eval;

import com.maurice.analytics.dynamic.event.model.error.MauriceEvaluationException;

public interface IEventPredicate {

    Boolean evaluate(Object value,String predicationScript) throws MauriceEvaluationException;

}
