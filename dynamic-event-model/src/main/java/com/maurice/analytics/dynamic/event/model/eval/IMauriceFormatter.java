package com.maurice.analytics.dynamic.event.model.eval;


public interface IMauriceFormatter<O> {

    O parse(String format, String inputObject);


    String format(String format, O o , TransformContext transformationContext);

}
