package com.maurice.analytics.dynamic.event.model.eval;

import java.util.HashMap;
import java.util.Map;

public class TransformContext {

    Map<String,PreTransformPolicy> preTransformPolicyMap = new HashMap<>();

    Map<String,PostTransformPolicy> postTransformPolicyMap = new HashMap<>();

    public Map<String,PreTransformPolicy> getPreTransformationPolicies() {
        return preTransformPolicyMap;
    }

    public Map<String,PostTransformPolicy> getPostTransformationPolicies() {
        return postTransformPolicyMap;
    }

    public void addPreTransformationPolicy(PreTransformPolicy policy){
        getPreTransformationPolicies().put(policy.getPath() , policy);
    }

    public  void addPostTransformationPolicy(PostTransformPolicy policy){
        getPostTransformationPolicies().put(policy.getPath(), policy);
    }

    public PostTransformPolicy getPostTransformPolicy(String path){
        return postTransformPolicyMap.get(path);
    }

    public PreTransformPolicy getPreTransformPolicy(String path){
        return  preTransformPolicyMap.get(path);
    }

}
