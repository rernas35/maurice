package com.maurice.analytics.dynamic.event.model.eval.impl.field;

import com.maurice.analytics.dynamic.event.model.eval.FieldHandler;
import com.maurice.analytics.dynamic.event.model.eval.TransformContext;

public class IntegerFieldHandler extends FieldHandler<Integer> {
    public static final String CONVERT_TO_INTEGER = "convertToInteger";

    public String getConverterMethod(){
        return "function " + CONVERT_TO_INTEGER + "(fieldValue){" +
                "return parseInt(fieldValue);"+
                "}" + System.lineSeparator();
    }

    public String getConverterMethodName(){
        return CONVERT_TO_INTEGER;
    }

    @Override
    public boolean matches(String type) {
        return type.equals("integer");
    }

    @Override
    public String serializeInternal(Object aDouble, TransformContext transformationContext, String path) {
        return ((Double)aDouble).intValue() + "";
    }
}
