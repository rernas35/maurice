package com.maurice.analytics.dynamic.event.model.eval.impl;

import com.maurice.analytics.dynamic.event.model.error.MauriceEvaluationException;
import com.maurice.analytics.dynamic.event.model.error.MauriceFormatException;
import com.maurice.analytics.dynamic.event.model.eval.FieldHandler;
import com.maurice.analytics.dynamic.event.model.eval.IMauriceJSFormatter;
import com.maurice.analytics.dynamic.event.model.eval.PreTransformPolicy;
import com.maurice.analytics.dynamic.event.model.eval.TransformContext;
import com.maurice.analytics.dynamic.event.model.eval.model.FieldDefinition;
import com.maurice.analytics.dynamic.event.model.util.FormatUtil;
import jdk.nashorn.api.scripting.JSObject;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class FlatJSFormatter extends JSEngine<JSObject> implements IMauriceJSFormatter {


    public String format(String format, JSObject object, TransformContext transformationContext) {
        Optional<String> delimiter = FormatUtil.getDelimiter(format);
        Map<String, FieldDefinition> fieldMap = FormatUtil.getFieldDefinitionMap(FormatUtil.getStringArray(format,delimiter));
        return object.keySet().stream()
                            .filter(k ->{
                                            PreTransformPolicy policy = transformationContext.getPreTransformPolicy(k);
                                            return policy == null || !(policy instanceof SkipPolicy);
                                        })
                            .map(fieldName ->   {
                                                    FieldDefinition fieldDefinition = fieldMap.get(fieldName);
                                                    FieldHandler fieldHandler = FieldHandlerFactory.getFieldHandler(fieldDefinition);
                                                    return fieldHandler.serialize(object.getMember(fieldName),transformationContext,fieldName);
                                                })
                            .collect(Collectors.joining(delimiter.orElse("")));
    }


    public JSObject parse(String format, String inputObject) {
        String parsingScript = getParsingScript(format);
        return evaluate(inputObject,parsingScript);
    }


    public JSObject evaluate(Object value, String finalScript) throws MauriceEvaluationException {
        return super.evaluate(value, finalScript, "convertEntity");
    }

    @Override
    public String getParsingScript(String format) throws MauriceFormatException {
        Optional<String> delimiter = FormatUtil.getDelimiter(format);
        StringBuffer script = new StringBuffer();
        putConverterMethods(script);
        appendFunctionHeader(delimiter, script);
        String[] fieldStrArray = FormatUtil.getStringArray(format,delimiter);
        for( int i=0;i<fieldStrArray.length; i++) {
                String f = fieldStrArray[i];
                FieldDefinition fieldDefinition = FormatUtil.getFieldDefinition(f);
                FieldHandler fieldHandler = FieldHandlerFactory.getFieldHandler(fieldDefinition);
                script.append(" " + fieldHandler.getAssignmentJSCode(fieldDefinition.getName(),String.valueOf(i)));
            script.append(FormatUtil.LINE_SEPERATOR);
        }
        appendEnclosingMethod(script);
        return script.toString();
    }

    private void appendFunctionHeader(Optional<String> delimiter, StringBuffer script) {
        script.append("function convertEntity(value){");
        script.append(FormatUtil.LINE_SEPERATOR);
        script.append(" var entity = new Object();");
        script.append(FormatUtil.LINE_SEPERATOR);
        if (delimiter.isPresent()){
            script.append(  String.format(" var fieldArray = value.split('%s');",delimiter.get()));
        }else {
            script.append(  String.format(" var fieldArray = [value];",delimiter));
        }
            script.append(FormatUtil.LINE_SEPERATOR);
    }

    private void appendEnclosingMethod(StringBuffer script) {
        script.append(" return entity;");
        script.append(FormatUtil.LINE_SEPERATOR);
        script.append("}");
    }

    private void putConverterMethods(StringBuffer script) {
        Arrays.stream(FieldHandlerFactory.getAllHandlers()).forEach(f -> script.append(f.getConverterMethod()));
    }


}
