package com.maurice.analytics.dynamic.event.model.eval;

public class TransformPolicy<E> {

    String path;

    public TransformPolicy(String path){
        this.path = path;
    }

    public E format(E e){
        return e;
    }

    public String getPath() {
        return path;
    }
}
