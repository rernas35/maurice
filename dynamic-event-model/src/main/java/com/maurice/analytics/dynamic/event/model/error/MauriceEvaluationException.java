package com.maurice.analytics.dynamic.event.model.error;

public class MauriceEvaluationException extends RuntimeException {
    public MauriceEvaluationException(String message, Throwable cause) {
        super(message, cause);
    }

}
