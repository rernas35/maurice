package com.maurice.analytics.dynamic.event.model.eval;

import com.maurice.analytics.dynamic.event.model.error.MauriceFormatException;
import jdk.nashorn.api.scripting.JSObject;


public interface IMauriceJSFormatter extends IMauriceFormatter<JSObject> {

    @Override
    JSObject parse(String format, String inputStr);

    String getParsingScript(String format) throws MauriceFormatException;
}
