package com.maurice.analytics.dynamic.event.model.util;

import javassist.ClassPool;
import javassist.CtClass;
import org.apache.ignite.configuration.CacheConfiguration;

import static javassist.bytecode.SignatureAttribute.*;


public class IgniteUtil {

    public static String PACKAGE_NAME = "maurice.gen.cache.";

    public static CacheConfiguration getCacheConfiguration(Class keyClazz, Class valueClazz, String cacheName)  {
        try {
            ClassPool pool = ClassPool.getDefault();
            CtClass cc = pool.makeClass(PACKAGE_NAME + cacheName);

            CtClass configurationClass = pool.get("org.apache.ignite.configuration.CacheConfiguration");
            cc.setSuperclass(configurationClass);

            ClassSignature cs = new ClassSignature(null,
                    // Set interface and its generic params
                    new ClassType("org.apache.ignite.configuration.CacheConfiguration",
                            new TypeArgument[]{
                                    new TypeArgument(new ClassType(keyClazz.getCanonicalName())),
                                    new TypeArgument(new ClassType(valueClazz.getCanonicalName()))
                            }), null);

            cc.setGenericSignature(cs.encode());

            Class c = cc.toClass(IgniteUtil.class.getClassLoader(), IgniteUtil.class.getProtectionDomain());
            CacheConfiguration conf = (CacheConfiguration) c.newInstance();
            conf.setName(cacheName);
            return conf;
        }catch(Exception ex){
            return null;
        }
    }

    public static void main(String... args) throws Exception{
        CacheConfiguration cc = getCacheConfiguration(Long.class,String.class,"Falan");
        cc.setTypes(String.class,String.class );
        System.out.println(cc);

    }
}
