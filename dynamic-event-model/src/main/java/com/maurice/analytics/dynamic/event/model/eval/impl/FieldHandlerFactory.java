package com.maurice.analytics.dynamic.event.model.eval.impl;

import com.maurice.analytics.dynamic.event.model.error.MauriceFormatException;
import com.maurice.analytics.dynamic.event.model.eval.FieldHandler;
import com.maurice.analytics.dynamic.event.model.eval.impl.field.FloatFieldHandler;
import com.maurice.analytics.dynamic.event.model.eval.impl.field.IntegerFieldHandler;
import com.maurice.analytics.dynamic.event.model.eval.impl.field.JSONFieldHandler;
import com.maurice.analytics.dynamic.event.model.eval.impl.field.StringFieldHandler;
import com.maurice.analytics.dynamic.event.model.eval.model.FieldDefinition;

public class FieldHandlerFactory {

    private static final FieldHandler[] ALL_HANDLERS = {new StringFieldHandler(),
                                                        new IntegerFieldHandler(),
                                                        new FloatFieldHandler(),
                                                        new JSONFieldHandler()};

    public static FieldHandler getFieldHandler(FieldDefinition fieldDefinition) {
        for (FieldHandler handler: ALL_HANDLERS) {
            if (handler.matches(fieldDefinition.getType()))
                return handler;
        }
        throw new MauriceFormatException(String.format("No Handler is defined for %s" , fieldDefinition.getType()));
    }

    public static FieldHandler[] getAllHandlers() {
        return ALL_HANDLERS;
    }
}
