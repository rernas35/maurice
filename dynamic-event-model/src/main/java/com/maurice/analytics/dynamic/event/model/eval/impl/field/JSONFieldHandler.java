package com.maurice.analytics.dynamic.event.model.eval.impl.field;

import com.maurice.analytics.dynamic.event.model.eval.FieldHandler;
import com.maurice.analytics.dynamic.event.model.eval.PreTransformPolicy;
import com.maurice.analytics.dynamic.event.model.eval.TransformContext;
import com.maurice.analytics.dynamic.event.model.eval.impl.SkipPolicy;
import jdk.nashorn.api.scripting.JSObject;

import java.util.stream.Collectors;

public class JSONFieldHandler extends FieldHandler<JSObject> {
    public static final String CONVERT_TO_JSON = "convertToJSON";
    public static final String PATH_CONNECTOR = ".";

    public String getConverterMethod(){
        return "function " + CONVERT_TO_JSON + "(fieldValue){" +
                "return JSON.parse(fieldValue);"+
                "}" + System.lineSeparator();
    }

    public String getConverterMethodName(){
        return CONVERT_TO_JSON;
    }

    @Override
    public boolean matches(String type) {
        return type.equals("json");
    }

    @Override
    public String serializeInternal(Object object, TransformContext transformationContext, String path) {
        JSObject jsObject = (JSObject) object;
        String jsonBody = jsObject.keySet()
                                    .stream()
                                    .filter(k -> {
                                                    PreTransformPolicy policy = transformationContext.getPreTransformPolicy(path + PATH_CONNECTOR + k);
                                                    return policy == null || !(policy instanceof SkipPolicy);
                                                })
                                    .map(k ->   {
                                                Object value = jsObject.getMember(k);
                                                String jsonFormat = (value instanceof String) ? "\"%s\":\"%s\"" : "\"%s\":%s";
                                                if (value instanceof JSObject)
                                                    if (((JSObject) value).isArray())
                                                        value = serializeArray((JSObject) value,transformationContext,path + PATH_CONNECTOR + k);
                                                    else
                                                        value = serializeInternal(value,transformationContext, path + PATH_CONNECTOR + k );
                                                return String.format(jsonFormat, k, value);
                                                })
                                    .collect(Collectors.joining(","));


        return "{" + jsonBody + "}";
    }

    private String serializeArray(JSObject jsObjectArray, TransformContext transformationContext, String path ){
        return "[" +  jsObjectArray.values()
                            .stream()
                            .map(item ->    {
                                            if (item instanceof JSObject)
                                                if (((JSObject)item).isArray())
                                                    return serializeArray(((JSObject)item),transformationContext,path);
                                                else
                                                    return serializeInternal(item,transformationContext,path);
                                            if (item instanceof String)
                                                return "\"" + item +  "\"";
                                            else
                                                return item + "";
                                           } )
                            .collect(Collectors.joining(",")) + "]";


    }
}
