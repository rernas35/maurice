package com.maurice.analytics.dynamic.event.model.eval.impl.field;

import com.maurice.analytics.dynamic.event.model.eval.FieldHandler;
import com.maurice.analytics.dynamic.event.model.eval.TransformContext;

public class FloatFieldHandler extends FieldHandler<Double> {
    public static final String CONVERT_TO_FLOAT = "convertToFloat";

    public String getConverterMethod(){
        return "function " + CONVERT_TO_FLOAT + "(fieldValue){" +
                "return parseFloat(fieldValue);"+
                "}" + System.lineSeparator();
    }

    public String getConverterMethodName(){
        return CONVERT_TO_FLOAT;
    }

    @Override
    public boolean matches(String type) {
        return type.equals("float");
    }

    @Override
    public String serializeInternal(Object aDouble, TransformContext transformationContext, String path) {
        return ((Double)aDouble).toString() ;
    }
}
