package com.maurice.analytics.dynamic.event.model.eval.model;

public class FieldDefinition {

    final String name;
    final String type;

    public FieldDefinition(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }
}
