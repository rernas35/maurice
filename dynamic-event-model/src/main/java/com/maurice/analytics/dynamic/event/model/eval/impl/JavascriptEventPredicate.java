package com.maurice.analytics.dynamic.event.model.eval.impl;

import com.maurice.analytics.dynamic.event.model.error.MauriceEvaluationException;
import com.maurice.analytics.dynamic.event.model.eval.IEventPredicate;

public class JavascriptEventPredicate extends JSEngine<Boolean> implements IEventPredicate{


    private static String FUNCTION_TEMPLATE =   "function evaluate(value){" +
                                                " %s " +
                                                "}";

    public JavascriptEventPredicate() {
        super();
    }

    @Override
    public Boolean evaluate(Object value, String predicationScript) throws MauriceEvaluationException {
            String finalScript = String.format(FUNCTION_TEMPLATE,predicationScript);
            return super.evaluate(value,finalScript,"evaluate");
    }


}
