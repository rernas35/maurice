package com.maurice.analytics.dynamic.event.model.eval.impl.field;

import com.maurice.analytics.dynamic.event.model.eval.FieldHandler;
import com.maurice.analytics.dynamic.event.model.eval.TransformContext;

public class StringFieldHandler extends FieldHandler<String> {
    public static final String CONVERT_TO_STRING = "convertToString";

    public String getConverterMethod(){
            return "function " + CONVERT_TO_STRING + "(fieldValue){" +
                    "return fieldValue + \"\";"+
                    "}" + System.lineSeparator();
    }

    public String getConverterMethodName(){
        return CONVERT_TO_STRING;
    }

    @Override
    public boolean matches(String type) {
        return type.equals("string");
    }

    @Override
    public String serializeInternal(Object s, TransformContext transformationContext, String path) {
        return s.toString();
    }
}
