package com.maurice.analytics.dynamic.event.model.error;

public class MauriceFormatException extends RuntimeException {

    public MauriceFormatException(String message) {
        super(message);
    }

    public MauriceFormatException(String message, Throwable cause) {
        super(message, cause);
    }

}
