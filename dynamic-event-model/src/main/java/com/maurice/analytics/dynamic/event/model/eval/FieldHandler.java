package com.maurice.analytics.dynamic.event.model.eval;

public abstract class FieldHandler<S> {

    public abstract String getConverterMethod();

    public abstract String getConverterMethodName();

    public String getAssignmentJSCode(String name,String index) {
        return String.format("entity.%s = %s(fieldArray[%s]);" + System.lineSeparator(),name,getConverterMethodName(),index);
    }

    public abstract boolean matches(String type);

    public abstract String serializeInternal(Object s, TransformContext transformationContext, String path);

    public String serialize(Object s, TransformContext transformationContext, String path){
        String serialized = serializeInternal(s,transformationContext,path);
        PostTransformPolicy policy = transformationContext.getPostTransformPolicy(path);
        if (policy != null){
            return policy.format(serialized);
        }
        return  serialized;
    }
}
