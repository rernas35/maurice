package com.maurice.analytics.mapreduce.error;


public class MauriceMRRuntimeException extends Exception {

   
   
   public MauriceMRRuntimeException(String message) {
      super(message);
   }
   
}
