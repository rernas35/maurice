package com.maurice.analytics.mapreduce.result;

import java.io.OutputStream;
import java.io.PrintWriter;

public class ResultWriter extends PrintWriter {
   
   public ResultWriter(OutputStream out, boolean autoFlush) {
      super(out, autoFlush);
   }
   
   @Override
   public void write(char[] buf) {
      super.write(buf);
   }
   
   @Override
   public void write(String s) {
      super.write(s);
   }
   
   
   
}
