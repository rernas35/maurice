package com.maurice.analytics.mapreduce.interpreter;

import scala.tools.nsc.interpreter.Results;

import java.util.Map;

public interface IMapReduceInterpreter {
   
   void initialize(Map<String,String> coniguration);
   
   void execute(String... line);
   
   void shutdown();
   
   void startCommandListener();
   
   void stopCommandListener();
   
   Long getExecutionId();
   
   Results.Result interpret(String line);
   
   void setWaiting();
   
}
