package com.maurice.analytics.mapreduce.config;

import com.maurice.analytics.common.configuration.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class MapReduceConfiguration extends Configuration{
   
   static Logger logger = LoggerFactory.getLogger(MapReduceConfiguration.class);
   
   static MapReduceConfiguration instance =new MapReduceConfiguration(); 
   static {
      try {
          instance.load("mapreduce.properties");
      } catch (IOException  e) {
         logger.error("error while reading mapreduce configuration " , e);
         System.exit(-1);
      } 
   }
   
   public static String getJDBCUrl(){
      return instance.getProperty("jdbc.url");
   }
   
   public static String getJDBCUser(){
      return instance.getProperty("jdbc.username");
   }
   
   public static String getJDBCPassword(){
      return instance.getProperty("jdbc.password");
   }
   
   public static String getJDBCDriver(){
      String driver = instance.getProperty("jdbc.driver");
      if (driver == null)
         return "org.postgresql.Driver";
      return driver;
   }
   
   public static String getMRRestUrl(){
      String restURL = instance.getProperty("mr.rest.url");
      return restURL;
   }
   
   public static Integer getWaitingCheckPeriod(){
      String waitingCheckPeriod = instance.getProperty("waiting.check.period");
      if (waitingCheckPeriod == null){
         logger.warn("waiting.check.period is not set. Default value is set as 10 seconds");
         return 10;
      }
      try {
         return Integer.parseInt(waitingCheckPeriod);
      } catch (NumberFormatException nfe) {
         logger.error("waiting.check.period must be numeric",nfe);
         logger.warn("waiting.check.period is not set. Default value is set as 10 seconds");
         return 10;
      }
   } 

}
