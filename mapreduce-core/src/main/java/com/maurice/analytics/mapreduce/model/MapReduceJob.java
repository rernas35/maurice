package com.maurice.analytics.mapreduce.model;

import com.maurice.analytics.cache.model.MapReduceStatus;

public class MapReduceJob {
   
   Long id;
   String name;
   MapReduceStatus status;
   
   
   public Long getId() {
      return id;
   }
   
   public void setId(Long id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }
   
   public void setName(String name) {
      this.name = name;
   }
   
   public MapReduceStatus getStatus() {
      return status;
   }
   
   public void setStatus(MapReduceStatus status) {
      this.status = status;
   }
   
}
