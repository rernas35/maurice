package com.maurice.analytics.mapreduce;

import com.maurice.analytics.cache.MauriceAnalyticsMessageBus;
import com.maurice.analytics.cache.model.MapReduceEvent;
import com.maurice.analytics.cache.model.MapReduceStatus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class StopInterpreter {

   public static void main(String[] args) throws IOException {
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      System.out.print("enter the name of the job: ");
      while (true) {
         String name = br.readLine();
         MapReduceEvent e = new MapReduceEvent(MapReduceStatus.SHUTDOWN_SENT, "shutdown");
         MauriceAnalyticsMessageBus.publishMapReduceExecutorCommand(e);


      }
   }
   
}
