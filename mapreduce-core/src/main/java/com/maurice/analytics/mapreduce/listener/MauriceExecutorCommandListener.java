package com.maurice.analytics.mapreduce.listener;

import com.hazelcast.core.Message;
import com.hazelcast.core.MessageListener;
import com.maurice.analytics.cache.model.MapReduceEvent;
import com.maurice.analytics.cache.model.MapReduceStatus;
import com.maurice.analytics.mapreduce.interpreter.IMapReduceInterpreter;

public class MauriceExecutorCommandListener implements MessageListener<MapReduceEvent> {

	IMapReduceInterpreter interpreter;

	public MauriceExecutorCommandListener(IMapReduceInterpreter interpreter) {
		this.interpreter = interpreter;
	}

	@Override
   public void onMessage(Message<MapReduceEvent> event) {
	  MapReduceEvent mrEvent = event.getMessageObject();
      if (mrEvent.getExecutionId().equals(interpreter.getExecutionId())
    		  && mrEvent.getStatus() == MapReduceStatus.SHUTDOWN_SENT)
         interpreter.shutdown();  
   }

}
