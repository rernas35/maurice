package com.maurice.analytics.mapreduce.interpreter;

import com.maurice.analytics.mapreduce.error.MauriceMRRuntimeException;
import com.maurice.analytics.mapreduce.sql.MapReduceDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.tools.nsc.interpreter.Results;

import java.util.Map;


public abstract class AbstractMapReduceInterpreter implements IMapReduceInterpreter {
   
   private final static Logger logger = LoggerFactory.getLogger(AbstractMapReduceInterpreter.class);
   
   @Override
   public void initialize(Map<String, String> coniguration) {
      initializeInternal(coniguration);
      startCommandListener();
   }
   
   @Override
   public void shutdown() {
      logger.info("shutdown is called! Shutdown process will be started.");
      stopCommandListener();
      shutdownInternal();
   }
   
   public Results.Result interpret(String codeLine) {
      try {
         Results.Result result = interpretInternal(codeLine);
         if (result.toString().toLowerCase().equals("error"))
            throw new MauriceMRRuntimeException("Error for " + codeLine );
         return result;
      } catch (MauriceMRRuntimeException e) {
         logger.error("Error while executing the line {} for execution {}", codeLine,getExecutionId());
         logError(codeLine);
         shutdown();
         return null;
      } catch (Exception e) {
         logError(codeLine);
         shutdown();
         throw e;
      } 
      
      
   }

   private void logError(String codeLine) {
      MapReduceDAO.log(getExecutionId(), "ERROR", "Error for line : " + codeLine);
      MapReduceDAO.logExecutionEnd(getExecutionId());
   }
   
   public abstract Results.Result interpretInternal(String codeLine);
   
   public abstract void initializeInternal(Map<String, String> coniguration);
   
   public abstract void shutdownInternal();
   
}
