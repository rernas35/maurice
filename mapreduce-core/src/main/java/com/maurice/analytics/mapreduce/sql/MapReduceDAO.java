
package com.maurice.analytics.mapreduce.sql;

import com.maurice.analytics.common.sql.ConnectionProvider;
import com.maurice.analytics.mapreduce.model.MapReduce;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.Arrays;
import java.util.Date;
import java.util.regex.Pattern;

import static com.maurice.analytics.mapreduce.config.MapReduceConfiguration.*;

public class MapReduceDAO {

   private static Logger logger = LoggerFactory.getLogger(MapReduceDAO.class);

   public static MapReduce getMapReduceById(Long mapReduceId) {

      try (Connection connection = ConnectionProvider.getConnection(getJDBCUrl(), getJDBCDriver(), getJDBCUser(), getJDBCPassword());) {
         PreparedStatement statement = connection.prepareStatement("select * from MAURICE_MGMT_MAP_REDUCE where map_reduce_id = ?");
         statement.setLong(1, mapReduceId);
         ResultSet rs = statement.executeQuery();
         MapReduce mr = new MapReduce();
         if (rs.next()) {
            mr.setId(rs.getLong("map_reduce_id"));
            mr.setName(rs.getString("name"));
            String code = rs.getString("code");
            String[] split = code.split(Pattern.quote("~"));
            mr.setCodes(Arrays.asList(split));
         }
         return mr;
      } catch (SQLException e) {
         logger.error("error while querying mapreduce from DB", e);
         System.exit(-1);
         return null;
      }

   }

   public static Long logExecutionStart(Long mapReduceId, Long userId) {
      try (Connection connection = ConnectionProvider.getConnection(getJDBCUrl(), getJDBCDriver(), getJDBCUser(), getJDBCPassword());) {
         PreparedStatement statement = connection.prepareStatement("insert into MAURICE_MGMT_MAP_REDUCE_EXECUTION"
                                                                                 + "(map_reduce_id,"
                                                                                 + "start_timestamp,"
                                                                                 + "initiator_id,"
                                                                                 + "current_state) "
                                                                                 + "values (?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
         statement.setLong(1, mapReduceId);
         statement.setTimestamp(2, new Timestamp(new Date().getTime()));
         statement.setLong(3, userId);
         statement.setString(4, "STARTED");
         statement.execute();
         ResultSet rs = statement.getGeneratedKeys();
         if (rs.next()){
            return rs.getLong(1);
         }else
            throw new SQLException("Error while retrieving generated keys");
      } catch (SQLException e) {
         logger.error("error while inserting record on MAURICE_MGMT_MAP_REDUCE_EXECUTION", e);
         return null;
      }

   }
   
   public static void logExecutionEnd(Long executionId){
      logExecution(executionId, "STOPPED");
   }
   
   public static void logExecution(Long executionId,String status){

      try (Connection connection = ConnectionProvider.getConnection(getJDBCUrl(), getJDBCDriver(), getJDBCUser(), getJDBCPassword());) {
         PreparedStatement statement = connection.prepareStatement("update MAURICE_MGMT_MAP_REDUCE_EXECUTION set end_timestamp=?,current_state = ? "
                                                                        + "where map_reduce_execution_id = ? ");
         statement.setTimestamp(1, new Timestamp(new Date().getTime()));
         statement.setString(2, status);
         statement.setLong(3, executionId);
         statement.execute();
      } catch (SQLException e) {
         logger.error("error while updating record on MGMT_MAP_REDUCE_EXECUTION", e);
      }
   }
   
   public static void log(Long executionId, String state, String description){
      try (Connection connection = ConnectionProvider.getConnection(getJDBCUrl(), getJDBCDriver(), getJDBCUser(), getJDBCPassword());) {
         PreparedStatement statement = connection.prepareStatement("insert into MAURICE_MGMT_MAP_REDUCE_EXECUTION_LOG"
                                                                                 + "(map_reduce_execution_id,"
                                                                                 + "state,"
                                                                                 + "description) "
                                                                                 + "values (?,?,?)");
         statement.setLong(1, executionId);
         statement.setString(2, state);
         statement.setString(3, description);
         statement.execute();
      } catch (SQLException e) {
         logger.error("error while inserting log for MAURICE_MGMT_MAP_REDUCE_EXECUTION_LOG table", e);
      }

   }
   
   
   public static Long getSequence(String sequenceName) {

      try (Connection connection = ConnectionProvider.getConnection(getJDBCUrl(), getJDBCDriver(), getJDBCUser(), getJDBCPassword());) {
         PreparedStatement statement = connection.prepareStatement("select nextval('" + sequenceName + "')");
         ResultSet rs = statement.executeQuery();
         MapReduce mr = new MapReduce();
         if (rs.next()) {
            long retValue = rs.getLong(1);
            logger.info("retreieved seq {} for {}" ,sequenceName , retValue );
            return retValue;
         }
         
         logger.info("no retreieved seq for {}!!" ,sequenceName  );
         return null;
      } catch (SQLException e) {
         logger.error("error while retrieving sequence", e);
         return null;
      }

   }


}
