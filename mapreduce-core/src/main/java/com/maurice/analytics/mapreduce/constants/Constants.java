package com.maurice.analytics.mapreduce.constants;


public class Constants {

   public final static String MASTER_URL = "master.url";
   public final static String OUTPUT_DIR = "output.dir";
   public final static String NAME = "mapreduce.name";
   public final static String ID = "mapreduce.id";
   public final static String USER_ID = "mapreduce.user_id";
   public final static String SECONDS = "verapi.streaming.period.seconds";
   
   public final static String EXECUTOR_URI = "spark.executor.uri";
   public final static String EXECUTOR_HOME = "spark.mesos.executor.home";
}
