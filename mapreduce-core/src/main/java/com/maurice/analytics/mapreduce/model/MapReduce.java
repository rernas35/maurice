package com.maurice.analytics.mapreduce.model;

import java.util.List;

public class MapReduce {

   Long id;
   Long userId;
   String name;
   List<String> codes;
   
   public Long getId() {
      return id;
   }
   
   public void setId(Long id) {
      this.id = id;
   }
   
   public Long getUserId() {
      return userId;
   }
   
   public void setUserId(Long userId) {
      this.userId = userId;
   }
   
   public String getName() {
      return name;
   }
   
   public void setName(String name) {
      this.name = name;
   }
   
   public List<String> getCodes() {
      return codes;
   }
   
   public void setCodes(List<String> codes) {
      this.codes = codes;
   }
   
   @Override
   public String toString() {
      return "id:" + id + " name:" + name + " codes:" + codes ;
   }
   
   
}
