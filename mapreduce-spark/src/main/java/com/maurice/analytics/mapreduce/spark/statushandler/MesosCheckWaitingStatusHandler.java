package com.maurice.analytics.mapreduce.spark.statushandler;

import com.jayway.jsonpath.Criteria;
import com.jayway.jsonpath.Filter;
import com.jayway.jsonpath.JsonPath;
import com.maurice.analytics.mapreduce.interpreter.IMapReduceInterpreter;
import net.minidev.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class MesosCheckWaitingStatusHandler extends AbstractCheckWaitingStatusHandler{
   
   private static final Logger logger = LoggerFactory.getLogger(MesosCheckWaitingStatusHandler.class);
   
   public MesosCheckWaitingStatusHandler(IMapReduceInterpreter interpreter, String submissionId,Long executionId) {
      super(interpreter, submissionId, executionId);
   }

   boolean checkWaiting(String url, String response) {
      try {
         response = callRestService(url);
      } catch (IOException e) {
        logger.error("Error while checking status for waiting",e);
      }
      
      Filter frameworkId = Filter.filter(Criteria.where("id").eq(getIdentifier()));

      JSONArray array = JsonPath.parse(response).read("$.frameworks[?(@.id=='"+ getIdentifier()+"')].tasks",frameworkId);
      logger.info(array.toJSONString());
      logger.info(String.valueOf(array.toJSONString().equals("[[]]")));
      
      return array.toJSONString().equals("[[]]");
   }
  
   
}
