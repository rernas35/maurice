
package com.maurice.analytics.mapreduce.spark.interpreter;

import com.maurice.analytics.cache.MauriceAnalyticsMessageBus;
import com.maurice.analytics.cache.model.MapReduceEvent;
import com.maurice.analytics.cache.model.MapReduceStatus;
import com.maurice.analytics.mapreduce.sql.MapReduceDAO;
import org.apache.spark.scheduler.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MauriceSparkListener implements SparkListenerInterface {
   
   private final static Logger logger = LoggerFactory.getLogger(MauriceSparkInterpreter.class);
   Long mapReduceId;
   Long executionId;
   Long userId;
   String name ;

   public MauriceSparkListener(String name, Long mapReduceId,Long userId) {
      this.name = name;
      this.mapReduceId = mapReduceId;
      this.userId = userId;
   }

   @Override
   public void onBlockUpdated(SparkListenerBlockUpdated arg0) {
      // TODO Auto-generated method stub

   }

   @Override
   public void onSpeculativeTaskSubmitted(SparkListenerSpeculativeTaskSubmitted speculativeTask) {

   }

   @Override
   public void onEnvironmentUpdate(SparkListenerEnvironmentUpdate arg0) {
      // TODO Auto-generated method stub

   }

   @Override
   public void onExecutorAdded(SparkListenerExecutorAdded arg0) {
      // TODO Auto-generated method stub

   }

   @Override
   public void onExecutorMetricsUpdate(SparkListenerExecutorMetricsUpdate arg0) {
      // TODO Auto-generated method stub

   }

   @Override
   public void onExecutorRemoved(SparkListenerExecutorRemoved arg0) {
      // TODO Auto-generated method stub

   }

   @Override
   public void onExecutorBlacklisted(SparkListenerExecutorBlacklisted executorBlacklisted) {

   }

   @Override
   public void onExecutorUnblacklisted(SparkListenerExecutorUnblacklisted executorUnblacklisted) {

   }

   @Override
   public void onNodeBlacklisted(SparkListenerNodeBlacklisted nodeBlacklisted) {

   }

   @Override
   public void onNodeUnblacklisted(SparkListenerNodeUnblacklisted nodeUnblacklisted) {

   }

   @Override
   public void onJobEnd(SparkListenerJobEnd event) {}

   @Override
   public void onJobStart(SparkListenerJobStart arg0) {}

   @Override
   public void onOtherEvent(SparkListenerEvent arg0) {
      // TODO Auto-generated method stub

   }

   @Override
   public void onStageCompleted(SparkListenerStageCompleted arg0) {
      // TODO Auto-generated method stub

   }

   @Override
   public void onStageSubmitted(SparkListenerStageSubmitted arg0) {
      // TODO Auto-generated method stub

   }

   @Override
   public void onTaskEnd(SparkListenerTaskEnd taskEvent) {

   }

   @Override
   public void onTaskGettingResult(SparkListenerTaskGettingResult arg0) {
      // TODO Auto-generated method stub

   }

   @Override
   public void onTaskStart(SparkListenerTaskStart event) {
   }

   @Override
   public void onUnpersistRDD(SparkListenerUnpersistRDD arg0) {
      // TODO Auto-generated method stub

   }

   @Override
   public void onApplicationStart(SparkListenerApplicationStart event) {
      MauriceAnalyticsMessageBus.publishMapReduceEventStatus(new MapReduceEvent(MapReduceStatus.RUNNING, name + " is started."));
      logger.info(name + " is started.");
      executionId = MapReduceDAO.logExecutionStart(mapReduceId, userId);
      MapReduceDAO.log(executionId, "STARTED", "");
   }

   @Override
   public void onBlockManagerAdded(SparkListenerBlockManagerAdded arg0) {
      // TODO Auto-generated method stub

   }

   @Override
   public void onBlockManagerRemoved(SparkListenerBlockManagerRemoved arg0) {
      // TODO Auto-generated method stub

   }

   @Override
   public void onApplicationEnd(SparkListenerApplicationEnd event) {
      MauriceAnalyticsMessageBus.publishMapReduceEventStatus(new MapReduceEvent(MapReduceStatus.COMPLETED, name + " is completed."));
      logger.info(name + " is completed.");
      MapReduceDAO.logExecutionEnd(executionId);
      MapReduceDAO.log(executionId, "STOPPED", "");
   }
   
   
   public Long getExecutionId() {
      return executionId;
   }

}
