package com.maurice.analytics.mapreduce.spark;

import com.maurice.analytics.mapreduce.constants.Constants;
import com.maurice.analytics.mapreduce.interpreter.IMapReduceInterpreter;
import com.maurice.analytics.mapreduce.model.MapReduce;
import com.maurice.analytics.mapreduce.spark.interpreter.MauriceSparkInterpreter;
import com.maurice.analytics.mapreduce.sql.MapReduceDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;


public class MauriceMapReduce {
   
   private static Logger logger = LoggerFactory.getLogger(MauriceMapReduce.class);
   
   public static void main(String[] args) {
      IMapReduceInterpreter interpreter = new MauriceSparkInterpreter();
      
      String idStr = args[2];
      String userIDStr = args[3];
      long id = Long.parseLong(idStr);
      
      logger.info("mapreduce id : " + id);
      logger.info("user id : " + userIDStr);
      
      MapReduce mapReduce = MapReduceDAO.getMapReduceById(id);
      logger.info("mapreduce : " + mapReduce);
      
      String classOutputDirStr = args[1];
      if (!classOutputDirStr.endsWith("/")) classOutputDirStr += "/";
      classOutputDirStr += mapReduce.getName();
      File classOutputDir = new File(classOutputDirStr);
      if (!(classOutputDir.exists())){
         logger.info("creating folder {} for class outputs.", classOutputDirStr);
         boolean folderCreated = classOutputDir.mkdir();
         if (!folderCreated){
            logger.info("Folder can't be created.");
            System.exit(1);
         }
            
      }
      
      Map<String, String> configuration = new HashMap<String, String>();
      configuration.put(Constants.MASTER_URL, args[0]);
      configuration.put(Constants.OUTPUT_DIR, classOutputDirStr);
      configuration.put(Constants.NAME,mapReduce.getName());
      configuration.put(Constants.ID,String.valueOf(mapReduce.getId()) );
      configuration.put(Constants.USER_ID , userIDStr);
      
      for(int i=4; i < args.length ; i++){ 
         String param = args[i];
         String[] keyValueArray = param.split(Pattern.quote("="));
         if (keyValueArray.length == 2){
            configuration.put(keyValueArray[0], keyValueArray[1]);
         }
        
      }
      
      interpreter.initialize(configuration);
      
      logger.info("Executing the code" );
      mapReduce.getCodes().forEach( x -> interpreter.interpret(x));
      logger.info("Executed the code" );
      
   }

}
