package com.maurice.analytics.mapreduce.spark.statushandler;

import com.jayway.jsonpath.Criteria;
import com.jayway.jsonpath.Filter;
import com.jayway.jsonpath.JsonPath;
import com.maurice.analytics.mapreduce.interpreter.IMapReduceInterpreter;
import net.minidev.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class SparkCheckWaitingStatusHandler extends AbstractCheckWaitingStatusHandler{
   
   private static final Logger logger = LoggerFactory.getLogger(SparkCheckWaitingStatusHandler.class);
   
   public SparkCheckWaitingStatusHandler(IMapReduceInterpreter interpreter, String applicationId,Long executionId) {
      super(interpreter, applicationId, executionId);
   }

   boolean checkWaiting(String url, String response) {
      try {
         response = callRestService(url);
      } catch (IOException e) {
        logger.error("Error while checking status for waiting",e);
      }

      Filter applicationIdFilter = Filter.filter(Criteria.where("id").eq(getIdentifier()));
      
      JSONArray array = JsonPath.parse(response).read("$.activeapps[*].state", applicationIdFilter);
      String state = array.get(0).toString();
      return state.toLowerCase().equals("waiting");
   }
   
}
