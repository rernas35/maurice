package com.maurice.analytics.mapreduce.spark.statushandler;

import com.maurice.analytics.mapreduce.config.MapReduceConfiguration;
import com.maurice.analytics.mapreduce.interpreter.IMapReduceInterpreter;
import com.maurice.analytics.mapreduce.sql.MapReduceDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.TimerTask;

public abstract class AbstractCheckWaitingStatusHandler extends TimerTask{
   
   private final String USER_AGENT = "Mozilla/5.0";
   private static final Logger logger = LoggerFactory.getLogger(AbstractCheckWaitingStatusHandler.class);

   private String identifier;
   private Long executionId;
   private IMapReduceInterpreter interpreter;
   
   private static final String WAITING_STATUS = "WAITING";
   
   public AbstractCheckWaitingStatusHandler(IMapReduceInterpreter interpreter, String identifier,Long executionId) {
      logger.info("Check Waiting Status Handler identifier : {}", identifier);
      this.identifier = identifier;
      this.executionId = executionId;
      this.interpreter = interpreter;
   }
  
   
   public void check(){
      String url = MapReduceConfiguration.getMRRestUrl();
      
      if (url == null) {
         logger.warn("Skipping WAITING state check since mr.rest.url is not set in mapreduce.properties file");
         return;
      }

      String response= null   ;
      
      if (checkWaiting(url, response)){
         logger.info("State is in WAITING mode");
         MapReduceDAO.log(executionId, WAITING_STATUS, "Application is set to " + WAITING_STATUS + " state.");
         MapReduceDAO.logExecution(executionId, WAITING_STATUS);
         interpreter.setWaiting();
      }
      
      
      
   }
   
   String callRestService(String url) throws MalformedURLException, IOException, ProtocolException {
      URL obj = new URL(url);
      HttpURLConnection con = (HttpURLConnection) obj.openConnection();

      // optional default is GET
      con.setRequestMethod("GET");

      //add request header
      con.setRequestProperty("User-Agent", "");

      int responseCode = con.getResponseCode();
      logger.info("Response Code : {}",responseCode);

      BufferedReader in = new BufferedReader(
              new InputStreamReader(con.getInputStream()));
      String inputLine;
      StringBuffer response = new StringBuffer();

      while ((inputLine = in.readLine()) != null) {
         response.append(inputLine);
      }
      in.close();
      return response.toString();
   }


   abstract boolean checkWaiting(String url, String response);
     
   
   public String getIdentifier() {
      return identifier;
   }

   @Override
   public void run() {
      check();
      
   }
   
   

}
