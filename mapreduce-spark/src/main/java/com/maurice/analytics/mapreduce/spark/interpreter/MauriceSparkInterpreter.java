
package com.maurice.analytics.mapreduce.spark.interpreter;

import com.hazelcast.core.ITopic;
import com.maurice.analytics.cache.MauriceAnalyticsMessageBus;
import com.maurice.analytics.cache.model.MapReduceEvent;
import com.maurice.analytics.mapreduce.config.MapReduceConfiguration;
import com.maurice.analytics.mapreduce.constants.Constants;
import com.maurice.analytics.mapreduce.error.MauriceMRRuntimeException;
import com.maurice.analytics.mapreduce.interpreter.AbstractMapReduceInterpreter;
import com.maurice.analytics.mapreduce.listener.MauriceExecutorCommandListener;
import com.maurice.analytics.mapreduce.result.ResultWriter;
import com.maurice.analytics.mapreduce.spark.statushandler.MesosCheckWaitingStatusHandler;
import com.maurice.analytics.mapreduce.spark.util.Utils;
import com.maurice.analytics.mapreduce.sql.MapReduceDAO;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.repl.SparkILoop;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.Console;
import scala.collection.JavaConversions;
import scala.tools.nsc.Settings;
import scala.tools.nsc.interpreter.IMain;
import scala.tools.nsc.interpreter.Results;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Timer;
import java.util.regex.Pattern;

public class MauriceSparkInterpreter extends AbstractMapReduceInterpreter {

   private final static Logger logger = LoggerFactory.getLogger(MauriceSparkInterpreter.class);

   String name;
   Long mapReduceId;
   Long userId;
   SparkSession session;
   SparkConf conf;
   SparkContext context;
   SparkILoop sparkLoop;
   Object intp;
   Long seconds;
   boolean waitingStatus;
   
   Boolean sscDefined = false;

   private String streamingVariable = "ssc";
   private MauriceExecutorCommandListener commandListener;
   private String commandListenerRegistrationId;
   private ITopic<MapReduceEvent> commandTopic;
   private MauriceSparkListener listener;

   public void initializeInternal(Map<String, String> configuration) {
      name = configuration.get(Constants.NAME);
      mapReduceId = Long.parseLong(configuration.get(Constants.ID));
      userId = Long.parseLong(configuration.get(Constants.USER_ID));

      logger.info("initializing application with a name of {}", name);
      String outputDir = configuration.get(Constants.OUTPUT_DIR);
      sparkLoop = new SparkILoop((java.io.BufferedReader) null, new ResultWriter(Console.out(), false));
      Settings settings = new Settings();

      LinkedList<String> argList = new LinkedList<>();
      argList.add("-usejavacp");
      argList.add("-Yrepl-class-based");
      argList.add("-Yrepl-outdir");
      argList.add(outputDir);
      argList.add("-classpath");

      String classpath = System.getProperty("java.class.path");
      argList.add(classpath);

      scala.collection.immutable.List<String> list = JavaConversions.asScalaBuffer(argList).toList();

      settings.processArguments(list, true);
      sparkLoop.settings_$eq(settings);
      sparkLoop.createInterpreter();
      intp = Utils.invokeMethod(sparkLoop, "intp");
      Utils.invokeMethod(intp, "setContextClassLoader");
      Utils.invokeMethod(intp, "initializeSynchronous");

      conf = new SparkConf().setMaster(configuration.get(Constants.MASTER_URL));
      conf.set("spark.repl.class.outputDir", outputDir);
      conf.set("spark.scheduler.mode", "FAIR");
      conf.setAppName(name);

      assignSparkParameters(configuration);
      assignMauriceParameters(configuration);

      Class SparkSession = Utils.findClass("org.apache.spark.sql.SparkSession");
      Object builder = Utils.invokeStaticMethod(SparkSession, "builder");
      Utils.invokeMethod(builder, "config", new Class[] {SparkConf.class}, new Object[] {conf});

      session = (SparkSession) Utils.invokeMethod(builder, "getOrCreate");

      context = (SparkContext) Utils.invokeMethod(session, "sparkContext");

      addSparkListener(name);

      importCommonSparkPackages();

      bindSparkComponents();
      
      scheduleWaitingCheck(context);
   }

   private void scheduleWaitingCheck(SparkContext context) {
      Timer timer = new Timer();
      timer.schedule(new MesosCheckWaitingStatusHandler(this,context.applicationId(),listener.getExecutionId()), MapReduceConfiguration.getWaitingCheckPeriod() * 1000);
   }

   private void assignSparkParameters(Map<String, String> configuration) {
      configuration.keySet().stream()
         .filter(x -> x.startsWith("spark"))
         .forEach(key -> {
            conf.set(key, configuration.get(key));
         });
   }

   private void assignMauriceParameters(Map<String, String> configuration) {
      configuration.keySet().stream()
         .filter(x -> x.startsWith("verapi"))
         .forEach(key -> {
            if (key.equals(Constants.SECONDS))
               try {
                  seconds = Long.parseLong(configuration.get(Constants.SECONDS));
               } catch (NumberFormatException e) {
                  logger.error("Error while initializing streaming context since period is defined inproperly.", e);
                  System.exit(1);
               }
         });
   }

   @Override
   public void startCommandListener() {
      commandListener = new MauriceExecutorCommandListener(this);
      commandTopic = MauriceAnalyticsMessageBus.getMapReduceExecutorCommandTopic();
      commandListenerRegistrationId = commandTopic.addMessageListener(commandListener);
   }

   public void shutdownInternal() {
      try {
         logger.info("Spark application shutdown process is started.");
         if (!waitingStatus && sscDefined) 
            interpret(streamingVariable + ".stop(true)");
         else if (waitingStatus) {
            MapReduceDAO.logExecutionEnd(listener.getExecutionId());
            MapReduceDAO.log(listener.getExecutionId(), "STOPPED", "");
         }
            
         logger.info("waiting for shutdown process of the application");
         Thread.currentThread().sleep(10000);
      } catch (InterruptedException e) {
         logger.error("Error while shutting down MR", e);
      }
      logger.info("JVM will be shutdown");
      System.exit(0);

   }

   @Override
   public void stopCommandListener() {
      logger.info("removing message listener");
      commandTopic.removeMessageListener(commandListenerRegistrationId);
      logger.info("message listener removed.");
   }

   private void addSparkListener(String name) {
      listener = new MauriceSparkListener(name, mapReduceId, userId);
      listener.onApplicationStart(null);
      context.addSparkListener(listener);
   }

   private void bindSparkComponents() {
      interpret("@transient val _binder = new java.util.HashMap[String, Object]()");
      Map<String, Object> binder = (Map<String, Object>) getLastObject();
      binder.put("sc", context);
      binder.put("conf", conf);

      interpret("@transient val sc = "
         + "_binder.get(\"sc\").asInstanceOf[org.apache.spark.SparkContext]");
      interpret("@transient val conf = "
         + "_binder.get(\"conf\").asInstanceOf[org.apache.spark.SparkConf]");

      if (seconds != null) {
         interpret("var ssc = new StreamingContext(sc,Seconds(" + seconds + ")) ");
         sscDefined = true;
      }

   }

   private void importCommonSparkPackages() {
      Results.Result res = interpret("import org.apache.spark._");
      res = interpret("import org.apache.spark.streaming._");
      res = interpret("import org.apache.spark.streaming.StreamingContext._ ");
      res = interpret("import org.apache.spark._");
   }

   public Object getLastObject() {
      IMain.Request r = (IMain.Request) Utils.invokeMethod(intp, "lastRequest");
      if (r == null || r.lineRep() == null) {
         return null;
      }
      Object obj = r.lineRep().call("$result",
         JavaConversions.asScalaBuffer(new LinkedList<>()));
      return obj;
   }

   public Results.Result interpretInternal(String line) {
      Results.Result res = (Results.Result) Utils.invokeMethod(intp, "interpret", new Class[] {String.class},
         new Object[] {line});
      return res;
   }

   public void execute(String... lines) {
      for (String line : lines) {
         interpret(line);
      }
   }

   public static void main(String[] args) throws MauriceMRRuntimeException {
      Map<String, String> configuration = new HashMap<String, String>();
      configuration.put(Constants.MASTER_URL, args[0]);
      configuration.put(Constants.OUTPUT_DIR, args[1]);
      configuration.put(Constants.NAME, "Streaming_Engine");
      configuration.put(Constants.ID, "1");
      configuration.put(Constants.USER_ID, "1232");

      for (int i = 2; i < args.length; i++) {
         String param = args[i];
         String[] keyValueArray = param.split(Pattern.quote("="));
         if (keyValueArray.length == 2) {
            configuration.put(keyValueArray[0], keyValueArray[1]);
         }

      }

      MauriceSparkInterpreter v = new MauriceSparkInterpreter();
      v.seconds = 1000L;
      v.initialize(configuration);
      //		VerapiInterpreter v = new VerapiInterpreter("local");
      v.execute(
         "import org.apache.spark.streaming.Seconds",
         "val dstream = ssc.socketTextStream(\"localhost\", "+v.seconds+")",

         //         "val emptyRDD = sc.parallelize(Seq(\"bisi\"))",
         //         "emptyRDD.foreach(x => println(x))");

         "dstream.foreachRDD(rdd => rdd.foreach(x => println(x)))",
         "ssc.start()",
         "ssc.awaitTermination()");
   }

   @Override
   public Long getExecutionId() {
      return listener.getExecutionId();
   }

   @Override
   public void setWaiting() {
      waitingStatus = true;
   }
   
}
