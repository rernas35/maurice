
package com.maurice.analytics.management.ws;

import com.maurice.analytics.cache.model.EventDefinition;
import com.maurice.analytics.cache.model.TopicRoute;
import com.maurice.analytics.cache.model.User;
import com.maurice.analytics.management.ws.impl.ManagementServiceImpl;
import com.maurice.analytics.management.ws.model.Response;

import java.util.List;

public interface IManagementService {
   
   Response addEventDefinition(EventDefinition api);

   Response updateEventDefinition(EventDefinition api);

   Response deleteEventDefinition(String definitionName);

   Response addUser(User user);

   Response deleteUser(String name);

   Response addTopicToEventDefinition(String definitionName, TopicRoute topicRoute);

   Response deleteTopicFromEventDefinition(String definitionName, String topicRouteName);
   
   Response<List<EventDefinition>> listEventDefinition();

   ManagementServiceImpl.EventDefinitionResponse getEventDefinition(String definitionName);

   Response<List<User>> listUser();
   
   Response<List<TopicRoute>> listTopicRoutesOfAPI(String definitionName);
   
   Response<List<EventDefinition>> listEventDefinitionbyUser(String name);
   
}
