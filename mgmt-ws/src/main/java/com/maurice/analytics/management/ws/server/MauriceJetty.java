
package com.maurice.analytics.management.ws.server;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import java.io.IOException;

public class MauriceJetty {

   private static final Logger LOGGER = LoggerFactory.getLogger(MauriceJetty.class);

   private static final int PORT = 9290;

   private static final String RESOURCE_PATH = "spring/";
   private static final String CONTEXT_PATH = "/analytics";
   private static final String CXF_MAPPING_URL = "/api/*";

   public static void main(String[] args) throws Exception {
      new MauriceJetty().startJetty(PORT);
   }

   private void startJetty(int port) throws Exception {
      LOGGER.debug("Starting server at port {}", port);
      Server server = new Server(port);

      server.setHandler(getServletContextHandler());

      addRuntimeShutdownHook(server);

      server.start();
      LOGGER.info("Server started at port {}", port);
      server.join();
   }

   private static ServletContextHandler getServletContextHandler() throws IOException {
      ServletContextHandler contextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS); // SESSIONS requerido para JSP 
      contextHandler.setErrorHandler(null);

      contextHandler.setResourceBase(new ClassPathResource(RESOURCE_PATH).getURI().toString());
      contextHandler.setContextPath(CONTEXT_PATH);

      // Spring
      WebApplicationContext webAppContext = getWebApplicationContext();
      CXFServlet cxfServlet = new CXFServlet();
      ServletHolder springServletHolder = new ServletHolder("CXFServlet", cxfServlet);
      contextHandler.addServlet(springServletHolder, CXF_MAPPING_URL);
      contextHandler.addEventListener(new ContextLoaderListener(webAppContext));

      return contextHandler;
   }

   private static WebApplicationContext getWebApplicationContext() {
      //        XmlWebApplicationContext context = new XmlWebApplicationContext();
      //        context.setConfigLocation(CONFIG_LOCATION_PACKAGE);
      AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
      context.setConfigLocation("com.maurice.analytics.management.ws.context");
      return context;
   }

   private static void addRuntimeShutdownHook(final Server server) {
      Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

         @Override
         public void run() {
            if (server.isStarted()) {
               server.setStopAtShutdown(true);
               try {
                  server.stop();
               } catch (Exception e) {
                  System.out.println("Error while stopping jetty server: " + e.getMessage());
                  LOGGER.error("Error while stopping jetty server: " + e.getMessage(), e);
               }
            }
         }
      }));
   }

}
