
package com.maurice.analytics.management.ws;

import com.maurice.analytics.management.model.MapReduce;
import com.maurice.analytics.management.model.MapReduceEntity;
import com.maurice.analytics.management.ws.model.Response;
import com.maurice.analytics.mapreduce.model.MapReduceJob;

import java.util.List;

public interface IMapReduceManagementService {
   
   Response killMapReduce(Long mrId);
   
   Response<MapReduceJob> getStatusMapReduce(Long mrId);
   
   //24.07.2017 --ws interface impl.
   Response<List<MapReduceEntity>> findByUserId(Long userId);
   
   Response create(MapReduce entity);
   
   Response update(MapReduce entity);
   
   Response delete(Long mrId);
}
