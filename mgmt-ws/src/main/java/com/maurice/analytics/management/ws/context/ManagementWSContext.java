package com.maurice.analytics.management.ws.context;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource({"management-context.xml","management-ws-context.xml"})
public class ManagementWSContext {


}
