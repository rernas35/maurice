package com.maurice.analytics.management.ws.impl;

import com.maurice.analytics.cache.MauriceAnalyticsMessageBus;
import com.maurice.analytics.cache.model.MapReduceEvent;
import com.maurice.analytics.cache.model.MapReduceStatus;
import com.maurice.analytics.management.IMRAdminService;
import com.maurice.analytics.management.dao.MapReduceEntityDAO;
import com.maurice.analytics.management.model.MapReduce;
import com.maurice.analytics.management.model.MapReduceEntity;
import com.maurice.analytics.management.service.MapReduceService;
import com.maurice.analytics.management.service.UserService;
import com.maurice.analytics.management.ws.IMapReduceManagementService;
import com.maurice.analytics.management.ws.model.Response;
import com.maurice.analytics.mapreduce.model.MapReduceJob;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import java.util.List;

@Service
@Path("/mapreduce")
@Api(value="/mapreduce", description="Management API is for mapreduce")
public class MapReduceManagementServiceImpl implements IMapReduceManagementService {

    private static Logger LOGGER = LoggerFactory.getLogger(MapReduceManagementServiceImpl.class);

   @Autowired
   IMRAdminService mrAdminService;
   
   @Autowired
   MapReduceService mapReduceService;
   
   @Autowired
   UserService userService;

   @GET
   @Path("/task/kill/{mrId}")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=Response.class,value="Kill Map reduce job")
   public Response killMapReduce(@PathParam("mrId") Long mrId) {
      MapReduceEvent event = new MapReduceEvent(MapReduceStatus.SHUTDOWN_SENT, "Shutdown is sent via web services.");
      MapReduceEntity mr = mrAdminService.findById(mrId);
      MauriceAnalyticsMessageBus.publishMapReduceExecutorCommand(event);
      return new Response();
   }

   @GET
   @Path("/task/status/{mrId}")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=Response.class,value="Get status of map reduce job")
   public Response<MapReduceJob> getStatusMapReduce(@PathParam("mrId") Long mrID) {
      // TODO Auto-generated method stub
      return null;
   }

   //24.07.2017 -ws implementation.
   @GET
   @Path("/task/{userId}")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=Response.class,value="Get entity list of given user id")
	public Response<List<MapReduceEntity>> findByUserId(@PathParam("userId") Long userId) {
	   Response<List<MapReduceEntity>> response = new Response<List<MapReduceEntity>>();
	   MapReduceEntityDAO dao = new MapReduceEntityDAO();
	   response.setT(dao.findByUserId(userId));
	   return response;
	}
   
   //24.07.2017 -ws implementation.
   @POST
   @Path("/task/")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=Response.class,value="Create entity for given user id")
	public Response create(MapReduce mapReduce) {
        LOGGER.info("Create mapreduce with a name of {}",mapReduce.getName());
	   mrAdminService.createOrUpdate(mapReduce);
       LOGGER.info("{} created",mapReduce.getName());
	   return new Response();
	   
	}

   @SuppressWarnings("rawtypes")
   @PUT
   @Path("/task/")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=Response.class,value="update user entity by map reduce id")
   public Response update(MapReduce mapReduce) {
	   mrAdminService.createOrUpdate(mapReduce);
	   return new Response();
   }

   @SuppressWarnings("rawtypes")
   @DELETE
   @Path("/task/{mrIdToBeDeleted}")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=Response.class,value="delete user entity by map reduce id")
   public Response delete(@PathParam("mrIdToBeDeleted") Long mrIdToBeDeleted) {
        mrAdminService.delete(mrIdToBeDeleted);
	   return new Response();
   }
   

}
