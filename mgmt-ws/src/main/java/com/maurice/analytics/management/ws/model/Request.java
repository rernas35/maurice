package com.maurice.analytics.management.ws.model;


public class Request<T> {
   
   T t;

   
   public T getT() {
      return t;
   }

   
   public void setT(T t) {
      this.t = t;
   }
   
   

}
