package com.maurice.analytics.management.ws.model;


public class Response<T> {
   
   private Integer statusCode = 0;
   private Integer errorCode = 0;
   private String description = "OK";
   
   T t;
   
   public Integer getStatusCode() {
      return statusCode;
   }

   public void setStatusCode(Integer statusCode) {
      this.statusCode = statusCode;
   }

   
   public Integer getErrorCode() {
      return errorCode;
   }

   
   public void setErrorCode(Integer errorCode) {
      this.errorCode = errorCode;
   }

   
   public String getDescription() {
      return description;
   }

   
   public void setDescription(String description) {
      this.description = description;
   }

   
   public T getT() {
      return t;
   }

   
   public void setT(T t) {
      this.t = t;
   }
   
   
    

}
