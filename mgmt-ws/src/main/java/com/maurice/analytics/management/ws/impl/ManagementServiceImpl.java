package com.maurice.analytics.management.ws.impl;

import com.maurice.analytics.cache.model.EventDefinition;
import com.maurice.analytics.cache.model.TopicRoute;
import com.maurice.analytics.cache.model.User;
import com.maurice.analytics.management.IAdminService;
import com.maurice.analytics.management.ws.IManagementService;
import com.maurice.analytics.management.ws.model.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import java.util.List;


@CrossOriginResourceSharing(
        allowOrigins = {
                "http://localhost"
        }
)
@Service
@Path("/management")
@Api(value="/management", description="Management API is for adding, deleting and doing some association stuff.")
public class ManagementServiceImpl implements IManagementService {

   @Autowired
   private IAdminService adminService;


   @POST
   @Path("/event")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=Response.class,value="Add Event Definition.")
   public Response addEventDefinition(EventDefinition eventDefinition) {
      adminService.addEventDefinition(eventDefinition);
      return new Response();
   }

   @PUT
   @Path("/event")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=Response.class,value="Update Event Definition.")
   public Response updateEventDefinition(EventDefinition eventDefinition) {
      adminService.updateEventDefinition(eventDefinition);
      return new Response();
   }

   @DELETE
   @Path("/event/{definitionName}")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=Response.class,value="Delete Event definition.")
   public Response deleteEventDefinition(@PathParam("definitionName") String definitionName) {
      adminService.deleteEventDefinition(definitionName);
      return new Response();

   }

   @POST
   @Path("/user")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=Response.class,value="Add User.")
   public Response addUser(User user) {
      adminService.addUser(user);
      return new Response();
   }

   @DELETE
   @Path("/user/{username}")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=Response.class,value="delete User.")
   public Response deleteUser(@PathParam("username") String name) {
      adminService.deleteUser(name);
      return new Response();
   }

   @POST
   @Path("/event/{definitionName}/topic")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=Response.class,value="Add Topic to Event Definition.")
   public Response addTopicToEventDefinition(@PathParam("definitionName") String definitionName, TopicRoute topicRoute) {
      adminService.addTopicToEventDefinition(definitionName, topicRoute);
      return new Response();
   }

   @DELETE
   @Path("/event/{definitionName}/topic/{topicRouteName}")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=Response.class,value="Delete Topic from Event Definition.")
   public Response deleteTopicFromEventDefinition(@PathParam("definitionName") String definitionName,@PathParam("topicRouteName") String topicRoute) {
      adminService.deleteTopicFromEventDefinition(definitionName, topicRoute);
      return new Response();
   }

   @GET
   @Path("/event")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=APIListResponse.class,value="List all Event Definitions")
   public APIListResponse listEventDefinition() {
      APIListResponse response = new APIListResponse();
      response.setT(adminService.listEventDefinition());
      return response;
   }

   @GET
   @Path("/event/{definitionName}")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=APIListResponse.class,value="List all Event Definitions")
   public EventDefinitionResponse getEventDefinition(@PathParam("definitionName") String definitionName) {
      EventDefinitionResponse response = new EventDefinitionResponse();
      response.setT(adminService.getEventDefinition(definitionName));
      return response;
   }

   @GET
   @Path("/user")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=UserListResponse.class,value="List Users")
   public UserListResponse listUser() {
      UserListResponse response = new UserListResponse();
      response.setT(adminService.listUser());
      return response;
   }


   @GET
   @Path("/event/{definitionName}/topic")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=TopicRouteListResponse.class,value="List Topic routes of Event Definitions")
   public TopicRouteListResponse listTopicRoutesOfAPI(@PathParam("definitionName") String definitionName) {
      TopicRouteListResponse response = new TopicRouteListResponse();
      response.setT(adminService.listTopicRoutesOfEventDefinition(definitionName));
      return response;
   }

   @GET
   @Path("/user/{username}/event")
   @Produces({"application/json"})
   @Consumes({"application/json"})
   @Override
   @ApiOperation(response=APIListResponse.class,value="List Event Definitions of given user")
   public APIListResponse listEventDefinitionbyUser(@PathParam("username") String username) {
      APIListResponse response = new APIListResponse();
      response.setT(adminService.listEventDefinitionByUser(username));
      return response;
   }

   
   public static class APIListResponse extends Response<List<EventDefinition>> {};

   public static class EventDefinitionResponse extends Response<EventDefinition> {};
   
   public static class TopicRouteListResponse extends Response<List<TopicRoute>> {};
   
   public static class UserListResponse extends Response<List<User>> {};
   


}
