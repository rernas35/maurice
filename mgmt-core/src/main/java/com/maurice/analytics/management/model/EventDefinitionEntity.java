
package com.maurice.analytics.management.model;

import com.maurice.analytics.cache.model.EventDefinition;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MAURICE_MGMT_EVENT_DEFINITION")
public class EventDefinitionEntity implements ICachableEntity<String> {

   private Long definitionId;
   private String externalId;
   @CacheKey
   private String name;
   private String description;
   private UserEntity owner;
   private String format;

   private Set<TopicRouteEntity> topicRoutes = new HashSet<TopicRouteEntity>();

   @Id
   @Column(name = "DEFINITION_ID")
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   public Long getDefinitionId() {
      return definitionId;
   }

   public void setDefinitionId(Long definitionId) {
      this.definitionId = definitionId;
   }

   @Column(name = "NAME", nullable = false, unique = true)
   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   @Column(name = "EXTERNAL_ID", nullable = false)
   public String getExternalId() {
      return externalId;
   }

   public void setExternalId(String externalId) {
      this.externalId = externalId;
   }

   @Column(name = "DESCRIPTION", nullable = true)
   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   @ManyToOne(fetch = FetchType.EAGER)
   @JoinColumn(name = "OWNER_ID", nullable = true)
   public UserEntity getOwner() {
      return owner;
   }

   public void setOwner(UserEntity owner) {
      this.owner = owner;
   }

   @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "eventDefinition", orphanRemoval = true, targetEntity = TopicRouteEntity.class)
   public Set<TopicRouteEntity> getTopicRoutes() {
      return topicRoutes;
   }

   public void setTopicRoutes(Set<TopicRouteEntity> topicRoutes) {
      this.topicRoutes = topicRoutes;
   }

   @Column(name = "FORMAT", nullable = true)
   public String getFormat() {
      return format;
   }

   public void setFormat(String format) {
      this.format = format;
   }

   @Override
   public boolean equals(Object obj) {
      if (!(obj instanceof EventDefinitionEntity))
         return false;
      return definitionId.equals(((EventDefinitionEntity) obj).definitionId);
   }

   @Override
   @Transient
   public String getKey() {
      return getName();
   }

   public EventDefinition toEventDefinition() {
      EventDefinition eventDefinition = new EventDefinition();
      eventDefinition.setId(getDefinitionId());
      eventDefinition.setName(getName());
      eventDefinition.setExternalId(getExternalId());
      if (getOwner() != null)
         eventDefinition.setOwnerName(getOwner().getName());
      eventDefinition.setFormat(getFormat());

      for (TopicRouteEntity topicRouteEntity : getTopicRoutes()) {
         eventDefinition.getTopicRouteList().add(topicRouteEntity.toTopicRoute());

      }

      return eventDefinition;

   }

}
