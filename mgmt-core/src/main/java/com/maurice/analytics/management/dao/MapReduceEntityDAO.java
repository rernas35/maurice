package com.maurice.analytics.management.dao;

import com.maurice.analytics.management.model.MapReduceEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class MapReduceEntityDAO extends GenericDAO<MapReduceEntity, Long>{
	
	

   public List<MapReduceEntity> findByUserId(Long userId){
	   
	   List<MapReduceEntity> entityList = new ArrayList<MapReduceEntity>();
	   
	   //error line:
	   Session session = getSessionFactory().getCurrentSession();
	   
	   Query query = session.createQuery("from MapReduceEntity where owner.userId = :userId");
	   query.setParameter("userId", userId);
	   
	   entityList = query.list();
	   
	   return entityList;
   }
   

   public void delete(Long mrId) {
	   
	   Session session = getSessionFactory().getCurrentSession();
	   
	   Query query = session.createQuery("delete from MapReduceEntity where mapreduceId = :mapreduceId");
	   query.setParameter("mapreduceId", mrId);
	   query.executeUpdate();
   }
   

   public void update(MapReduceEntity entity) {
	   
	   Session session = getSessionFactory().getCurrentSession();
	   
	   Query query = session.createQuery("update MapReduceEntity set name = :name, "
	   		+ "code = :code, period = :period where mapreduceId = :mapreduceId");
	   query.setParameter("name", entity.getName());
	   query.setParameter("code", entity.getCode());
	   query.setParameter("period", entity.getPeriod());
	   query.setParameter("mapreduceId", entity.getMapreduceId());
	   
	   query.executeUpdate();
	   
   }
}
