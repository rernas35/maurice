package com.maurice.analytics.management.service;

import com.maurice.analytics.management.dao.GenericDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Service
public abstract class GenericService<  T ,
                                       ID extends Serializable,
                                       DAO extends GenericDAO<T,ID>> {

   private static Logger LOGGER = LoggerFactory.getLogger(GenericService.class);

   protected abstract DAO getDAO();
   
   @Transactional(propagation=Propagation.REQUIRED)
   public void persist(T t){
      getDAO().persist(t);
   }
   
   @Transactional(propagation=Propagation.REQUIRED)
   public T merge(T t){
      return (T) getDAO().merge(t);
   }
   
   @Transactional(propagation=Propagation.REQUIRED)
   public void delete(T t){
      getDAO().delete(t);
   }

   @Transactional(propagation=Propagation.REQUIRED)
   public void deleteById(ID id){
      T t = getById(id);
      getDAO().delete(t);
   }
   
   @Transactional(propagation=Propagation.REQUIRED,readOnly=true)
   public T getById(ID id){
      return getDAO().getById(id);
   }
   
   @Transactional(propagation=Propagation.REQUIRED,readOnly=true)
   public List<T> list(){
      return getDAO().list();
   }

   
}
