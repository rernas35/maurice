package com.maurice.analytics.management.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="MAURICE_MGMT_MAP_REDUCE_EXECUTION")
public class MapReduceExecutionEntity {

   Long executionId;
   MapReduceEntity mapReduce;
   
   Date startTimestamp;
   Date endTimestamp;

   String currentState;
   UserEntity initiator;
   
   List<MapReduceExecutionLogEntity> logs;

   @Id
   @Column(name = "MAP_REDUCE_EXECUTION_ID")
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   public Long getExecutionId() {
      return executionId;
   }

   
   public void setExecutionId(Long executionId) {
      this.executionId = executionId;
   }

   @ManyToOne(fetch = FetchType.EAGER,targetEntity = MapReduceEntity.class)
   @JoinColumn(name = "MAP_REDUCE_ID", nullable = true)
   public MapReduceEntity getMapReduce() {
      return mapReduce;
   }

   
   public void setMapReduce(MapReduceEntity mapReduce) {
      this.mapReduce = mapReduce;
   }

   @Column(name = "START_TIMESTAMP", nullable = false)
   public Date getStartTimestamp() {
      return startTimestamp;
   }

   
   public void setStartTimestamp(Date startTimestamp) {
      this.startTimestamp = startTimestamp;
   }

   @Column(name = "END_TIMESTAMP", nullable = true)
   public Date getEndTimestamp() {
      return endTimestamp;
   }

   
   public void setEndTimestamp(Date endTimestamp) {
      this.endTimestamp = endTimestamp;
   }

   @ManyToOne(fetch = FetchType.EAGER)
   @JoinColumn(name = "INITIATOR_ID", nullable = true)
   public UserEntity getInitiator() {
      return initiator;
   }

   public void setInitiator(UserEntity initiator) {
      this.initiator = initiator;
   }

   @Column(name = "CURRENT_STATE", nullable = false)
   public String getCurrentState() {
      return currentState;
   }

   public void setCurrentState(String currentState) {
      this.currentState = currentState;
   }

   @OneToMany(cascade = CascadeType.ALL,
      fetch = FetchType.LAZY,
      mappedBy = "execution",
      targetEntity = MapReduceExecutionLogEntity.class)
   public List<MapReduceExecutionLogEntity> getLogs() {
      return logs;
   }
   
   public void setLogs(List<MapReduceExecutionLogEntity> logs) {
      this.logs = logs;
   }
   
   
}
