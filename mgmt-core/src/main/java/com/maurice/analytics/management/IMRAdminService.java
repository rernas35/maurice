package com.maurice.analytics.management;

import com.maurice.analytics.management.model.MapReduce;
import com.maurice.analytics.management.model.MapReduceEntity;

public interface IMRAdminService {
   
   MapReduceEntity findById(Long mrId);

   void createOrUpdate(MapReduce mapReduce);

   void delete(Long mapReduceId);

}
