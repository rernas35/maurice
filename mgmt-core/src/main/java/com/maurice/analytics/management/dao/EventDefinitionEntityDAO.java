package com.maurice.analytics.management.dao;

import com.maurice.analytics.management.model.EventDefinitionEntity;
import org.springframework.stereotype.Repository;

@Repository
public class EventDefinitionEntityDAO extends CachableGenericDAO<String, Long, EventDefinitionEntity>{

}
