package com.maurice.analytics.management;

import com.maurice.analytics.cache.model.EventDefinition;
import com.maurice.analytics.cache.model.TopicRoute;
import com.maurice.analytics.cache.model.TransformScript;
import com.maurice.analytics.cache.model.User;
import com.maurice.analytics.management.model.EventDefinitionEntity;
import com.maurice.analytics.management.model.TopicRouteEntity;
import com.maurice.analytics.management.model.TransformScriptEntity;
import com.maurice.analytics.management.model.UserEntity;

import java.util.Optional;

public interface IConversionService {

    EventDefinitionEntity newEventDefinitionEntity(EventDefinition eventDefinition);

    Optional<EventDefinitionEntity> convertEventDefinitionEntity(EventDefinition eventDefinition);

    UserEntity newUserEntity(User user);

    Optional<UserEntity> convertUserEntity(User user);

    TopicRouteEntity newTopicRouteEntity(TopicRoute topicRoute,EventDefinitionEntity eventDefinitionEntity);

    TransformScriptEntity newTransformScriptEntity(TransformScript transformScript, TopicRouteEntity topicRoute);


}
