package com.maurice.analytics.management.service;

import com.maurice.analytics.management.dao.MapReduceEntityDAO;
import com.maurice.analytics.management.model.MapReduceEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MapReduceService extends GenericService<MapReduceEntity, Long, MapReduceEntityDAO> {

   @Autowired
   MapReduceEntityDAO dao;
   
   @Override
   protected MapReduceEntityDAO getDAO() {
      return dao;
   }
   

   public List<MapReduceEntity> findByUserId(Long userId){
      return dao.findByUserId(userId);
   }
   

   public void delete(Long mrId) {
	  dao.delete(mrId);
   }
   

   public void update(MapReduceEntity entity) {
      dao.update(entity);
   }


}
