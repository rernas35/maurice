package com.maurice.analytics.management.model;

import com.maurice.analytics.cache.model.TopicRoute;
import com.maurice.analytics.cache.model.TopicThroughput;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="MAURICE_MGMT_TOPIC_ROUTE")
public class TopicRouteEntity {
   
   private Long topicRouteId;
   
   private String topicName;
   private String evaluationScript;
   private List<TransformScriptEntity> transformScriptLList = new ArrayList<TransformScriptEntity>();
   private TopicThroughput throughput = TopicThroughput.LOW;
   
   private EventDefinitionEntity eventDefinition;
   
   @Id
   @Column(name="TOPIC_ROUTE_ID")
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   public Long getTopicRouteId() {
      return topicRouteId;
   }
   
   public void setTopicRouteId(Long topicRouteId) {
      this.topicRouteId = topicRouteId;
   }
   
   @Column(name = "TOPIC_NAME", nullable = false) 
   public String getTopicName() {
      return topicName;
   }
   
   public void setTopicName(String topicName) {
      this.topicName = topicName;
   }
   
   @Column(name = "EVALUATION_SCRIPT", nullable = false) 
   public String getEvaluationScript() {
      return evaluationScript;
   }
   
   public void setEvaluationScript(String evaluationScript) {
      this.evaluationScript = evaluationScript;
   }
   
   @ManyToOne(fetch = FetchType.EAGER ,targetEntity=EventDefinitionEntity.class)
   @JoinColumn(name = "DEFINITION_ID", nullable = true)
   public EventDefinitionEntity getEventDefinition() {
      return eventDefinition;
   }
   
   public void setEventDefinition(EventDefinitionEntity eventDefinition) {
      this.eventDefinition = eventDefinition;
   }

   public void setThroughput(TopicThroughput throughput) {
      this.throughput = throughput;
   }
   
   @Enumerated(EnumType.STRING)
   @Column(name = "THROUGHPUT", nullable = false) 
   public TopicThroughput getThroughput() {
      return throughput;
   }

   @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "topicRoute", orphanRemoval = true, targetEntity = TransformScriptEntity.class)
   public List<TransformScriptEntity> getTransformScriptLList(){
       return transformScriptLList;
   }

    public void setTransformScriptLList(List<TransformScriptEntity> transformScriptLList) {
        this.transformScriptLList = transformScriptLList;
    }

    @Override
   public boolean equals(Object obj) {
      if (!(obj instanceof TopicRouteEntity)) return false;
      return topicRouteId == ((TopicRouteEntity) obj).topicRouteId;
   }
//    
//   @Override
//   public int hashCode() {
//      return topicRouteId.hashCode();
//   }
   
   public TopicRoute toTopicRoute(){
      TopicRoute topicRoute = new TopicRoute();
      topicRoute.setEvaluationScript(getEvaluationScript());
      topicRoute.setThroughput(getThroughput());
      topicRoute.setTopicName(getTopicName());
      topicRoute.setTopicRouteId(getTopicRouteId());
      getTransformScriptLList().forEach( ms -> topicRoute.getTransformScriptList().add(ms.toTransformScript()));
      return topicRoute;
      
   }

}
