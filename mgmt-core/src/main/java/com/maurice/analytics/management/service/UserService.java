package com.maurice.analytics.management.service;

import com.maurice.analytics.management.dao.UserEntityDAO;
import com.maurice.analytics.management.model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService extends CachableGenericService<String, Long, UserEntity,UserEntityDAO > {

   @Autowired
   UserEntityDAO dao;
   
   @Override
   protected UserEntityDAO getDAO() {
      return dao;
   }

}
