package com.maurice.analytics.management.model;

import java.io.Serializable;

public class MapReduce implements Serializable {

	private static final long serialVersionUID = -3664332165093387042L;
	
	private Long mapReduceId;
	private String name;
	private String code; 
	private Long ownerId;
	private String period;
	
	public Long getMapReduceId() {
		return mapReduceId;
	}

	public void setMapReduceId(Long mapReduceId) {
		this.mapReduceId = mapReduceId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public Long getOwnerId() {
		return ownerId;
	}
	
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	
	public String getPeriod() {
		return period;
	}
	
	public void setPeriod(String period) {
		this.period = period;
	}
	
	
	
	
}
