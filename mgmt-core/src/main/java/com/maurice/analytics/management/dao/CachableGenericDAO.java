package com.maurice.analytics.management.dao;

import com.maurice.analytics.management.cache.util.CacheUtil;
import com.maurice.analytics.management.model.ICachableEntity;
import com.maurice.analytics.management.model.exception.NoKeyDefinedForCachableObject;
import org.hibernate.query.Query;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public abstract class CachableGenericDAO<   KEY extends Serializable,
                                            ID extends Serializable,
                                            T extends ICachableEntity<KEY>>
                        extends GenericDAO<T,ID>{



    public T getByKey(KEY key){
        Class<T> persistentClazz = getPersistentClass();
        Optional<String> fieldName = CacheUtil.getKeyField(persistentClazz);
        if (!fieldName.isPresent())
            throw new NoKeyDefinedForCachableObject();
        Query query = getSessionFactory().getCurrentSession().createQuery(String.format("from %s where %s = :name ",persistentClazz.getSimpleName(),fieldName.get()));
        query.setParameter(fieldName.get(),key);
        List resultList = query.getResultList();
        if (resultList.isEmpty())
            return null;
        return (T) query.getResultList().get(0);
    }

   

}
