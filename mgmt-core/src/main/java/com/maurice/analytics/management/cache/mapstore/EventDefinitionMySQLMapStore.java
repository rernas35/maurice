package com.maurice.analytics.management.cache.mapstore;

import com.maurice.analytics.cache.model.EventDefinition;
import com.maurice.analytics.management.model.EventDefinitionEntity;
import com.maurice.analytics.management.service.EventDefinitionService;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class EventDefinitionMySQLMapStore extends GenericMapStore<  String,
                                                                    Long,
                                                                    EventDefinition,
                                                                    EventDefinitionEntity,
                                                                    EventDefinitionService> {


    @Override
    protected Optional<EventDefinitionEntity> toPersistedEntity(EventDefinition v) {
        return getConversionService().convertEventDefinitionEntity(v);
    }

    @Override
    protected EventDefinitionEntity toEntity(EventDefinition v) {
        return getConversionService().newEventDefinitionEntity(v);
    }

    @Override
    protected EventDefinition toCacheObject(EventDefinitionEntity e) {
        return e.toEventDefinition();
    }

    @Override
    protected void updateEntityWithStoredValues(EventDefinition value) {
        EventDefinitionEntity entity = getService().getByKey(value.getName());
        value.setId(entity.getDefinitionId());
        value.getTopicRouteList()
                .stream()
                .forEach(t ->   {
                                    entity.getTopicRoutes()
                                            .forEach(te ->  {
                                                                if (te.getTopicName().equals(t.getTopicName())) t.setTopicRouteId(te.getTopicRouteId());
                                                            });
                                }
                        );
    }
}
