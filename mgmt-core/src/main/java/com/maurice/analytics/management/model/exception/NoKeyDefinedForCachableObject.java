package com.maurice.analytics.management.model.exception;

public class NoKeyDefinedForCachableObject extends RuntimeException{

    public NoKeyDefinedForCachableObject(String message) {
        super(message);
    }

    public NoKeyDefinedForCachableObject() {
    }
}
