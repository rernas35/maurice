package com.maurice.analytics.management.service;

import com.hazelcast.config.*;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.maurice.analytics.cache.MauriceAnalyticsCache;
import com.maurice.analytics.cache.constants.MauriceCacheConstants;
import com.maurice.analytics.cache.model.EventDefinition;
import com.maurice.analytics.cache.model.TopicRoute;
import com.maurice.analytics.cache.model.TopicThroughput;
import com.maurice.analytics.cache.model.User;
import com.maurice.analytics.management.IAdminService;
import com.maurice.analytics.management.IMauriceTopicManager;
import com.maurice.analytics.management.cache.mapstore.EventDefinitionMySQLMapStore;
import com.maurice.analytics.management.cache.mapstore.GenericMapStore;
import com.maurice.analytics.management.cache.mapstore.UserMySQLMapStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;


@Service
@Transactional(propagation = Propagation.REQUIRED)
public class AdminService implements IAdminService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminService.class);

    @Autowired
    IMauriceTopicManager topicManager;

    @Autowired
    PlatformTransactionManager txManager;

    @Autowired
    EventDefinitionMySQLMapStore eventDefinitionMySQLMapStore;

    @Autowired
    UserMySQLMapStore userMySQLMapStore;

    @Autowired
    EventDefinitionService eventDefinitionService;

    @Autowired
    ConversionService conversionService;

    @PostConstruct
    public void init() {

        MauriceAnalyticsCache.initialize(this::initializeHazelcastInstanceWithMapStore);
        TransactionTemplate trxTemplate = new TransactionTemplate(txManager);
        trxTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                LOGGER.info("transaction manager is obtained. Do something here ");
            }
        });

    }

    private HazelcastInstance initializeHazelcastInstanceWithMapStore(){
        //Config config = getConfig();
        //return Hazelcast.newHazelcastInstance(config);
        return Hazelcast.newHazelcastInstance();
    }

    private Config getConfig() {
        Config config = new Config();
        config.getGroupConfig().setName("maurice").setPassword("maurice");
        setNetworkConfig(config);
        setMapStoreConfig(config,MauriceCacheConstants.EVENT_DEFINITION_CACHE,eventDefinitionMySQLMapStore);
        setMapStoreConfig(config,MauriceCacheConstants.USER_CACHE,userMySQLMapStore);
        return config;
    }

    private void setNetworkConfig(Config config) {
        NetworkConfig network = config.getNetworkConfig();
        JoinConfig join = network.getJoin();
        join.getMulticastConfig().setEnabled(false);
        join.getTcpIpConfig().addMember("127.0.0.1:5701")
                .addMember("127.0.0.1:5702").setEnabled(true);
        network.getInterfaces().setEnabled(true)
                .addInterface("127.0.0.1");
    }

    private void setMapStoreConfig(Config config, String mapName, GenericMapStore mapStore) {
        MapConfig mapConfig = config.getMapConfig(mapName);
        MapStoreConfig mapStoreConfig = mapConfig.getMapStoreConfig();
        mapStoreConfig.setEnabled(true);
        mapStoreConfig.setImplementation(mapStore);
    }


    public void addEventDefinition(EventDefinition eventDefinition) {
        MauriceAnalyticsCache.putEventDefinition(eventDefinition);
        LOGGER.info("Event definition is put into cache.");

        topicManager.createTopic(eventDefinition.getName(), TopicThroughput.LOW);
        LOGGER.info("Topic is created on PubSub");

        if (!eventDefinition.getTopicRouteList().isEmpty()) {
            for (TopicRoute topicRoute : eventDefinition.getTopicRouteList()) {
                topicManager.createTopic(topicRoute.getTopicName(), topicRoute.getThroughput());
                LOGGER.info("Topic is created on PubSub");

            }
        }
    }

    @Override
    public void updateEventDefinition(EventDefinition eventDefinition) {
        MauriceAnalyticsCache.putEventDefinition(eventDefinition);
        LOGGER.info("Event definition is put into cache.");

    }

    public void deleteEventDefinition(String definitionName) {
        MauriceAnalyticsCache.removeEventDefinition(definitionName);
        LOGGER.info("API is removed from cache.");

        topicManager.deleteTopic(definitionName);
        LOGGER.info("Topic is removed on PubSub");
    }

    public void addUser(User user) {
        MauriceAnalyticsCache.putUser(user);
        LOGGER.info("User is put into cache.");
    }

    public void deleteUser(String name) {
        MauriceAnalyticsCache.removeUser(name);
        LOGGER.info("User is removed from cache.");
    }

    public void addTopicToEventDefinition(String definitionName, TopicRoute topicRoute) {
        EventDefinition eventDefinition = MauriceAnalyticsCache.getEventDefinition(definitionName);
        eventDefinition.getTopicRouteList().add(topicRoute);
        MauriceAnalyticsCache.putEventDefinition(eventDefinition);
        LOGGER.info("Topic is added to event definition in cache");
        topicManager.createTopic(topicRoute.getTopicName(), topicRoute.getThroughput());
        LOGGER.info("Topic is created on PubSub");

    }

    public void deleteTopicFromEventDefinition(String definitionName, String topicRouteName) {
        EventDefinition eventDefinition = MauriceAnalyticsCache.getEventDefinition(definitionName);

        conversionService.convertEventDefinitionEntity(eventDefinition)
                            .ifPresent(e ->
                                            e.getTopicRoutes()
                                                    .stream()
                                                    .filter(t -> t.getTopicName().equals(topicRouteName))
                                                    .findAny()
                                                    .ifPresent(s -> {
                                                                        eventDefinition.getTopicRouteList().remove(s.toTopicRoute());
                                                                        MauriceAnalyticsCache.putEventDefinition(eventDefinition);
                                                                        LOGGER.info("Topic is removed to API in cache");
                                                                        topicManager.deleteTopic(s.getTopicName());
                                                                        LOGGER.info("Topic is removed on PubSub");
                                                                    }));;

    }

    public List<EventDefinition> listEventDefinition() {
        return MauriceAnalyticsCache.getEventDefinitions();
    }

    public EventDefinition getEventDefinition(String name) {
        return MauriceAnalyticsCache.getEventDefinition(name);
    }

    public List<User> listUser() {
        return MauriceAnalyticsCache.getUsers();
    }

    public List<TopicRoute> listTopicRoutesOfEventDefinition(String definitionName) {
        EventDefinition eventDEfinition = MauriceAnalyticsCache.getEventDefinition(definitionName);
        return eventDEfinition.getTopicRouteList();
    }

    public List<EventDefinition> listEventDefinitionByUser(String name) {
        return MauriceAnalyticsCache.getUser(name).getEventDefinitionNameList()
                .stream().map(s -> MauriceAnalyticsCache.getEventDefinition(s))
                .collect(Collectors.toList());

    }


}
