package com.maurice.analytics.management.cache.mapstore;

import com.maurice.analytics.cache.model.User;
import com.maurice.analytics.management.model.UserEntity;
import com.maurice.analytics.management.service.UserService;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserMySQLMapStore extends GenericMapStore<  String,
                                                            Long,
                                                            User,
                                                            UserEntity,
                                                            UserService> {

    @Override
    protected Optional<UserEntity> toPersistedEntity(User v) {
        return getConversionService().convertUserEntity(v);
    }

    @Override
    protected UserEntity toEntity(User v) {
        return getConversionService().newUserEntity(v);
    }

    @Override
    protected User toCacheObject(UserEntity e) {
        return e.toUser();
    }

    @Override
    protected void updateEntityWithStoredValues(User value) {
        value.setId(getService().getByKey(value.getName()).getUserId());
    }
}
