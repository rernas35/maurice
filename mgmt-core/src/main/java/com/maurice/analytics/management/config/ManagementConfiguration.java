package com.maurice.analytics.management.config;

import com.maurice.analytics.common.configuration.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ManagementConfiguration extends Configuration{
   
   static Logger logger = LoggerFactory.getLogger(ManagementConfiguration.class);
   
   static ManagementConfiguration instance =new ManagementConfiguration(); 
   static {
      try {
          instance.load("management.properties");
      } catch (IOException  e) {
         logger.error("error while reading mapreduce configuration " , e);
         System.exit(-1);
      } 
   }
   
   public static String getZookeeper(){
      return instance.getProperty("zookeeper");
   }
   
   public static Integer getReplicationFactor(){
      try {
         String valueStr = instance.getProperty("replication.factor");
         logger.info("Replication factor is ${}", valueStr);
         if (valueStr == null){
            logger.error("replication.factor is not set.Default value is set as 1.");
            return 1;
         }
         return Integer.parseInt(valueStr);
      }catch (NumberFormatException e) {
         logger.error("replication.factor must be a number.Default value is set as 1.");
         return  1;
         
      }
   }
   

}
