
package com.maurice.analytics.management.dao;

import com.maurice.analytics.management.model.TopicRouteEntity;
import org.springframework.stereotype.Repository;

@Repository
public class TopicRouteEntityDAO extends GenericDAO<TopicRouteEntity, Long> {

   @Override
   public void persist(TopicRouteEntity t) {
      throw new RuntimeException("Use orphan functioanlity under ApplicationEntity or EventDefinitionEntity");
   }

   @Override
   public TopicRouteEntity merge(TopicRouteEntity t) {
      throw new RuntimeException("Use orphan functioanlity under ApplicationEntity or EventDefinitionEntity");
   }

   @Override
   public void delete(TopicRouteEntity t) {
      throw new RuntimeException("Use orphan functioanlity under ApplicationEntity or EventDefinitionEntity");
   }

}
