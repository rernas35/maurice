package com.maurice.analytics.management.cache.mapstore;

import com.hazelcast.core.MapStore;
import com.hazelcast.core.PostProcessingMapStore;
import com.maurice.analytics.cache.model.ICachable;
import com.maurice.analytics.management.IConversionService;
import com.maurice.analytics.management.model.ICachableEntity;
import com.maurice.analytics.management.service.CachableGenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Configurable
public abstract class GenericMapStore<  KEY extends Serializable,
                                        ID extends Serializable,
                                        VALUE extends ICachable<KEY>,
                                        ENTITY extends ICachableEntity<KEY>,
                                        SERVICE extends CachableGenericService
                                    >
                        implements MapStore<KEY, VALUE>,PostProcessingMapStore{

    @Autowired
    SERVICE service;

    @Autowired
    IConversionService conversionService;

    protected SERVICE getService() {
        return service;
    }

    protected IConversionService getConversionService() {
        return conversionService;
    }

    @Override
    public void store(KEY key, VALUE value) {
        ENTITY entity = toEntity(value);
        if (value.getId() != null){
            service.merge(entity);
        }else {
            service.persist(entity);
        }

        updateEntityWithStoredValues(value);
    }

    @Override
    public void storeAll(Map<KEY, VALUE> map) {
        map.entrySet().stream().forEach(value -> toPersistedEntity(value.getValue()).ifPresent(service::persist));
    }

    @Override
    public void delete(KEY key) {
        service.deleteByKey(key);
    }

    @Override
    public void deleteAll(Collection<KEY> collection) {
        collection.stream().forEach(k -> service.deleteByKey(k));
    }

    @Override
    public VALUE load(KEY k) {
        ENTITY entity = (ENTITY) service.getByKey(k);
        if (entity != null)
            return toCacheObject(entity);
        return null;
    }

    @Override
    public Map<KEY, VALUE> loadAll(Collection<KEY> collection) {
        return collection.stream().map(k -> toCacheObject((ENTITY)service.getByKey(k)))
                                    .collect(Collectors.toMap(o -> o.getKey(), Function.identity()));
    }

    @Override
    public Iterable<KEY> loadAllKeys() {
        return service.listKeys();
    }

    protected abstract Optional<ENTITY> toPersistedEntity(VALUE v);

    protected abstract ENTITY toEntity(VALUE v);

    protected abstract VALUE toCacheObject(ENTITY e);

    protected abstract void updateEntityWithStoredValues(VALUE value);

}
