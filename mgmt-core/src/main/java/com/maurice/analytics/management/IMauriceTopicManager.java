package com.maurice.analytics.management;

import com.maurice.analytics.cache.model.TopicThroughput;

public interface IMauriceTopicManager {
   
   void createTopic(String topicName,TopicThroughput throughput);
   
   void deleteTopic(String topicName);

}
