package com.maurice.analytics.management.model;

import javax.persistence.*;

@Entity
@Table(name="MAURICE_MGMT_MAP_REDUCE_EXECUTION_LOG")
public class MapReduceExecutionLogEntity {
   
   Long logId;
   MapReduceExecutionEntity execution;
   String description;
   String state;
   
   @Id
   @Column(name = "EXECUTION_LOG_ID")
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   public Long getLogId() {
      return logId;
   }
   
   
   public void setLogId(Long logId) {
      this.logId = logId;
   }
   
   @ManyToOne(fetch = FetchType.EAGER,targetEntity = MapReduceExecutionEntity.class)
   @JoinColumn(name = "MAP_REDUCE_EXECUTION_ID", nullable = true)
   public MapReduceExecutionEntity getExecution() {
      return execution;
   }

   
   public void setExecution(MapReduceExecutionEntity execution) {
      this.execution = execution;
   }
   
   
   public String getState() {
      return state;
   }
   
   @Column(name = "STATE", nullable = true)
   public void setState(String state) {
      this.state = state;
   }


   @Column(name = "DESCRIPTION", nullable = true)
   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }
}
