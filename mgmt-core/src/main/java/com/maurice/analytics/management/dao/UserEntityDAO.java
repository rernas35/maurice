package com.maurice.analytics.management.dao;

import com.maurice.analytics.management.model.UserEntity;
import org.springframework.stereotype.Repository;

@Repository
public class UserEntityDAO extends CachableGenericDAO<String, Long, UserEntity>{

}
