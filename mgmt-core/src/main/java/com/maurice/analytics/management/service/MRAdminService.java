package com.maurice.analytics.management.service;

import com.maurice.analytics.management.IMRAdminService;
import com.maurice.analytics.management.model.MapReduce;
import com.maurice.analytics.management.model.MapReduceEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation=Propagation.REQUIRED)
public class MRAdminService implements IMRAdminService {

   private static Logger LOGGER = LoggerFactory.getLogger(MRAdminService.class);

   @Autowired
   MapReduceService mrService;

   @Autowired
   UserService userService;
   
   public MapReduceEntity findById(Long mrId){
      return mrService.getById(mrId);
   }

   public void createOrUpdate(MapReduce mapReduce){
      MapReduceEntity mrEntity = new MapReduceEntity();

      mrEntity.setMapreduceId(mapReduce.getMapReduceId());
      mrEntity.setName(mapReduce.getName());
      mrEntity.setCode(mapReduce.getCode());
      mrEntity.setOwner(userService.getById(mapReduce.getOwnerId()));
      mrEntity.setPeriod(mapReduce.getPeriod());

      mrService.merge(mrEntity);
   }

   @Override
   public void delete(Long mapReduceId) {
      MapReduceEntity mrEntity = mrService.getById(mapReduceId);
      mrService.delete(mrEntity);
   }
}
