package com.maurice.analytics.management.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="MAURICE_MGMT_MAP_REDUCE")
public class MapReduceEntity {

   Long mapreduceId;
   
   String name;
   String code;
   String period;
   UserEntity owner;
   
   Date createTimestamp = new Date();
   Date updateTimeStamp = new Date();
   
   List<MapReduceExecutionEntity> executions;
   
   @Id
   @Column(name = "MAP_REDUCE_ID")
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   public Long getMapreduceId() {
      return mapreduceId;
   }
   
   public void setMapreduceId(Long mapreduceId) {
      this.mapreduceId = mapreduceId;
   }
   
   @Column(name = "NAME", nullable = false)
   public String getName() {
      return name;
   }
   
   public void setName(String name) {
      this.name = name;
   }
   
   @Column(name = "CODE", nullable = false)
   public String getCode() {
      return code;
   }
   
   public void setCode(String code) {
      this.code = code;
   }
   
   @Column(name = "PERIOD", nullable = true)
   public String getPeriod() {
      return period;
   }
   
   public void setPeriod(String period) {
      this.period = period;
   }
   
   @ManyToOne(fetch = FetchType.EAGER)
   @JoinColumn(name = "OWNER_ID", nullable = true)
   public UserEntity getOwner() {
      return owner;
   }

   public void setOwner(UserEntity owner) {
      this.owner = owner;
   }
   
   @Column(name = "CREATE_TIMESTAMP", nullable = false)
   public Date getCreateTimestamp() {
      return createTimestamp;
   }
   
   public void setCreateTimestamp(Date createTimestamp) {
      this.createTimestamp = createTimestamp;
   }
   
   @Column(name = "UPDATE_TIMESTAMP", nullable = false)
   public Date getUpdateTimeStamp() {
      return updateTimeStamp;
   }
   
   public void setUpdateTimeStamp(Date updateTimeStamp) {
      this.updateTimeStamp = updateTimeStamp;
   }
  
   @OneToMany(cascade = CascadeType.ALL,
      fetch = FetchType.LAZY,
      mappedBy = "mapReduce",
      targetEntity = MapReduceExecutionEntity.class)
   public List<MapReduceExecutionEntity> getExecutions() {
      return executions;
   }
   
   public void setExecutions(List<MapReduceExecutionEntity> executions) {
      this.executions = executions;
   }
   
}
