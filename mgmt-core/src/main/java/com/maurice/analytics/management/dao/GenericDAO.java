package com.maurice.analytics.management.dao;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.List;

public abstract class GenericDAO<   T,
                                    ID extends Serializable>{

    private static Logger LOGGER = LoggerFactory.getLogger(GenericDAO.class);

   @Autowired
   SessionFactory sessionFactory;
   
   private Class<T> persistentClass;
   
   public GenericDAO() {
       this.persistentClass = (Class<T>) Arrays.stream(((ParameterizedType) getClass()
                                                .getGenericSuperclass())
                                                .getActualTypeArguments()).filter(
                                                        c -> ((Class<T>)c).getPackage().getName().startsWith("com.maurice.analytics.management.model")
                                                ).findAny().get();
  }

    protected Class<T> getPersistentClass() {
        return persistentClass;
    }

    public void persist(T t){
       getSessionFactory().getCurrentSession().persist(t);
   }
   
   public T merge(T t){
      return (T) getSessionFactory().getCurrentSession().merge(t);
   }
   
   public void delete(T t){
      getSessionFactory().getCurrentSession().delete(t);
   }
   
   public T getById(ID id){
      return getSessionFactory().getCurrentSession().get(persistentClass, id);
   }
   
   public List<T> list(){
      CriteriaBuilder cb = getSessionFactory().getCurrentSession().getCriteriaBuilder();
      CriteriaQuery<T> query = cb.createQuery(persistentClass);
      Root<T> root = query.from( persistentClass );
      query.select(root);
      return getSessionFactory().getCurrentSession().createQuery(query).getResultList();  
   }

   
   public void setSessionFactory(SessionFactory sessionFactory) {
      this.sessionFactory = sessionFactory;
   }
   
   
   public SessionFactory getSessionFactory() {
      return sessionFactory;
   }

   

}
