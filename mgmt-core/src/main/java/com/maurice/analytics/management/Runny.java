package com.maurice.analytics.management;

import com.maurice.analytics.cache.model.User;
import com.maurice.analytics.management.context.ManagementContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Runny {

    public static void main(String... args){
        ApplicationContext ctx = new AnnotationConfigApplicationContext(ManagementContext.class);
        IAdminService adminService = ctx.getBean(IAdminService.class);
        User user = new User();
        user.setExternalId("asd");
        user.setName("sadasd");
        adminService.addUser(user);
        adminService.listUser().stream().forEach(System.out::println);
    }
}
