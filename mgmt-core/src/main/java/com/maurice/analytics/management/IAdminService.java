
package com.maurice.analytics.management;

import com.maurice.analytics.cache.model.EventDefinition;
import com.maurice.analytics.cache.model.TopicRoute;
import com.maurice.analytics.cache.model.User;

import java.util.List;


public interface IAdminService  {


    void addEventDefinition(EventDefinition eventDefinition);

    void updateEventDefinition(EventDefinition eventDefinition);

    void deleteEventDefinition(String definitionName);

    void addUser(User user);

    void deleteUser(String name);

    void addTopicToEventDefinition(String definitionName, TopicRoute topicRoute) ;
   
    void deleteTopicFromEventDefinition(String definitionName, String topicRouteName) ;
   
    List<EventDefinition> listEventDefinition();

    EventDefinition getEventDefinition(String name);

    List<EventDefinition> listEventDefinitionByUser(String name);
   
    List<User> listUser();

    List<TopicRoute> listTopicRoutesOfEventDefinition(String definitionName);

}
