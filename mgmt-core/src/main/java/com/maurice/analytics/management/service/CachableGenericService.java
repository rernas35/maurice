package com.maurice.analytics.management.service;


import com.maurice.analytics.management.dao.CachableGenericDAO;
import com.maurice.analytics.management.model.ICachableEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Service
public abstract class CachableGenericService<   KEY extends Serializable,
                                                ID extends Serializable,
                                                T extends ICachableEntity<KEY>,
                                                DAO extends CachableGenericDAO<KEY,ID,T>>
                extends GenericService< T,ID,DAO> {


    @Transactional(propagation=Propagation.REQUIRED,readOnly=true)
    public T getByKey(KEY key){
        return getDAO().getByKey(key);
    }

    @Transactional(propagation=Propagation.REQUIRED)
    public void deleteByKey(KEY key){
        T t = getByKey(key);
        getDAO().delete(t);
    }

    @Transactional(propagation= Propagation.REQUIRED,readOnly=true)
    public List<KEY> listKeys(){
        return list().stream().map(t -> t.getKey()).collect(Collectors.toList());
    }
}
