
package com.maurice.analytics.management.kafka;

import com.maurice.analytics.cache.model.TopicThroughput;
import com.maurice.analytics.management.IMauriceTopicManager;
import com.maurice.analytics.management.config.ManagementConfiguration;
import kafka.admin.AdminUtils;
import kafka.admin.RackAwareMode;
import kafka.utils.ZKStringSerializer$;
import kafka.utils.ZkUtils;
import org.I0Itec.zkclient.ZkClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Properties;

@Service
public class MauriceKafkaManager implements IMauriceTopicManager {

   private static ManagementConfiguration configuration;
   private static Logger logger = LoggerFactory.getLogger(MauriceKafkaManager.class);

   private final static int sessionTimeoutMs = 1000;
   private final static int connectionTimeoutMs = 1000;

   private static ZkUtils zkUtils;

   public MauriceKafkaManager() {
      configuration = new ManagementConfiguration();
      try {
         configuration.load(System.getProperty("conf.dir"), "management.properties");
         zkUtils = getZKUtils();
      } catch (IOException e) {
         logger.error("Error while initialization.Can't read management properties", e);
         System.exit(1);
      }
   }

   @Override
   public void createTopic(String topicName, TopicThroughput throughput) {
      try {
         AdminUtils.createTopic(zkUtils, topicName, throughput.getPartitionCount(), configuration.getReplicationFactor(), new Properties(),
            RackAwareMode.Disabled$.MODULE$);
      } catch (Exception e) {
         logger.error("Error while creating topic.", e);
      }
   }

   @Override
   public void deleteTopic(String topicName) {
      try {
         AdminUtils.deleteTopic(zkUtils, topicName);
      } catch (Exception e) {
         logger.error("Error while creating topic.", e);
      }

   }

   private static ZkUtils getZKUtils() {
      ZkClient zkClient = new ZkClient(configuration.getZookeeper(), sessionTimeoutMs, connectionTimeoutMs, ZKStringSerializer$.MODULE$);
      ZkUtils zkUtils = ZkUtils.apply(zkClient, false);
      return zkUtils;
   }

   public static void main(String[] args) {
      //   new MauriceKafkaManager().createTopic("deneme",TopicThroughput.LOW,2);
      //     new MauriceKafkaManager().deleteTopic("deneme");
   }

}
