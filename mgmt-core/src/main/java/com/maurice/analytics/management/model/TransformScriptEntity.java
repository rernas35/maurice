package com.maurice.analytics.management.model;

import com.maurice.analytics.cache.model.TransformPhase;
import com.maurice.analytics.cache.model.TransformScript;

import javax.persistence.*;


@Entity
@Table(name = "MAURICE_MGMT_TRANSFORM_SCRIPT")
public class TransformScriptEntity {

    private Long scriptId;
    private String phase;
    private String fieldPath;
    private String javascript;
    private String action;
    private TopicRouteEntity topicRoute;


    @Id
    @Column(name = "SCRIPT_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getScriptId() {
        return scriptId;
    }

    public void setScriptId(Long scriptId) {
        this.scriptId = scriptId;
    }

    @Column(name = "PHASE", nullable = false)
    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    @Column(name = "JAVASCRIPT", nullable = true)
    public String getJavascript() {
        return javascript;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

    @Column(name = "FIELD_PATH", nullable = false)
    public String getFieldPath() {
        return fieldPath;
    }

    public void setFieldPath(String fieldPath) {
        this.fieldPath = fieldPath;
    }

    @Column(name = "ACTION", nullable = false)
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @ManyToOne(fetch = FetchType.EAGER ,targetEntity=TopicRouteEntity.class)
    @JoinColumn(name = "TOPIC_ROUTE_ID", nullable = false)
    public TopicRouteEntity getTopicRoute() {
        return topicRoute;
    }

    public void setTopicRoute(TopicRouteEntity topicRoute) {
        this.topicRoute = topicRoute;
    }

    public TransformScript toTransformScript(){
        TransformScript script = new TransformScript();
        script.setAction(action);
        script.setPhase(TransformPhase.valueOf(phase));
        script.setFieldPath(fieldPath);
        script.setJavascript(javascript);
        return script;
    }

}
