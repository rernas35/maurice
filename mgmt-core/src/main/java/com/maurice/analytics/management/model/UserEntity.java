package com.maurice.analytics.management.model;

import com.maurice.analytics.cache.model.User;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name="MAURICE_MGMT_USER")
public class UserEntity implements ICachableEntity<String>{

   private Long userId;
   @CacheKey
   private String name;
   private String externalId;
   private Set<EventDefinitionEntity> apis;
   
   
   @Id
   @Column(name="USER_ID")
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   public Long getUserId() {
      return userId;
   }
   
   public void setUserId(Long userId) {
      this.userId = userId;
   }

   @Column(name = "NAME", nullable = false, unique = true)
   public String getName() {
      return name;
   }
   
   public void setName(String name) {
      this.name = name;
   }
   
   @Column(name = "EXTERNAL_ID", nullable = false)
   public String getExternalId() {
      return externalId;
   }
   
   public void setExternalId(String externalId) {
      this.externalId = externalId;
   }

   @OneToMany(cascade = CascadeType.ALL,
      fetch = FetchType.LAZY,
      mappedBy = "owner",
      targetEntity = EventDefinitionEntity.class)
   public Set<EventDefinitionEntity> getApis() {
      return apis;
   }
   
   public void setApis(Set<EventDefinitionEntity> apis) {
      this.apis = apis;
   }
   
   @Override
   public boolean equals(Object obj) {
      if (!(obj instanceof UserEntity)) return false;
      return userId.equals(((UserEntity) obj).userId);
   }

   @Override
   @Transient
   public String getKey() {
      return getName();
   }

   public User toUser(){
      User user = new User();
      user.setId(getUserId());
      user.setExternalId(getExternalId());
      user.setName(getName());
      
      return user;
      
   }
   
   
   
}
