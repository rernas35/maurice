package com.maurice.analytics.management.cache.util;

import com.maurice.analytics.management.model.ICachableEntity;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Optional;

public class CacheUtil {

    public static  Optional<String> getKeyField(Class clazz){
        Optional<Field> field = Arrays.stream(clazz.getDeclaredFields()).filter(f -> f.getAnnotation(ICachableEntity.CacheKey.class) != null).findAny();
        if (field.isPresent())
            return Optional.of(field.get().getName());
        return Optional.empty();

    }


}
