package com.maurice.analytics.management.service;

import com.maurice.analytics.management.dao.TopicRouteEntityDAO;
import com.maurice.analytics.management.model.TopicRouteEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopicRouteService extends GenericService<TopicRouteEntity, Long, TopicRouteEntityDAO> {

   @Autowired
   TopicRouteEntityDAO dao;
   
   @Override
   protected TopicRouteEntityDAO getDAO() {
      return dao;
   }

}
