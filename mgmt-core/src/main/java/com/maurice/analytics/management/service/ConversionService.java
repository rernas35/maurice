package com.maurice.analytics.management.service;


import com.maurice.analytics.cache.model.EventDefinition;
import com.maurice.analytics.cache.model.TopicRoute;
import com.maurice.analytics.cache.model.TransformScript;
import com.maurice.analytics.cache.model.User;
import com.maurice.analytics.management.IConversionService;
import com.maurice.analytics.management.model.EventDefinitionEntity;
import com.maurice.analytics.management.model.TopicRouteEntity;
import com.maurice.analytics.management.model.TransformScriptEntity;
import com.maurice.analytics.management.model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ConversionService implements IConversionService {

    @Autowired
    UserService userService;

    @Autowired
    EventDefinitionService eventDefinitionService;


    @Override
    public EventDefinitionEntity newEventDefinitionEntity(EventDefinition eventDefinition) {
        EventDefinitionEntity entity = new EventDefinitionEntity();
        entity.setDefinitionId(eventDefinition.getId());
        entity.setDescription(eventDefinition.getDescription());
        entity.setExternalId(eventDefinition.getExternalId());
        entity.setName(eventDefinition.getName());
        entity.setOwner(userService.getByKey(eventDefinition.getOwnerName()));
        entity.setFormat(eventDefinition.getFormat());
        entity.getTopicRoutes().addAll(eventDefinition.getTopicRouteList().stream().map(t -> newTopicRouteEntity(t,entity)).collect(Collectors.toList()));
        return entity;
    }

    @Override
    public Optional<EventDefinitionEntity> convertEventDefinitionEntity(EventDefinition eventDefinition) {
        return Optional.of(eventDefinitionService.getByKey(eventDefinition.getName()));
    }

    @Override
    public UserEntity newUserEntity(User user) {
        UserEntity entity = new UserEntity();
        entity.setExternalId(user.getExternalId());
        entity.setName(user.getName());
        return entity;
    }

    @Override
    public Optional<UserEntity> convertUserEntity(User user) {
        return Optional.of(userService.getByKey(user.getName()));
    }

    @Override
    public TopicRouteEntity newTopicRouteEntity(TopicRoute topicRoute,EventDefinitionEntity eventDefinitionEntity) {
        TopicRouteEntity entity = new TopicRouteEntity();
        entity.setEvaluationScript(topicRoute.getEvaluationScript());
        entity.setThroughput(topicRoute.getThroughput());
        entity.setTopicName(topicRoute.getTopicName());
        entity.setEventDefinition(eventDefinitionEntity);
        entity.getTransformScriptLList().addAll(topicRoute.getTransformScriptList().stream().map(ts -> newTransformScriptEntity(ts,entity)).collect(Collectors.toList()));
        return entity;
    }

    @Override
    public TransformScriptEntity newTransformScriptEntity(TransformScript transformScript, TopicRouteEntity topicRoute) {
        TransformScriptEntity entity = new TransformScriptEntity();
        entity.setAction(transformScript.getAction());
        entity.setJavascript(transformScript.getJavascript());
        entity.setPhase(transformScript.getPhase().toString());
        entity.setFieldPath(transformScript.getFieldPath());
        entity.setTopicRoute(topicRoute);
        return entity;
    }

}
