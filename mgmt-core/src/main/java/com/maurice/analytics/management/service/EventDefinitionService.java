package com.maurice.analytics.management.service;


import com.maurice.analytics.management.dao.EventDefinitionEntityDAO;
import com.maurice.analytics.management.model.EventDefinitionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventDefinitionService extends CachableGenericService<String,Long,EventDefinitionEntity,EventDefinitionEntityDAO> {

   @Autowired
   EventDefinitionEntityDAO dao;
   
   @Override
   protected EventDefinitionEntityDAO getDAO() {
      return dao;
   }


}
