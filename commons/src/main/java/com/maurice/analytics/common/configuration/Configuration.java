package com.maurice.analytics.common.configuration;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Configuration {
   
   Properties properties = new Properties();
   
   public void load(String filename) throws FileNotFoundException, IOException{
      load(System.getProperty("conf.dir"),filename);
   }
   
   public void load(String folder,String fileName) throws FileNotFoundException, IOException{
      if (!folder.endsWith("/")) folder += "/" + fileName;
      if (folder.startsWith("hdfs"))
         loadHDFS(folder, fileName);
      else
         properties.load(new FileInputStream(new File(folder)));
   }
   
   private void loadHDFS(String folder, String fileName)  throws FileNotFoundException, IOException{


      org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();

      String hadoopHome = System.getenv("HADOOP_HOME");
      if (!hadoopHome.endsWith("/")) hadoopHome += "/";
      
      conf.addResource(new Path(hadoopHome + "etc/hadoop/core-site.xml"));
      conf.addResource(new Path(hadoopHome +"/etc/hadoop/hdfs-site.xml"));
      conf.addResource(new Path(hadoopHome +"/etc/hadoop/mapred-site.xml"));

      FileSystem fileSystem = FileSystem.get(conf);
      String targetFile = "/verapi/conf/scheduler.conf";
      Path srcPath = new Path(targetFile);

      String filename = targetFile.substring(targetFile.lastIndexOf('/') + 1, targetFile.length());
      String dstPath = "/tmp/" +filename;
      
      try {
         fileSystem.copyFromLocalFile(srcPath, new Path(dstPath));
      } catch (Exception e) {
         System.err.println("Exception caught! :" + e);
         System.exit(1);
      } finally {
         fileSystem.close();
      }
      properties.load(new FileInputStream(new File(dstPath)));
   
   }
   
   
   public String getProperty(String key) {
      return properties.getProperty(key);
   }
   
   
   public Properties getProperties() {
      return properties;
   }

}
