package com.maurice.analytics.common.parameters;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.List;

public class EvaluationParam {

  
   List<ParameterWithType> paramList;
   List<ParameterWithType> reportList;
   
   String evaluationScript;
   

   public List<ParameterWithType> getReportList() {
      if (reportList == null)
         reportList = new ArrayList<ParameterWithType>();
      return reportList;
   }
   
   public void setReportList(List<ParameterWithType> reportList) {
      this.reportList = reportList;
   }
   
   public List<ParameterWithType> getParamList() {
      if (paramList == null)
         paramList = new ArrayList<ParameterWithType>();
      return paramList;
   }
   
   public void setParamList(List<ParameterWithType> paramList) {
      this.paramList = paramList;
   }
   
   @Transient
   public String getParams(){
      List<String> sList = new ArrayList<String>();
      for (ParameterWithType paramType : paramList) {
         sList.add(paramType.toString());
      }
      return String.join(",", sList);
   }
   
   public String getEvaluationScript() {
      return evaluationScript;
   }
   
   public void setEvaluationScript(String evaluationScript) {
      this.evaluationScript = evaluationScript;
   }
   
   @Override
   public String toString() {
      Gson gson =  new GsonBuilder().disableHtmlEscaping().create();
      return gson.toJson(this);
   }
   
   public static EvaluationParam create(String s){
      Gson gson = new Gson();
      return gson.fromJson(s, EvaluationParam.class);
   }
   
   public static void main(String[] args) {
      EvaluationParam ep = new EvaluationParam();
      ep.getParamList().add(new ParameterWithType("falan","Long"));
      ep.getParamList().add(new ParameterWithType("filan","Long"));
      ep.setEvaluationScript("falan > 0 && filan == 0");
      System.out.println(ep);
   }
   

}
