package com.maurice.analytics.common.sql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionProvider {

   private static Logger logger = LoggerFactory.getLogger(ConnectionProvider.class); 
   
   public static Connection getConnection(String url,String driver, String username,String password){
      
      try {
         logger.info("Driver : {}" , driver);
         Class.forName(driver);
         Connection connection = null;
         connection = DriverManager.getConnection(
            url,username, password);
         return connection;
      } catch (ClassNotFoundException | SQLException e) {
        logger.error("wrong type of driver is set in mapreduce.properties.",e);
        System.exit(-1);
        return null;
      }
   }
   
}
