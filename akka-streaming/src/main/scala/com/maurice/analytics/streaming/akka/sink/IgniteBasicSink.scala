package com.maurice.analytics.streaming.akka.sink

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import com.maurice.analytics.cache.MauriceAnalyticsCache
import com.maurice.analytics.dynamic.event.model.gen.PojoGenerator
import org.apache.ignite.Ignition

import scala.collection.mutable

class IgniteBasicSink(system:ActorSystem, mat:ActorMaterializer) extends BaseSink {
  val ignite = Ignition.start()

  val genereatedClassMap : mutable.Map[String,java.lang.Class[java.io.Serializable]] = new mutable.HashMap[String,java.lang.Class[java.io.Serializable]]()


  def processSink(source:Source[(String, String), AnyRef]):Unit = {

    source.map{ e =>  {
                        val cacheName = e._1;
                        val value = e._2;
                        val eventDef = MauriceAnalyticsCache.getEventDefinition(cacheName);
                        val mapValue = genereatedClassMap.get(cacheName)
                        val pojo  = mapValue.getOrElse(PojoGenerator.generateClass(cacheName,eventDef.getFormat) )
                        if (mapValue.isEmpty)
                          genereatedClassMap.put(cacheName,pojo);
                        val serializedObject = PojoGenerator.initiateObject(pojo.asInstanceOf[java.lang.Class[java.io.Serializable]],eventDef.getFormat,value);
                        (cacheName,serializedObject)
                      }}
      .runForeach(e => push2Ignite(e))(mat)

  }

  def push2Ignite(e:(String,Object)):Unit = {
  }


}
