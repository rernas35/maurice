package com.maurice.analytics.streaming.akka.sink

import java.lang

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.maurice.analytics.dynamic.event.model.util.IgniteUtil
import org.apache.ignite.IgniteCache
import org.apache.ignite.cache.CacheMode

import scala.collection.mutable

class IgniteTableSink(system:ActorSystem, mat:ActorMaterializer) extends IgniteBasicSink(system,mat) {

  val cacheMap : mutable.Map[String,IgniteCache[java.lang.String,java.lang.Object]] = new mutable.HashMap[String,IgniteCache[java.lang.String,java.lang.Object]]()

  override def push2Ignite(e:(String,Object)):Unit = {
      getCache(e._1).put(System.currentTimeMillis() + "",e._2)
  }

  def getCache(cacheName:String):IgniteCache[java.lang.String,java.lang.Object]={

      val mapValue = cacheMap.get(cacheName)
      if (mapValue.isEmpty){
        ignite.getOrCreateCache(getCacheConfiguration(cacheName));
        val cache :IgniteCache[lang.String,Object] = ignite.cache(cacheName);
        cacheMap.put(cacheName,cache)
        cache
      }else
        mapValue.get
  }

  protected def getCacheConfiguration(cacheName: String) = {
    val clazzOption = genereatedClassMap.get(cacheName).get;
    val orgCacheCfg = IgniteUtil.getCacheConfiguration("".getClass, clazzOption, cacheName);
    orgCacheCfg.setName(cacheName)
    orgCacheCfg.setCacheMode(CacheMode.REPLICATED)
    orgCacheCfg.setIndexedTypes(classOf[lang.String], clazzOption)
    orgCacheCfg
  }
}
