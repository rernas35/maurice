package com.maurice.analytics.streaming.akka.sink

import akka.stream.scaladsl.Source

trait BaseSink {

  def processSink(source:Source[(String, String), AnyRef]):Unit

}
