package com.maurice.analytics.streaming.akka.sink

import akka.actor.ActorSystem
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl.Producer
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import com.maurice.analytics.streaming.akka.StreamingConstants.BOOTSTRAP_SERVER_KEY
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.{ByteArraySerializer, StringSerializer}

class KafkaSink(system:ActorSystem,mat:ActorMaterializer) extends BaseSink {


  def processSink(source:Source[(String, String), AnyRef]):Unit = {
    val producerSettings = ProducerSettings(system,new ByteArraySerializer,new StringSerializer)
      .withBootstrapServers(System.getProperty(BOOTSTRAP_SERVER_KEY))

    source.map{ e => new ProducerRecord[Array[Byte], String](e._1, e._2)}
      .runWith(Producer.plainSink(producerSettings))(mat)
  }


}
