package com.maurice.analytics.streaming.akka

import java.util.regex.Pattern

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import com.maurice.analytics.streaming.akka.StreamingConstants._
import com.maurice.analytics.streaming.akka.bender.StreamBender
import com.maurice.analytics.streaming.akka.sink.{IgniteStreamerSink, IgniteTableSink, KafkaSink}
import com.maurice.analytics.streaming.akka.source.KafkaSource

object Runny extends App {

  val topic = System.getProperty(TOPIC_KEY);
  val bootstrapServer = System.getProperty(BOOTSTRAP_SERVER_KEY);
  val mode = System.getProperty(MODE)

  val sourceSink = mode.split(Pattern.quote(">"));
  val modeSource = sourceSink(0)
  val modeSink = sourceSink(1)

  implicit val system = ActorSystem(SYSTEM_NAME)
  implicit val materializer = ActorMaterializer()

  private var source: Source[(String, String), AnyRef] = null

  if (modeSource.equals("kafka"))
    source = new KafkaSource(system).getSource();
  private val sinkInput: Source[(String, String), AnyRef] = source.via(StreamBender.mapToTopicsFlow())
  if (modeSink.equals("ignite-table"))
    new IgniteTableSink(system,materializer).processSink(sinkInput)
  else if (modeSink.equals("ignite-stream"))
    new IgniteStreamerSink(system,materializer).processSink(sinkInput)
  else if (modeSink.equals("kafka"))
    new KafkaSink(system,materializer).processSink(sinkInput)
  
  
}