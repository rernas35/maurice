package com.maurice.analytics.streaming.akka.bender

import java.util.function.Supplier

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.hazelcast.core.{Hazelcast, HazelcastInstance}
import com.maurice.analytics.cache.MauriceAnalyticsCache
import com.maurice.analytics.cache.model.{EventDefinition, TopicRoute, TransformPhase}
import com.maurice.analytics.dynamic.event.model.eval.TransformContext
import com.maurice.analytics.dynamic.event.model.eval.impl.{FlatJSFormatter, SkipPolicy}

import scala.collection.JavaConversions._
import scala.collection.mutable.ListBuffer

object StreamBender {

  MauriceAnalyticsCache.initialize(getHazelcastInstance)
  val formatter = new FlatJSFormatter();

  def mapToTopicsFlow(): Flow[(String,String), (String,String), NotUsed] =Flow[(String,String)]
                      .map(x => (MauriceAnalyticsCache.getEventDefinition(x._1),x._2))
                      .mapConcat(p => {
                                        var listBuffer : ListBuffer[(EventDefinition,TopicRoute,String)] = new ListBuffer[(EventDefinition,TopicRoute,String)]()
                                        p._1.getTopicRouteList.foreach(topicRoute => listBuffer += ((p._1,topicRoute,p._2)))
                                        listBuffer.toList
                                      }
                                )
                      .map(p => {
                                  println("adadas")
                                  val eventDefinition = p._1
                                  val topicRoute = p._2
                                  val value = p._3
                                  val jsObject = formatter.parse(eventDefinition.getFormat,value)
                                  val transformContext = new TransformContext();
                                  topicRoute.getTransformScriptList
                                    .filter(_.getPhase == TransformPhase.PRE)
                                    .foreach(x => transformContext.addPreTransformationPolicy(new SkipPolicy(x.getFieldPath)))
                                  (topicRoute.getTopicName,formatter.format(eventDefinition.getFormat,jsObject,transformContext) )
                                }
                      )


  def getHazelcastInstance() : Supplier[HazelcastInstance] = {
     new Supplier[HazelcastInstance] {
      override def get(): HazelcastInstance = Hazelcast.newHazelcastInstance();
    }
  }



}
