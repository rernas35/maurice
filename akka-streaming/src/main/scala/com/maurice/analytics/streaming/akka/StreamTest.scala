package com.maurice.analytics.streaming.akka
import akka.Done
import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl.{Flow, GraphDSL, RunnableGraph, Sink, Source, ZipWith}

import scala.concurrent.Future
import scala.concurrent.duration._

object StreamTest extends App {

  implicit val system = ActorSystem("Test")
  implicit val materializer = ActorMaterializer()

  def maxx(x:Int , y:Int):Int = {
    println("x : " + x + " - y : " + y)
    java.lang.Math.max(x,y)
  }

  def maxxx(x:Int, y:Int, z:Int  ):Int = {
    println("x : " + x + " - y : " + y + " - z : " + z)
    val result = java.lang.Math.max(java.lang.Math.max(x,y),z)
    result
  }

  val pickMaxOfThree = GraphDSL.create() { implicit b ⇒
    val zip1 = b.add(ZipWith[Int, Int, Int, Int](maxxx _))
    UniformFanInShape(zip1.out, zip1.in0, zip1.in1, zip1.in2)
  }

  pickMaxOfThree.addAttributes(Attributes.inputBuffer(initial = 1, max = 1))

  val resultSink = Sink.ignore

  val s1 = Source(1 to 100).throttle(1,1.seconds)
  val s2 = Source(101 to 200).throttle(1, 1.seconds).conflate((s1,s2) => s2 )
  val s3 = Source(201 to 300).throttle(1,4.seconds)


  val g = RunnableGraph.fromGraph(GraphDSL.create(resultSink) { implicit b ⇒ sink ⇒
    import GraphDSL.Implicits._

    // importing the partial graph will return its shape (inlets & outlets)
    val pm3 = b.add(pickMaxOfThree)
    val sysout = Flow[Int].alsoTo(Sink.foreach(println))

    s1 ~> pm3.in(0)
    s2 ~> pm3.in(1)
    s3 ~> pm3.in(2)
    pm3.out ~> sysout ~> sink.in
    ClosedShape
  })

  val max: Future[Done] = g.run()



/*
  implicit val executionContext = ExecutionContext.global
  implicit val system = ActorSystem("QuickStart")
  implicit val materializer = ActorMaterializer()
  val sourceTwo = Source(List(("falan","3") ,("falan","a")))
  val mtFlow =  StreamBender.mapToTopicsFlow()
  sourceTwo.via(mtFlow).to(Sink.foreach(print(_))).run()
*/
/*val source = Source(1 to 11)
val sink = Sink.fold[Int, Int](0)(_ + _)
val runnable: RunnableGraph[Future[Int]] = source.toMat(sink)(Keep.right)
val sum: Future[Int] = runnable.run()
print("sum:" + sum.onComplete(f => print(f)))*/

}
