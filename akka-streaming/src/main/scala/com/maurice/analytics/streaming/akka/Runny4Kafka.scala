package com.maurice.analytics.streaming.akka

import akka.actor.ActorSystem
import akka.kafka.scaladsl.{Consumer, Producer}
import akka.kafka.{ConsumerSettings, ProducerSettings, Subscriptions}
import akka.stream.ActorMaterializer
import com.maurice.analytics.streaming.akka.StreamingConstants._
import com.maurice.analytics.streaming.akka.bender.StreamBender
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.{ByteArraySerializer, StringDeserializer, StringSerializer}

object Runny4Kafka extends App {

  val topic = System.getProperty(TOPIC_KEY);
  val bootstrapServer = System.getProperty(BOOTSTRAP_SERVER_KEY);
  implicit val system = ActorSystem(SYSTEM_NAME)
  implicit val materializer = ActorMaterializer()
  
  val consumerSettings = ConsumerSettings(system, new StringDeserializer, new StringDeserializer)
                            .withBootstrapServers(bootstrapServer)
                            .withGroupId(SYSTEM_NAME)
                            .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
                            
  val producerSettings = ProducerSettings(system,new ByteArraySerializer,new StringSerializer)
                             .withBootstrapServers(bootstrapServer)

  Consumer.committableSource(consumerSettings, Subscriptions.topics(topic))
                          .map(x => (x.record.topic(),x.record.value()))
                          .via(StreamBender.mapToTopicsFlow())
                          .map{ e => new ProducerRecord[Array[Byte], String](e._1, e._2)}
                          .runWith(Producer.plainSink(producerSettings))
  
  
}