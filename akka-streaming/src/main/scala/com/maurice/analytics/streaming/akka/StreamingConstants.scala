package com.maurice.analytics.streaming.akka

object StreamingConstants {
  
  val SYSTEM_NAME:String = "maurice-stream-bender"
  val BOOTSTRAP_SERVER_KEY:String = "bootstrap-server"
  val TOPIC_KEY:String = "topics"
  val MODE : String = "mode"
  
}