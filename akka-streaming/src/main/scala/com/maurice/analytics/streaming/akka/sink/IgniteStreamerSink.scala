package com.maurice.analytics.streaming.akka.sink

import java.lang

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import org.apache.ignite.IgniteDataStreamer

import scala.collection.mutable

class IgniteStreamerSink(system:ActorSystem, mat:ActorMaterializer) extends IgniteBasicSink(system,mat) {

  val streamerMap : mutable.Map[String,IgniteDataStreamer[java.lang.String,java.lang.Object]] = new mutable.HashMap[String,IgniteDataStreamer[java.lang.String,java.lang.Object]]()

  override def push2Ignite(e:(String,Object)):Unit = {
      getStreamer(e._1).addData(System.currentTimeMillis() + "",e._2)
  }

  def getStreamer(cacheName:String):IgniteDataStreamer[java.lang.String,java.lang.Object]={

      val mapValue = streamerMap.get(cacheName)
      if (mapValue.isEmpty){
        ignite.getOrCreateCache(cacheName);
        val streamer :IgniteDataStreamer[lang.String,Object] = ignite.dataStreamer(cacheName);
        streamerMap.put(cacheName,streamer)
        streamer
      }else
        mapValue.get
  }

}
