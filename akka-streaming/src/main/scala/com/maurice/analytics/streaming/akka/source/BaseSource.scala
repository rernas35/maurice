package com.maurice.analytics.streaming.akka.source

import akka.stream.scaladsl.Source

trait BaseSource {

  def getSource():Source[(String, String), AnyRef]

}
