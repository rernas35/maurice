package com.maurice.analytics.streaming.akka

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source

object  SampleClass  extends App{
  implicit val system = ActorSystem("Sample")
  implicit val materializer = ActorMaterializer()

  val planB = Source(List("five", "six", "seven", "eight"))

  Source(0 to 6).map(n ⇒
    if (n < 5) n.toString
    else throw new RuntimeException("Boom!")
  ).recoverWithRetries(attempts = 3, {
    case _: RuntimeException ⇒ planB
  }).runForeach(println)

}
