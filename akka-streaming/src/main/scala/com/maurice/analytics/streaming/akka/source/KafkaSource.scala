package com.maurice.analytics.streaming.akka.source

import akka.actor.ActorSystem
import akka.kafka.scaladsl.Consumer
import akka.kafka.{ConsumerSettings, Subscriptions}
import akka.stream.scaladsl.Source
import com.maurice.analytics.streaming.akka.StreamingConstants.{BOOTSTRAP_SERVER_KEY, SYSTEM_NAME, TOPIC_KEY}
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer

class KafkaSource(system:ActorSystem) extends BaseSource {

  def getSource():Source[(String, String), AnyRef] = {
    val topic = System.getProperty(TOPIC_KEY);
    val bootstrapServer = System.getProperty(BOOTSTRAP_SERVER_KEY);

    val consumerSettings = ConsumerSettings(system, new StringDeserializer, new StringDeserializer)
      .withBootstrapServers(bootstrapServer)
      .withGroupId(SYSTEM_NAME)
      .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")

    Consumer.committableSource(consumerSettings, Subscriptions.topics(topic))
      .map(x => (x.record.topic(), x.record.value()))
  }

}
