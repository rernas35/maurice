package com.maurice.analytics.spark.implicits

import com.datastax.driver.core.ResultSet
import com.datastax.spark.connector.cql.CassandraConnector
import com.maurice.analytics.spark.implicits.utils.ConfigurationHelper._
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.{SparkConf, SparkContext}

object VerapiSource {

  def getKafkaStream(ssc: StreamingContext, topic: String): DStream[(String, String)] = {
    val topicMap = topic.split(",").map((_, getNumThread().toInt)).toMap
    val stream = KafkaUtils.createStream(ssc, getZookeeper(), getKafkaClientId(), topicMap)
    stream
  }

  def queryNoSQL(conf: SparkConf, cql: String, distributed: Boolean): ResultSet = {
    if (distributed)
      throw new RuntimeException("Method is not implemented.")
    else
      queryNoSQLOnMaster(conf, cql)
  }
  
  def queryNoSQLOnExecutor(sc : SparkContext , cql : String ){
     
  }

  def queryNoSQLOnMaster(conf: SparkConf, cql: String): ResultSet = {

    conf.set("spark.cassandra.connection.host", getCassandraCluster)
      .set("spark.cassandra.auth.username", getCassandraUser)
      .set("spark.cassandra.auth.password", getCassandraPassword)

    val conn: CassandraConnector = CassandraConnector.apply(conf)
    val csSession = conn.openSession()
    csSession.execute(cql)
  }

  def queryNoSQLOnSlaves(conf: SparkConf, cql: String): Unit = {

  }

  def loadRDBMSTable(context: SparkContext, table: String): DataFrame = {
    val sqlContext = new SQLContext(context)

    val dataframe = sqlContext.read.format("jdbc")
      .option("url", getJDBCAddress)
      .option("user", getJDBCUser)
      .option("password", getJDBCPassword)
      .option("driver", getRDBMSDriver())
      .option("dbtable", table).load

    dataframe
  }
  
  def queryRDBMS(context : SparkContext , sql : String, tables : String*) = {
    val sqlContext = new SQLContext(context)
    tables.foreach(t => 
    sqlContext.read.format("jdbc")
      .option("url", getJDBCAddress)
      .option("user", getJDBCUser)
      .option("password", getJDBCPassword)
      .option("driver", getRDBMSDriver())
      .option("dbtable", t).load
      .registerTempTable(t))
    
    sqlContext.sql(sql).show
    
  }

}