package com.maurice.analytics.spark.implicits.sample

import com.maurice.analytics.spark.implicits.VerapiSource
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.JavaConversions._

object Runny {
  
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("bomba");
    val sc = new SparkContext(conf);
    val rdd : RDD[String] = sc.parallelize(Seq("falan"));
//    rdd.pushToKafka("topicIste")
    
//    rdd.pushToKafka();
    
    val session = SparkSession.builder
      .config(conf)
      .getOrCreate()
    
    case class DataModelBean(i:Integer)
    
    val resultSet = VerapiSource.queryNoSQL(conf, "select api_id,app_id,product_id,request_date from verapi_analytics.verapi_api_call where api_id = 107 allow filtering",false)
    println("resdd.first():" + resultSet.one().getInt("api_id"))
    val mapped = resultSet.map(f => DataModelBean(f.getInt("api_id")))
    val rdd2 = sc.parallelize(mapped.toSeq)
//   
//    rdd2.foreach(r => {println(r)})
    
    
//    VerapiSource.loadRDBMSTable(sc,"operating_systems")
    VerapiSource.queryRDBMS(sc, """select o.name,o.family,s.status_code 
                                    from sum_08_os_status_code_minute s 
                                    inner join operating_systems o on s.operating_system_id = o.id"""
                                ,"operating_systems",
                                "sum_08_os_status_code_minute" )
    
    
//    val resdd = sc.parallelize(resultSet.all())
//    resdd.foreach(x => {println(x)})
    
    
//    rdd.toDF().saveToNoSQL("falan")
//      rdd.toDF().saveToRDBMS("sdsa");    
    
  }  
}