package com.maurice.analytics.spark.implicits

import java.util.HashMap

import com.maurice.analytics.spark.implicits.utils.ConfigurationHelper._
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, SaveMode}

import scala.reflect.ClassTag


class VerapiSaveToNoSQL(df: DataFrame) extends Serializable {

  def saveToNoSQL(tableName: String): Unit = {
    df.write.format("org.apache.spark.sql.cassandra")
      .options(Map("table" -> tableName, "keyspace" -> "verapi_business_analytics"))
      .mode(SaveMode.Append)
      .save()
  }

}

class VerapiSaveToRDBMS(df: DataFrame) extends Serializable {

  def saveToRDBMS(tableName: String): Unit = {
    df.write
      .mode(SaveMode.Append)
      .format("jdbc")
      .option("url", getJDBCAddress())
      .option("dbtable", "VBA" + "." + tableName)
      .option("user", getJDBCUser)
      .option("password", getJDBCPassword)
      .option("driver", getRDBMSDriver)
      .save()
  }

}

class VerapiPushKafkaTopicWithTopicName[V: ClassTag](rdd: RDD[(V)]) extends Serializable {

  val props = new HashMap[String, Object]()
  props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, getKafkaBrokers())
  props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, getKafkaValueSerializer())
  props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, getKafkaKeySerializer())

  val producer = new KafkaProducer[String, V](props)

  def pushToKafka(topicName : String): Unit = {

    rdd.foreach(record => {
        val message = new ProducerRecord[String, V](topicName, null, record)
        producer.send(message)
    })

  }

}

class VerapiPushKafkaTopic[T: ClassTag, K: ClassTag, V: ClassTag](rdd: RDD[(T,(K, Iterable[V]))]) extends Serializable {

  val props = new HashMap[String, Object]()
  props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, getKafkaBrokers())
  props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, getKafkaValueSerializer())
  props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, getKafkaKeySerializer())

  val producer = new KafkaProducer[K, V](props)

  def pushToKafka(): Unit = {

    rdd.foreach(record => {
      val it = record._2._2.iterator
      val k:K = record._2._1
      while (it.hasNext) {
        val e : V = it.next
        val kafkaOpTopic = record._1.asInstanceOf[String]
        val message = new ProducerRecord[K, V](kafkaOpTopic, k, e)
        producer.send(message)

      }
    })

  }

}

object VerapiDataFrameFunctions {
  implicit def saveToNoSQL(df: DataFrame) = new VerapiSaveToNoSQL(df)
  implicit def saveToRDBMS(df: DataFrame) = new VerapiSaveToRDBMS(df)
}

object VerapiRDDFunctions {
  implicit def pushToKafka[T: ClassTag ,K: ClassTag , V: ClassTag](rdd: RDD[(T,(K, Iterable[V]))]) 
                                                          = new VerapiPushKafkaTopic[T, K, V](rdd)
  implicit def pushToKafkaWithTopicName[V: ClassTag](rdd: RDD[V]) 
                                                          = new VerapiPushKafkaTopicWithTopicName[V](rdd)
}



