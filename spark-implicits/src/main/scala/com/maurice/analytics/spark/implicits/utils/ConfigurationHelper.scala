package com.maurice.analytics.spark.implicits.utils

import com.maurice.analytics.common.configuration.Configuration

object ConfigurationHelper extends Configuration{

  System.setProperty("conf.dir", "/Users/rernas/git/analytics/analytics/analytics-spark-implicits/src/main/resources")
  load(System.getProperty("conf.dir"), "mapreduce.properties")

 def getNumThread(): Integer = {
    try {
      Integer.parseInt(getProperty("numthread"))
    } catch {
      case nfe: NumberFormatException => {
        //TODO: log about default values.        
        1
      }
    }
  }
  
  def getZookeeper(): String = {
    val zookeeperQuorum = getProperty("zookeeper");
    if (zookeeperQuorum == null)
      "localhost"
    else
      zookeeperQuorum
  }

  def getKafkaBrokers(): String = {
    val kafkaBrokers = getProperty("kafka.brokers");
    if (kafkaBrokers == null)
      "localhost"
    else
      kafkaBrokers
  }
  
  def getKafkaClientId(): String = {
    val clientId = getProperty("client.id");
    if (clientId == null)
      "StreamingEngine"
    else
      clientId
  }

  def getKafkaKeySerializer(): String = {
    val kafkaSerializer = getProperty("kafka.key.serializer");
    if (kafkaSerializer == null)
      "org.apache.kafka.common.serialization.StringSerializer"
    else
      kafkaSerializer
  }

  def getKafkaValueSerializer(): String = {
    val kafkaSerializer = getProperty("kafka.value.serializer");
    if (kafkaSerializer == null)
      "org.apache.kafka.common.serialization.StringSerializer"
    else
      kafkaSerializer
  }

  def getCassandraCluster(): String = {
    val cassandracluster = getProperty("cassandra.cluster");
    if (cassandracluster == null)
      "localhost"
    else
      cassandracluster
  }
  
  def getCassandraKeySpace(): String = {
    val keySpace = getProperty("cassandra.keySpace");
    if (keySpace == null)
      "verapi_business_analytics"
    else
      keySpace
  }

  def getCassandraUser(): String = {
    val cassandrauser = getProperty("cassandra.user");
    if (cassandrauser == null)
      "cassandra"
    else
      cassandrauser
  }

  def getCassandraPassword(): String = {
    val cassandrapassword = getProperty("cassandra.password");
    if (cassandrapassword == null)
      "cassandra"
    else
      cassandrapassword
  }

  def getJDBCAddress(): String = {
    val rdbmsaddress = getProperty("jdbc.address");
    if (rdbmsaddress == null)
      "localhost"
    else
      rdbmsaddress
  }

  def getJDBCUser(): String = {
    val rdbmsuser = getProperty("jdbc.user");
    if (rdbmsuser == null)
      "postgres"
    else
      rdbmsuser
  }

  def getJDBCPassword(): String = {
    val rdbmspassword = getProperty("jdbc.password");
    if (rdbmspassword == null)
      "postgres"
    else
      rdbmspassword
  }
  
  def getRDBMSDriver(): String = {
    val driver = getProperty("rdbms.driver");
    if (driver == null)
      "org.postgresql.Driver"
    else
      driver
  }
}